This guide provides instructions to use the benchmarking tool suite 
for launching experiments through the platform IoT-LAB, and analyzing the results.

Setting up 
===========================

#### IoT-LAB account
To launch the experiments and retrieve their results, IoT-LAB credentials are required by the scripts. 
An account on [IoT-LAB](https://www.iot-lab.info/) has to be created by the user.

#### Configure the SSH access
SSH access to IoT-LAB Gateway has to be configured to copy data from the control node to your machine by using SCP.
A tutorial to configure the SSH access is available [here](https://www.iot-lab.info/tutorials/ssh-access/).

#### Create the profiles
Each node of the [IoT-LAB platform](https://www.iot-lab.info/hardware/) is composed of three elements: an Open Node, a Gateway and a Control Node. 
The Control Node interacts with the Open Node to monitor consumption and sensors values during experiments.

![Main components of an IoT-LAB node](documentation/images/iot-lab_ON-GW-CN.png)

The Control Node is configured by a profile, associated to the user account. After creating your IoT-LAB account, 
the following commands have to be executed in order to create the profiles that will be used by the benchmarking scripts:

```
ssh <iot-lab-user>@saclay.iot-lab.info

iotlab-profile addm3 -n consumption_and_rssi -current -voltage -power -period 332 -avg 16 -rssi -channels 26 -rperiod 100

iotlab-profile addm3 -n consumption_and_sniffer -current -voltage -power -period 332 -avg 16 -sniffer -channels 26
```

#### Libraries
To run the benchmarking scripts on your lcoal machine, Python3 is requited.
We tested the scripts with Python version 3.5.2.

```
apt install python3
```
You need also to install the CLI tools of IoT-LAB:
```
sudo pip install iotlabcli
```

Running benchmarks
==================

The `scripts` folder provides several programs for running benchmarks and analyzing their results.

Three python scripts are available in the `scripts` folder: `global.py`, `launch_experiment.py` and `analytics.py`. 
The documentation for these scripts can be accesed by running them with the option `--help`.

The `global.py` is the main script to start a benchmarking operation or to analyse the results.
You only need to use this script.

#### 1. ```global.py```
```
python3 global.py --help
```
![Global help](documentation/images/global_help.png)


#### Configuration details

The options of the configuration file or the script ```global.py```are defined as following:

| Configuration file element | Details                                                      |
| -------------------------- | ------------------------------------------------------------ |
| `username`                 | The username of the IoT-LAB account                          |
| `password`                 | The password of the IoT-LAB account                          |
| `ssh-key-file`             | The SSH key used to access the IoT-LAB userspace through SSH |
| `architecture`             | The architecture of the nodes (only m3 nodes)       |
| `topology`                 | The topology of the nodes to use in the experiments ("line", "grid")         |
| `duration`                 | The duration of each experiment in minutes                   |
| `os`                       | Operating system used for the firmware of the nodes (only “contiki-ng”)                               |
| `algorithms`               | The list of cryptograpphic algorithms to benchmark |
| `profile`                  | The profile to configure (consumption_and_sniffer or consumption_and_rssi) |
| `operation`                | The operation to do (launch-only, analyse-only, launch-and-analyse, analyse-several) |
| `experiments`              | The IDs of the experiments to compare                        |

The list of cryptographic algorithms supported by our benchmarking tool is available in the folder `scripts/utils.py/` (class Algorithm).

When using the operations `analyse-only` or `launch-and-analyse`, the option `data_processing` is required.
This options takes one of these values: `cut_percentage` or `cut-minutes`. It is used by the analysis tool to remove 
the transient measurements by using either a percentage or a number of minutes. 

When using the operation `analyse-several`, two more options are required which are:
- `output` which represents the folder where the analysis results will be stored
- `experiments_number` which denotes the maxium number of experiments to be used when computing the average values for each algorithm. 

#### 2. Launch the script with a configuration file
A configuration file can be used with the option `--config` (or `-c`). The content of the configuration file is detailed in part [Configuration](#configuration).

For a first test, the configuration file `conf/default_conf.conf` is provided.

**Nota Bene:** Your IoT-LAB credentials (usrname and password) have to be provided in the configuration file or through the options. 
Thus, you have to edit the configuration file or use the command line (by using the `--username` (or `-u`) and the `--password` (or `-pwd`) options).

```
python3 global.py --config conf/default_conf.conf
```

The default_conf.conf file contains the following configuration:
```
username: IoT-LAB_USERNAME
password: IoT-LAB_PWD
ssh-key-file: /home/soline/.ssh/pem

duration: 20
architecture: m3
topology: test2
os: contiki-ng

algorithms: [ascona128-512-0-logs]
#algorithms: [ascona128-512-0-logs, no-enc-512-0-logs, pyjamask-512-0-logs, skinny-512-0-logs]

operation: launch-ony
profile: consumption_and_sniffer

output: analyse1
```

The default configuarion launches two experiments of two minutes on M3 nodes configured in a line tpology, and with Contiki NG as Operating System. 
Then, it retrieves the experiment data in a local directory named `data/<experiment_id>`. The use of the firmware or the choice of the nodes are detailed in the section [More details](#more-details).

#### 3. Launch the script through the command line
As showin the help in section [Display the help](#display-the-help), some options can be used instead of the configuration file.

Thus, each one of the elements in the configuration file can be replaced by two command line options (one short and one long), as illustrated in the following table.

| Configuration file element | Short command line option | Long command line option |
| -------------------------- | ------------------------- | ------------------------ |
| `username`                 | `-u`                      | `--username`             |
| `password`                 | `-pwd`                    | `--password`             |
| `ssh-key-file`             | `-k`                      | `--ssh-key-file`         |
| `architecture`             | `-a`                      | `--architecture`         |
| `topology`                 | `-t`                      | `--topology`             |
| `duration`                 | `-d`                      | `--duration`             |
| `os`                       |                           | `--os`                   |
| `algorithms `              |                           | `--algorithms`           |
| `profile`                  | `-p`                      | `--profile`              |
| `operation`                | `-o`                      | `--operation`            |
| `experiments`              | `-e`                      | `--experiments`          |


A sample of a configuration file is the following:
```
duration: 2
architecture: m3
topology: line
os: contiki-ng
algorithms: [ascona128-512-0-logs]
username: IoT-LAB-USERNAME
password: IoT-LAB-PASSWORD
ssh-key-file: /home/user/.ssh/iot-lab-key
```

This configuration can be applied by using the following options of the script ```global.py```:

```
python3 global.py --duration 2 --architecture m3 --topolgy line --os contiki-ng 
                   --username IoT-LAB-USERNAME --password IoT-LAB-PASSWORD 
                  --algorithms ascona128-512-0-logs
                  --ssh-key-file /home/user/.ssh/iot-lab-key
```

or with short option names:

```
python3 global.py -d 2 -a m3 -t line --os contiki-ng -u IoT-LAB-USERNAME -pwd IoT-LAB-PASSWORD 
                  --algorithms ascona128-512-0-logs
                  -k /home/user/.ssh/iot-lab-key
```

## How it works
Launching and handling an experiment is done in four main steps:
1. Defining the resources related to the given configuration file (or parameters)
2. Launching the experiment using the iotlabcli library
3. Launching the traffic capture when the experiment’s status is “Running”
4. Retrieving the data related to the experiment when the status of the latter is “Terminated”

The client nodes and the server node are defined from the architecture and the topology
specified in the configuration file. We manually identified the nodes corresponding to a certain
configuration (architecture, topology) by observing which nodes are linked together when creating a RPL network. 
The following nodes are associated to the architecture “m3” and the “line” topology:

• m3-358.grenoble.iot-lab.info (as server)
• m3-318.grenoble.iot-lab.info (as client)
• m3-326.grenoble.iot-lab.info (as client)
• m3-334.grenoble.iot-lab.info (as client)
• m3-342.grenoble.iot-lab.info (as client)
• m3-350.grenoble.iot-lab.info (as client)

The following nodes are associated to the architecture “m3” and the "grid" topology:
• m3-32.lille.iot-lab.info (as server)
• m3-21.lille.iot-lab.info (as client)
• m3-30.lille.iot-lab.info (as client)
• m3-40.lille.iot-lab.info (as client)
• m3-63.lille.iot-lab.info (as client)
• m3-73.lille.iot-lab.info (as client)
### Retrieved data
After running the experiment with the `global.py` script, the resultats are available in the folder `data/<experiment_id>`:
```
 ├── data
 |    ├── <experiment_id_a>
 |    |    └── ...
 |    ├── ...
 |    ├── <experiment_id_z>
 |    |    ├── consumption
 |    |    |    ├── <node_id_A>.oml
 |    |    |    ├── ...
 |    |    |    └── <node_id_Z>.oml
 |    |    ├── event
 |    |    |    └──
 |    |    ├── log
 |    |    |    ├── <node_id_A>.log
 |    |    |    ├── ...
 |    |    |    └── <node_id_Z>.log
 |    |    ├── radio
 |    |    |    ├── <node_id_A>.oml
 |    |    |    ├── ...
 |    |    |    └── <node_id_Z>.oml
 |    |    └── sniffer
 |    |    |    └──
 |    |    ├── experiment.json
 |    |    ├── server.pcap
```
### Data analysis
The analysis tool takes as input the files generated in the `data/<experiment_id>`. 
It uses the script `global.py` with the operaion option : `analyse-only` and the identifiers of the experiments to compare their results. 
An example of such configuration is the following:
```
architecture: m3
topology: grid
experiments: [210093,210094]
operation: analyse-only
data_processing: cut-percentage
```
We note that the options architecture and topology are required in order to determine the client nodes and the server nodes
of the experiments.
