# Contiki 3.0 on M3 nodes

This repository presents 3 different programs:
* simple-app
* ccm-app - encrypt
* ccm-app - auth encrypt
* lilliput-app.

In order to make a firmware using Contiki 3.0 for M3 nodes, and include one of these programs, the next steps have to be followed:

1. Clone the Inria repository and the IoT-LAB Git repository:
 ```
 git clone https://github.com/iot-lab/iot-lab.git
 cd iot-lab/
 ```

2. Set up Contiki 3.0. In order to do this, the command:
 ```
 make setup-contiki
 ```
 is used. It will generate the repositories `parts/contiki` and `parts/openlab`, such as described in the `Makefile`.
 **Nota bene:** In order to get the list of available options, the command `make` can be used.

3. Apply the modifications presented in the file `<path of the Inria Gitlab repository>/lilliput-ae-contiki/Contiki-3.0/lilliput\ to\ contiki\ modif.txt` in the file `parts/contiki/Makefile.include`:
 ```
 vim parts/contiki/Makefile.include
 ```

4. (Optional) Copy the Lilliput library from the Inria Gitlab if we want to use it (if we create a firmware containing lilliput, such as `lilliput-app`).
 ```
 cp -r <path of the Inria Gitlab repository>/lilliput-ae-contiki/Contiki-3.0/core/lib/lilliputae/ parts/contiki/core/lib/
 ```

5. Copy the files corresponding to one of the four examples in the Inria Gitlab in a folder of the `examples` repository
 ```
 cd parts/contiki/examples
 mkdir inria
 cd inria
 cp -r <path of the Inria Gitlab repository>/lilliput-ae-contiki/Contiki-3.0/examples/encrypt/<path of the program to use> ./
 ```
 **Nota bene:** The "path of the program to use" can be:
  * simple-app/
  * ccm-app/Encrypt/
  * ccm-app/Auth_Encrypt/
  * lilliput-app/

6. Go to the copied repository.
 ```
 cd <name of the program to use>
 ```
 **Nota bene:** Now, the "name of the program to use" is (if it was not modified in the precedent step):
  * simple-app/
  * Encrypt/
  * Auth_Encrypt/
  * lilliput-app/

7. Make the firmware.
 * ... for M3 nodes:
  ```
  make TARGET=iotlab-m3
  ```
  **Nota bene:** The list of possible targets can be retrieved through the command: ` make targets`

Some files with the extension `.iotlab-m3` have been generated. They will be used to flash the firmware of the M3 nodes on the IoT-LAB platform.
