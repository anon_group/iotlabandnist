/* C include */
#include <stdio.h>

/* Contiki include */
#include "contiki.h"
#include "net/ip/uip-udp-packet.h"
#include "net/rpl/rpl.h"

/* Debug msg */
#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

/* Timer */
#include "sys/etimer.h"
#define DATA_INTERVAL_TRANS		(1 * CLOCK_SECOND)

/* UDP PORT connexion */
#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

/* UDP IP HEADER BUFFER */
#define UIP_IP_BUF   ((struct uip_udpip_hdr *)&uip_buf[UIP_LLH_LEN])

static struct uip_udp_conn *unicast_connection;

/*---------------------------------------------------------------------------*/
PROCESS(client_process, "client process");
AUTOSTART_PROCESSES(&client_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *appdata;
  if(uip_newdata()) {
    appdata = (char *)uip_appdata;
    appdata[uip_datalen()] = 0;
    PRINTF("Data received from -");
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF(" on port %d from port %d with length %d: '%s'\n",
          UIP_HTONS(UIP_IP_BUF->destport), UIP_HTONS(UIP_IP_BUF->srcport), uip_datalen(), appdata);
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0x2000, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
}
/*---------------------------------------------------------------------------*/
PROCESS(process_send_data, "Send data process");
PROCESS_THREAD(process_send_data, ev, data)
{
  char buf[UIP_APPDATA_SIZE];
  static int count = 0;

  PROCESS_BEGIN();

  set_global_address();

  snprintf(buf, sizeof(buf), "Hello server %d", count++);

  PRINTF("Sending unicast to ");
  PRINT6ADDR(data);
  PRINTF(" with length %d: '%s'\n", strlen(buf), buf);

  uip_udp_packet_sendto(unicast_connection, buf, strlen(buf),
                        data, UIP_HTONS(UDP_SERVER_PORT));

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(client_process, ev, data)
{
  static struct etimer send_data_timer;
  rpl_dag_t *dag;

  PROCESS_BEGIN();

  /* new connection with server host */
  unicast_connection = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL); 
  if(unicast_connection == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(unicast_connection, UIP_HTONS(UDP_CLIENT_PORT)); 

  etimer_set(&send_data_timer, DATA_INTERVAL_TRANS);

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }

    if(etimer_expired(&send_data_timer)) {
      etimer_reset(&send_data_timer);

      dag = rpl_get_any_dag();
      if(dag != NULL) {
        process_start(&process_send_data, &dag->dag_id);
      }
    }
  }
  PROCESS_END();
}
