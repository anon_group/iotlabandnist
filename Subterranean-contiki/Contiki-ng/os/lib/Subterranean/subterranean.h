#ifndef SUBTERRANEAN_H
#define SUBTERRANEAN_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
//#include "subterranean_ref.h"

//#include "api.h"
//#include "subterranean_ref.h"

#define CRYPTO_KEYBYTES 16
#define CRYPTO_NSECBYTES 0
#define CRYPTO_NPUBBYTES 16
#define CRYPTO_ABYTES 16
#define CRYPTO_NOOVERLAP 1

//aded
#define MAXTEST_BYTES_M 16 
#define MAXTEST_BYTES_AD 16
///////////////////////////////////////////////////////////////////
//#ifndef _SUBTERRANEAN_REF_H_
//#define _SUBTERRANEAN_REF_H_

#define SUBTERRANEAN_SIZE 257

int crypto_aead_encrypt(
	unsigned char *c, unsigned long long *clen,
	const unsigned char *m, unsigned long long mlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *nsec,
	const unsigned char *npub,
	const unsigned char *k
);

int crypto_aead_decrypt(
	unsigned char *m, unsigned long long *mlen,
	unsigned char *nsec,
	const unsigned char *c, unsigned long long clen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *npub,
	const unsigned char *k
);

#endif
