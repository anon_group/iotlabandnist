#!/usr/bin/env python3

import sqlite3
import os
from singleton import Singleton

DATABASE_PATH = 'data/experiments.db'
#DATABASE_PATH = '/media/virtualram/experiments.db'

def save_experiment_in_database(experiment_id, os, architecture, topology,
                                algorithm, duration, profile, data_folder_path,
                                original_experiment_id, associated_experiment_id):
    # Request to save the experiment and its metadata
    sql_add_experiment = """
        INSERT INTO Experiment(id, os, algorithm, architecture, data_folder_path,
        duration, profile, topology_id, associated_experiment)
        VALUES (?,?,?,?,?,?,?,?,?)
    """

    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()

    # Get the ID of the Topology related to the experiment
    #topology_id = get_topology_id(topology, architecture)
    topology_name = "{0}_{1}".format(architecture,topology)
    c.execute("SELECT id FROM Topology WHERE name=?", (topology_name,))
    topology_id = c.fetchall()[0][0]

    # Add the experiment in the database
    c.execute(sql_add_experiment, (experiment_id, os, algorithm,
                                   architecture, data_folder_path,
                                   duration, profile, topology_id, associated_experiment_id))
    conn.commit()

    # Update the original experiment if needed
    print('Update ?')
    if (original_experiment_id > 0):
        print('Update !')
        sql_update_experiment = """
            UPDATE Experiment
            SET associated_experiment=?
            WHERE id=?
        """

        c.execute(sql_update_experiment, (experiment_id, original_experiment_id))
        conn.commit()
    conn.close()

def database_exists():
    print(DATABASE_PATH)
    print(os.path.exists(DATABASE_PATH))
    return os.path.exists(DATABASE_PATH)

def get_nodes(role,topology_name):
    #topology_name = "{0}_{1}".format(architecture,topology)

    sql = """
        SELECT node_id, site
        FROM Node
        WHERE id IN (
            SELECT node_id
            FROM NodeTopology
            WHERE role=? AND topology_id = (
                SELECT id
                FROM Topology
                WHERE name = ?
            )
        )
    """

    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()

    c.execute(sql, (role,topology_name))
    result = c.fetchall()

    conn.close()

    if (role == 'server'):
        return result[0]
    else:
        return result

@Singleton
class DatabaseConnection(object):

    ######################
    #  Global functions  #
    ######################
    def __init__(self):
        self.conn = sqlite3.connect(DATABASE_PATH)
        self.c = self.conn.cursor()

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

    def __str__(self):
        return 'Database connection object'


    ########################
    #   Get from Database  #
    ########################
    def get_experiment_data(self, experiment_id):
        # Request to get the experiment
        sql_get_experiment = """
            SELECT os, architecture,
                (SELECT name
                 FROM Topology
                 WHERE Topology.id=Experiment.topology_id)
                 topology_name,
                 algorithm,
                 duration,
                 profile,
                 data_folder_path,
                 associated_experiment
            FROM Experiment
            WHERE id=?
        """

        # Execute the request and get its first result
        self.c.execute(sql_get_experiment, (experiment_id,))
        experiment_data = self.c.fetchone()

        return experiment_data

    def get_last_experiments(self, os, algorithm, architecture, duration, topology, nb_experiments):
        # Get the topology id
        topology_name = "{0}_{1}".format(architecture,topology)
        self.c.execute("SELECT id FROM Topology WHERE name=?", (topology_name,))
        topology_id = self.c.fetchall()[0][0]

        # Get the X last experiments corresponding to the parameters
        sql_get_experiment = """
            SELECT id
            FROM Experiment
            WHERE algorithm=? AND architecture=? AND duration=? AND topology_id=? AND os=?
            ORDER BY creation DESC
        """
        self.c.execute(sql_get_experiment, (algorithm, architecture, duration, topology_id, os))
        all_experiments_ids = self.c.fetchall()
        experiments_number = min(len(all_experiments_ids),nb_experiments)
        last_experiments_ids = [ all_experiments_ids[i][0] for i in range(experiments_number) ]
        return last_experiments_ids

    def get_nb_measures_for_experiment(self, experiment_id):
        self.c.execute("SELECT COUNT(*) FROM Measure WHERE experiment_id=?", (experiment_id,))
        nb_measures = self.c.fetchone()[0]
        return nb_measures

    def get_nodes(self,role,topology_name):
        #topology_name = "{0}_{1}".format(architecture,topology)

        sql = """
            SELECT node_id, site
            FROM Node
            WHERE id IN (
                SELECT node_id
                FROM NodeTopology
                WHERE role=? AND topology_id = (
                    SELECT id
                    FROM Topology
                    WHERE name = ?
                )
            )
        """

        self.c.execute(sql, (role,topology_name))
        result = self.c.fetchall()

        #print("Role: {0}, topology_name: {1}".format(role, topology_name))
        if (role == 'server'):
            return result[0]
        else:
            return result


    def get_site(self, topology_name):
        return self.get_nodes('server', topology_name)[1]

    def get_mean_power_for_interval(self, experiments_ids, interval_begin, interval_length):
        # For each experiment, get the sum and the number of power measures
        sql_get_sum_count = """
            SELECT SUM(node_power), COUNT(node_power)
            FROM Measure
            WHERE experiment_id=?
            AND node_timestamp>?
            AND node_timestamp<?
        """

        counts = []
        sums = []

        for experiment_id in experiments_ids:
            # Get the first timestamp of the experiment
            first_timestamp = self.get_min_timestamp(experiment_id)

            print("Timestamps")
            print(first_timestamp+interval_begin)
            print(first_timestamp+interval_begin+interval_length)
            # Retrieve the data
            self.c.execute(sql_get_sum_count, (experiment_id,
                                          first_timestamp+interval_begin,
                                          first_timestamp+interval_begin+interval_length))
            (power_sum, power_count) = self.c.fetchone()
            # Add them to our variables
            sums.append(power_sum)
            counts.append(power_count)

        print("Sums:")
        print(sums)
        print("Counts:")
        print(counts)

        print("sum(sums) = {0}".format(sum(sums)))
        print("sum(counts = {0})".format(sum(counts)))
        print("returned = {0}\n".format((sum(sums)/sum(counts))))
        return (sum(sums)/sum(counts))

    def get_total_power_for_experiment(self, experiment_id):
        # Set the request
        sql = """
            SELECT SUM(node_power)
            FROM Measure
            WHERE experiment_id=?
        """
        # Get the total power
        self.c.execute(sql, (experiment_id,))
        return self.c.fetchone()[0]

    def get_traffic_data_from_db(experiment_id):
        sql_get_traffic = """
            SELECT nb_packets, time_interval, sum_packets_lengths
            FROM Traffic
            WHERE experiment_id=?
        """

        # Execute the request
        self.c.execute(sql_get_traffic, (experiment_id,))
        traffic_data = self.c.fetchone()

        traffic_dict = {}
        traffic_dict['nb_packets'] = traffic_data[0]
        traffic_dict['time_interval'] = traffic_data[1]
        traffic_dict['sum_packets_lengths'] = traffic_data[2]

        return traffic_dict

    def has_traffic_data(experiment_id):
        sql_check_existing_traffic = """
            SELECT COUNT(*)
            FROM Traffic
            WHERE experiment_id=?
         """

         # Connect to the database
        (c,conn)=connect()

        c.execute(sql_check_existing_traffic, (experiment_id,))
        traffic_count = c.fetchone()[0]

        # Close the database
        close(conn)

        return (traffic_count > 0)


    def get_min_timestamp(self, experiment_id):
        sql_get_min_timestamp = """
        SELECT node_timestamp
        FROM Measure
        WHERE experiment_id=?
        ORDER BY node_timestamp ASC
        """

        # Retrieve the timestamp
        self.c.execute(sql_get_min_timestamp, (experiment_id,))

        # Get the results
        min_timestamp = self.c.fetchone()[0]
        return min_timestamp

    def get_traffic_measures_for_experiment(self, experiment_id):
        # Request to get the traffic data related to an experiment
        sql_get_traffic = """
            SELECT nb_packets, time_interval, sum_packets_lengths
            FROM Traffic
            WHERE experiment_id=?
        """

        # Get the traffic data
        self.c.execute(sql_get_traffic, (experiment_id,))
        traffic_data = self.c.fetchone()
        return traffic_data

    ##########################
    #  Insert into Database  #
    ##########################

    def save_experiment(self, experiment_id, os, architecture, topology,
                        algorithm, duration, profile, data_folder_path):
        # Request to save the experiment and its metadata
        sql_add_experiment = """
            INSERT INTO Experiment(id, os, algorithm, architecture, data_folder_path,
            duration, profile, topology_id)
            VALUES (?,?,?,?,?,?,?,?)
        """

        # Get the ID of the Topology related to the experiment
        #topology_id = get_topology_id(topology, architecture)
        topology_name = "{0}_{1}".format(architecture,topology)
        self.c.execute("SELECT id FROM Topology WHERE name=?", (topology_name,))
        topology_id = self.c.fetchall()[0][0]

        # Add the experiment in the database
        self.c.execute(sql_add_experiment, (experiment_id, os, algorithm,
                                            architecture, data_folder_path,
                                            duration, profile, topology_id))
        self.conn.commit()

    def save_measure_without_commit(self, node_timestamp, node_power, node_id, experiment_id):
        # Request to insert the measure
        sql_add_measure = """
            INSERT INTO Measure
            VALUES (?,?,?,?)
        """
        # Execute it
        self.c.execute(sql_add_measure,
                       (node_timestamp, node_power, node_id, experiment_id))

    def save_traffic(self, experiment_id, nb_packets, time_interval_in_seconds, sum_packets_lengths):
        # Request to save the traffic data related to an experiment
        sql_add_traffic_measure = """
            INSERT INTO Traffic(nb_packets, time_interval, sum_packets_lengths, experiment_id)
            VALUES (?,?,?,?)
        """

        # Execute and commit the request
        self.c.execute(sql_add_traffic_measure,
                       (nb_packets, time_interval_in_seconds,
                        sum_packets_lengths, experiment_id))
        self.conn.commit()

    def add_lim_packets_per_second(self, experiment_id, lim_packets_per_second):

        sql_get_latency_id = """
            SELECT id
            FROM Latency
            WHERE experiment_id=?
        """

        sql_add_new_latency = """
            INSERT INTO Latency(experiment_id, lim_packets_per_second)
            VALUES (?,?)
        """

        sql_update_latency = """
            UPDATE Latency
            SET lim_packets_per_second=?
            WHERE experiment_id=?
        """

        # Get the ID of the latency entry if it exists
        self.c.execute(sql_get_latency_id, (experiment_id,))
        latency_id_response = self.c.fetchone()
        if (latency_id_response == None):
            # Create the entry
            self.c.execute(sql_add_new_latency, (experiment_id, lim_packets_per_second))
            self.conn.commit()
        else:
            # Update the value of lim_packets_per_second
            latency_id = latency_id_response[0]
            self.c.execute(sql_update_latency, (lim_packets_per_second, experiment_id))
            self.conn.commit()

    def add_packet_loss(self, experiment_id, loss_rate):

        sql_get_latency_id = """
            SELECT id
            FROM Latency
            WHERE experiment_id=?
        """

        sql_add_new_latency = """
            INSERT INTO Latency(experiment_id, loss_rate)
            VALUES (?,?)
        """

        sql_update_latency = """
            UPDATE Latency
            SET loss_rate=?
            WHERE experiment_id=?
        """

        # Get the ID of the latency entry if it exists
        self.c.execute(sql_get_latency_id, (experiment_id,))
        latency_id_response = self.c.fetchone()
        if (latency_id_response == None):
            # Create the entry
            self.c.execute(sql_add_new_latency, (experiment_id, loss_rate))
            self.conn.commit()
        else:
            # Update the value of loss_rate
            latency_id = latency_id_response[0]
            self.c.execute(sql_update_latency, (loss_rate, experiment_id))
            self.conn.commit()

    def add_mean_latency(self, experiment_id, mean_latency):

        sql_get_latency_id = """
            SELECT id
            FROM Latency
            WHERE experiment_id=?
        """

        sql_add_new_latency = """
            INSERT INTO Latency(experiment_id, mean_latency)
            VALUES (?,?)
        """

        sql_update_latency = """
            UPDATE Latency
            SET mean_latency=?
            WHERE experiment_id=?
        """

        # Get the ID of the latency entry if it exists
        self.c.execute(sql_get_latency_id, (experiment_id,))
        latency_id_response = self.c.fetchone()
        if (latency_id_response == None):
            # Create the entry
            self.c.execute(sql_add_new_latency, (experiment_id, mean_latency))
            self.conn.commit()
        else:
            # Update the value of mean_latency
            latency_id = latency_id_response[0]
            self.c.execute(sql_update_latency, (mean_latency, experiment_id))
            self.conn.commit()

    def get_from_latency(self, experiment_id, data_type):
        if (data_type == 'lim_packets_per_second'):
            sql_get_from_latency = """
                SELECT lim_packets_per_second
                FROM Latency
                WHERE experiment_id=?
            """
        elif (data_type == 'loss_rate'):
            sql_get_from_latency = """
                SELECT loss_rate
                FROM Latency
                WHERE experiment_id=?
            """

        elif (data_type == 'mean_latency'):
            sql_get_from_latency = """
                SELECT mean_latency
                FROM Latency
                WHERE experiment_id=?
            """
        else:
            # TODO
            print('[Database] Error')

        self.c.execute(sql_get_from_latency, (experiment_id,))
        value = self.c.fetchone()
        return value[0]

    def create_all_tables(self):
        # -- Table creation -- #
        # Create the Topology table
        self.c.execute('CREATE TABLE Topology (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)')

        # Create the Node table
        self.c.execute('CREATE TABLE Node (id INTEGER PRIMARY KEY AUTOINCREMENT, node_id INTEGER, site TEXT, architecture TEXT)')

        # Create the NodeTopology table to link the nodes to a topology by attributing a role
        self.c.execute('CREATE TABLE NodeTopology (id INTEGER PRIMARY KEY AUTOINCREMENT, role TEXT, topology_id INTEGER NOT NULL, node_id INTEGER NOT NULL, FOREIGN KEY(topology_id) REFERENCES Topology(id), FOREIGN KEY(node_id) REFERENCES Node(id))')
        # Create the Experiment table
        # TODO: keep architecture and site, or let the Node table keep the information?
        # TODO: timestamp
        self.c.execute('CREATE TABLE Experiment (id INTEGER PRIMARY KEY, os TEXT, algorithm TEXT, architecture TEXT, data_folder_path TEXT, duration INTEGER, profile TEXT, creation DATETIME DEFAULT CURRENT_TIMESTAMP, topology_id INTEGER NOT NULL, associated_experiment INTEGER, FOREIGN KEY(topology_id) REFERENCES Topology(id), FOREIGN KEY(associated_experiment) REFERENCES Experiment(id))')


        # Create the latency table
        self.c.execute('CREATE TABLE Latency (id INTEGER PRIMARY KEY AUTOINCREMENT, lim_packets_per_second FLOAT, loss_rate FLOAT, mean_latency FLOAT, experiment_id INTEGER, FOREIGN KEY(experiment_id) REFERENCES Experiment(id))')

        # Create the measures table
        self.c.execute('CREATE TABLE Measure (node_timestamp FLOAT, node_power FLOAT, node_id TEXT, experiment_id INTEGER, FOREIGN KEY(experiment_id) REFERENCES Experiment(id))')

        # Create the table to store traffic measures
        self.c.execute('CREATE TABLE Traffic (id INTEGER PRIMARY KEY AUTOINCREMENT, nb_packets INTEGER, time_interval INTEGER, sum_packets_lengths FLOAT, experiment_id INTEGER, FOREIGN KEY(experiment_id) REFERENCES Experiment(id))')

        # -- Table population -- #
        nodes_dict = {
            'm3': {
                'line': {
                    'grenoble': {
                        'server': '358',
                        'clients': ['318', '326', '334', '342', '350']
                    }
                },
                'grid': {
                    'lille': {
                        'server': '32',
                        'clients': ['21', '30', '40', '63', '73']
                    }
                },
                'test2': {
                    'grenoble': {
                        'server': '101',
                        'clients': ['102']
                    }
                }
            }
        } # TODO: star

        sql_add_topology = """
            INSERT INTO Topology(name)
            VALUES (?)
        """

        sql_add_node = """
            INSERT INTO Node(node_id, site, architecture)
            VALUES (?,?,?)
        """

        sql_add_node_topology = """
            INSERT INTO NodeTopology(role, topology_id, node_id)
            VALUES (?,?,?)
        """

        for architecture in nodes_dict:

            # Populate Topology
            for topology in nodes_dict[architecture]:
                topology_name = "{0}_{1}".format(architecture,topology)
                # Check if the topology exists
                self.c.execute("SELECT id FROM Topology WHERE name=?", (topology_name,))
                topology_ids = self.c.fetchall()
                # Add the topology if it does not exist
                if (len(topology_ids) == 0):
                    self.c.execute(sql_add_topology, (topology_name,))
                    self.conn.commit()
                # Get the topology ID
                self.c.execute("SELECT id FROM Topology WHERE name=?", (topology_name,))
                topology_id = self.c.fetchall()[0][0]

                # Populate Node
                for site in nodes_dict[architecture][topology]:
                    # Server
                    server_id = nodes_dict[architecture][topology][site]['server']
                    # Check if the node exists
                    self.c.execute("SELECT id FROM Node WHERE node_id=? AND site=? AND architecture=?", (server_id,site,architecture))
                    nodes_ids = self.c.fetchall()
                    # Add the node if it does not exist
                    if (len(nodes_ids) == 0):
                        self.c.execute(sql_add_node, (server_id,site,architecture))
                        self.conn.commit()
                    # Get the node ID
                    self.c.execute("SELECT id FROM Node WHERE node_id=? AND site=? AND architecture=?", (server_id,site,architecture))
                    node_id = self.c.fetchall()[0][0]
                    # Add the NodeTopology relation
                    self.c.execute(sql_add_node_topology, ('server',topology_id, node_id))
                    self.conn.commit()


                    # Clients
                    for client_id in nodes_dict[architecture][topology][site]['clients']:
                        # Check if the node exists
                        self.c.execute("SELECT id FROM Node WHERE node_id=? AND site=? AND architecture=?", (client_id,site,architecture))
                        nodes_ids = self.c.fetchall()
                        # Add the node if it does not exist
                        if (len(nodes_ids) == 0):
                            self.c.execute(sql_add_node, (client_id,site,architecture))
                            self.conn.commit()
                        # Get the node ID
                        self.c.execute("SELECT id FROM Node WHERE node_id=? AND site=? AND architecture=?", (client_id,site,architecture))
                        node_id = self.c.fetchall()[0][0]
                        # Add the NodeTopology relation
                        self.c.execute(sql_add_node_topology, ('client',topology_id,node_id))
                        self.conn.commit()
