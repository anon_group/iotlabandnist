#!/usr/bin/env python3
from enum import Enum
import json
import database as Database
from pathlib import Path
import common
import shutil

#############
# Functions #
#############


## Where to find the local data

## Retrieve the path of the firmware
#
# @param firmware_type The name of the firmware to use
# @param node_type Type of the nodes to useclient_nodes
# @param architecture Architecture of the node to use
# @param os Operating System of the node to use
def get_firmware_path(firmware_type, node_type, architecture, os):
    return "firmwares/"+os.__str__()+"/"+architecture.__str__()+"-"+firmware_type+"-"+node_type+".iotlab"

## Retrieve the path of the file containing the JSON description of an experiment
#
# @param experiment_id The ID of the experiment described in the JSON file
def get_json_description_path(experiment_id):
    return 'data/{0}/experiment.json'.format(experiment_id)

def get_json_traffic_data_path(experiment_id):
    return 'data/{0}/traffic.json'.format(experiment_id)

## TODO
def get_experiment_folder_path(experiment_id):
    return 'data/{0}'.format(experiment_id)

## TODO
def get_graph_path(experiments_ids, measure, file_type):
    return get_plot_path(experiments_ids, measure, 'graph', file_type)

def get_default_graph_path(value_type, file_type):
    return 'data/{0}_graph.{1}'.format(value_type, file_type)

## TODO
def get_boxplot_path(experiments_ids, measure, file_type):
    print('Boxplot path = {}'.format(get_plot_path(experiments_ids, measure, 'boxplot', file_type)))
    return get_plot_path(experiments_ids, measure, 'boxplot', file_type)

def get_plot_path(experiments_ids, measure, plot_type, file_type):
    export_directory = 'data/{0}/analytics/measures/'.format('-'.join(map(str,experiments_ids)))
    Path(export_directory).mkdir(parents=True, exist_ok=True) # Create the directories if they not exist
    return '{0}/{1}-{2}.{3}'.format(export_directory, measure, plot_type, file_type)

## Retrieve the path of a PCAP file for an experiment
#
# @param experiment_id The ID of the experiment from which the PCAP is retrieved
# @param node_id The ID of the node associated to the PCAP file
def get_pcap_path(experiment_id, node_id):
    return 'data/{0}/{1}.pcap'.format(experiment_id, node_id)

def get_serial_aggregator_log_path(experiment_id):
    return 'data/{0}/serial_aggregator.log'.format(experiment_id)

## Retrieve the path of the PCAP file associated to the server of an experiment
#
# @param experiment_id The ID of the experiment associated to the PCAP
def get_server_pcap_path(experiment_id):
    return get_pcap_path(experiment_id, 'server')


## IoT-LAB related data

## Get the name of the site containing the nodes on IoT-LAB
#
# @param architecture The architecture of the nodes
# @param topology The topology of the nodes
def get_site(architecture, topology):
    topology_name = '{0}_{1}'.format(architecture, topology)
    db = Database.DatabaseConnection.Instance()
    return db.get_site(topology_name)

## Get the list of the client nodes
#
# @param architecture The architecture of the nodes
# @param topology The topology of the nodes
def get_client_nodes(architecture, topology):
    db = Database.DatabaseConnection.Instance()
    topology_name = '{0}_{1}'.format(architecture, topology)
    nodes_tuples = db.get_nodes('client',topology_name)
    clients_nodes = []
    for node in nodes_tuples:
        (node_id, site) = node
        clients_nodes.append('{0}-{1}.{2}.iot-lab.info'.format(architecture, node_id, site))
    return clients_nodes


## Get the list of the client nodes' names
#
# @param architecture The architecture of the nodes
# @param topology The topology of the nodes
def get_client_nodes_names(architecture, topology_name):
    db = Database.DatabaseConnection.Instance()
    #topology_name = '{0}_{1}'.format(architecture, topology)
    #print("TMP: TOPOLOGY: {0}".format(topology_name))
    nodes_tuples = db.get_nodes('client', topology_name)
    clients_nodes_names = []
    for node in nodes_tuples:
        (node_id, site) = node
        clients_nodes_names.append('{0}_{1}'.format(architecture, node_id))
    return clients_nodes_names

def get_client_nodes_ids(architecture, topology):
    topology_name = '{0}_{1}'.format(architecture, topology)

    #db = Database.DatabaseConnection.Instance() # TODO: Soline3108
    #nodes_tuples = db.get_clients_nodes(architecture, topology)
    nodes_tuples = Database.get_nodes('client',topology_name)
    clients_nodes_ids = []
    for node in nodes_tuples:
        node_id = node[0]
        #(node_id, site) = node
        clients_nodes_ids.append(node_id)
    return clients_nodes_ids

def get_server_id_number(topology_name):
    db = Database.DatabaseConnection.Instance()
    #topology_name = '{0}_{1}'.format(architecture, topology)
    #print("TMP: TOPOLOGY: {0}".format(topology_name))
    (node_id, site) = db.get_nodes('server', topology_name)
    return node_id

def get_clients_ids_numbers(topology_name):
    db = Database.DatabaseConnection.Instance()
    #topology_name = '{0}_{1}'.format(architecture, topology)
    #print("TMP: TOPOLOGY: {0}".format(topology_name))
    nodes_tuples = db.get_nodes('client', topology_name)
    clients_ids_numbers = []
    for node in nodes_tuples:
        (node_id, site) = node
        clients_ids_numbers.append(node_id)
    return clients_ids_numbers

## Get the list of the server nodes
#
# @param architecture The architecture of the nodes
# @param topology The topology of the nodes
def get_server_node(architecture, topology):
    db = Database.DatabaseConnection.Instance()
    topology_name = '{0}_{1}'.format(architecture, topology)
    (node_id, site) = db.get_nodes('server', topology_name)
    return "{0}-{1}.{2}.iot-lab.info".format(architecture, node_id, site)

## Get the list of the server nodes' names
#
# @param architecture The architecture of the nodes
# @param topology The topology of the nodes
def get_server_node_name(architecture, topology):
    db = Database.DatabaseConnection.Instance()
    node_id = db.get_nodes('server', topology)[0]
    return "{0}_{1}".format(architecture, node_id)


def get_server_node_id(architecture,topology):
    topology_name = '{0}_{1}'.format(architecture, topology)

    #db = Database.DatabaseConnection.Instance() # TODO: Soline3108
    node_id = Database.get_nodes('server',topology_name)[0]

    #node_id = db.get_server_node(architecture, topology)[0]
    return node_id

def get_measures():
    return common.measures_dict(
        ('power', float, 'Power (W)'),
        ('voltage', float, 'Voltage (V)'),
        ('current', float, 'Current (A)')
    )

def get_default_firmware_folder_path(experiment):
    firmware_folder = 'iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/'
    #packets_length = '512' # TODO
    print(experiment)
    print(experiment.architecture)
    print(experiment.algorithm)
    #input_folder_name = '{0}-{1}-0'.format(experiment.architecture, experiment.algorithm)
    input_folder_name = '{0}'.format(experiment.algorithm)
    return '{0}/{1}'.format(firmware_folder, input_folder_name)

def get_custom_firmware_folder_path(experiment, time_between_packets):
    firmware_folder = 'iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/'
    #packets_length = '512' # TODO

    # Create the folder
    output_folder_name = '{0}-{1}-{2}'.format(experiment.architecture, experiment.algorithm, time_between_packets)
    output_folder = '{0}/{1}'.format(firmware_folder, output_folder_name)
    Path(output_folder).mkdir(parents=True, exist_ok=True)

    return output_folder

def store_lim_packets_per_second(experiment_id, lim_packets_per_second):
    db = Database.DatabaseConnection.Instance()
    db.add_lim_packets_per_second(experiment_id, lim_packets_per_second)

def store_packets_loss(experiment_id, loss_rate):
    db = Database.DatabaseConnection.Instance()
    db.add_packet_loss(experiment_id, loss_rate)

def store_mean_latency(experiment_id, mean_latency):
    db = Database.DatabaseConnection.Instance()
    db.add_mean_latency(experiment_id, mean_latency)

def get_from_latency(experiment_id, data_type):
    # data_type can be :
    # - lim_packets_per_second
    # - loss_rate
    # - mean_latency
    # TODO: use switch case
    if (data_type == 'lim_packets_per_second' or 'loss_rate' or 'mean_latency'):
        db = Database.DatabaseConnection.Instance()
        return db.get_from_latency(experiment_id, data_type)
    else:
        # TODO
        print('[Utils - get_from_latency] Error')

###########
# Classes #
###########

## Class Algorithm
# Enumeration listing the kinds of Algorithms that can be used
class Algorithm(Enum):
    simple_app = 'simple-app'
    auth_encrypt = 'auth-encrypt'
    lilliput_ae = 'lilliput-ae'
    skinny_tk3 = 'skinnytk3'
    ascon_ae = 'ascon-ae'
    skinny = 'skinny'
    ascon_a_ae = 'ascon-a-ae'
    sparkle = 'sparkle'
    pyjamask = 'pyjamask'
    subterranean = 'subterranean'
    no_enc = 'no-enc'

    # Latency
    no_enc_128_logs = 'no-enc-128-logs'

    # Tests
    no_enc_128_0 = "no-enc-128-0"
    no_enc_256_0 = "no-enc-256-0"
    no_enc_512_0 = "no-enc-512-0"
    no_enc_128_0_logs = "no-enc-128-0-logs"
    no_enc_512_0_logs = "no-enc-512-0-logs"

    aes_ccm_128_0 = "aes-ccm-128-0-logs"

    gift_cofb_128_0 = "gift-cofb-128-0"
    gift_cofb_256_0 = "gift-cofb-256-0"
    gift_cofb_512_0 = "gift-cofb-512-0"
    gift_cofb_128_0_logs = "gift-cofb-128-0-logs"
    gift_cofb_512_0_logs = 'gift-cofb-512-0-logs'

    ascon_a_ae_128_0 = "ascon-a-ae-128-0"
    ascon_a_ae_256_0 = "ascon-a-ae-256-0"
    ascon_a_ae_512_0 = "ascon-a-ae-512-0"
    ascon_a_ae_128_0_logs = "ascon-a-ae-128-0-logs"
    ascon_a_ae_512_0_logs = "ascona128-512-0-logs"
    ascona128_128_0_logs = "ascona128-128-0-logs"

    hyena_128_0 = "hyena-128-0"
    hyena_256_0 = "hyena-256-0"
    hyena_512_0 = "hyena-512-0"
    hyena_128_0_logs = "hyena-128-0-logs"
    hyena_512_0_logs = "hyena-512-0-logs"

    pyjamask_128_0 = "pyjamask-128-0"
    pyjamask_256_0 = "pyjamask-256-0"
    pyjamask_512_0 = "pyjamask-512-0"
    pyjamask_128_0_logs = "pyjamask-128-0-logs"
    pyjamask_512_0_logs = "pyjamask-512-0-logs"

    sparkle_128_0 = "sparkle-128-0"
    sparkle_256_0 = "sparkle-256-0"
    sparkle_512_0 = "sparkle-512-0"
    sparkle_128_0_logs = "sparkle-128-0-logs"
    sparkle_512_0_logs = "sparkle-512-0-logs"

    subterranean_128_0 = "subterranean-128-0"
    subterranean_256_0 = "subterranean-256-0"
    subterranean_512_0 = "subterranean-512-0"
    subterranean_128_0_logs = "subterranean-128-0-logs"
    subterranean_512_0_logs = "subterranean-512-0-logs"

    sundae_gift_128_0 = "sundae-gift-128-0"
    sundae_gift_256_0 = "sundae-gift-256-0"
    sundae_gift_512_0 = "sundae-gift-512-0"
    sundae_gift_128_0_logs = "sundae-gift-128-0-logs"
    sundae_gift_512_0_logs = "sundae-gift-512-0-logs"

    lilliput_128_0 = "lilliput-ae-128-0"
    lilliput_256_0 = "lilliput-ae-256-0"
    lilliput_512_0 = "lilliput-ae-512-0"
    lilliput_ae_128_0_logs = "lilliput-ae-128-0-logs"
    lilliput_ae_512_0_logs = "lilliput-512-0-logs"

    skinny_128_0 = "skinny-128-0"
    skinny_256_0 = "skinny-256-0"
    skinny_512_0 = "skinny-512-0"
    skinny_128_0_logs = "skinny-128-0-logs"
    skinny_512_0_logs = "skinny-512-0-logs"

    grain_128_0 = "grain-128-0"
    grain_256_0 = "grain-256-0"
    grain_512_0 = "grain-512-0"
    grain_128_0_logs = "grain-128-0-logs"
    grain_512_0_logs = "grain-512-0-logs"

    locus_128_0 = "locus-128-0"
    locus_256_0 = "locus-256-0"
    locus_512_0 = "locus-512-0"
    locus_128_0_logs = "locus-128-0-logs"
    locus_512_0_logs = "locus-512-0-logs"

    lotus_128_0 = "lotus-128-0"
    lotus_256_0 = "lotus-256-0"
    lotus_512_0 = "lotus-512-0"
    lotus_128_0_logs = "lotus-128-0-logs"
    lotus_512_0_logs = "lotus-512-0-logs"


    def __str__(self):
        return self.value

## Class Architecture
# Enumeration listing what kind of architecture can be used
class Architecture(Enum):
    m3 = 'm3'

    def __str__(self):
        return self.value

## Class OperatingSystem
# Enumeration listing what kind of operating system can be used
class OperatingSystem(Enum):
    contiki3 = 'contiki-3'
    contiking = 'contiki-ng'
    riot = 'riot'

    def __str__(self):
        return self.value

## Class Operation
# Enumeration listing the kinds of Operation that can be used
class Operation(Enum):
    launch_only = 'launch-only'
    display_only = 'analyse-only'
    launch_and_display = 'launch-and-analyse'
    analyse_several = 'analyse-several'

    def __str__(self):
        return self.value

## Class ProfileType
# Enumeration listing the kinds of Profile that can be used
class ProfileType(Enum):
    consumption_and_rssi = 'consumption_and_rssi'
    consumption_and_sniffer = 'consumption_and_sniffer'

    def __str__(self):
        return self.value

## Class Topology
# Enumeration listing what kind of topology can be used
class Topology(Enum):
    line = 'line'
    grid = 'grid'
    star = 'star'
    test = 'test'
    test2 = 'test2'
    test3 = 'test3'
    test4 = 'test4'
    ministar = 'ministar'

    def __str__(self):
        return self.value

## Class DataProcessing
# Enumeration listing the keywords defining in the configuration file
# how to process data
class DataProcessing(Enum):
    cut_percentage = 'cut-percentage'
    cut_minutes = 'cut-minutes'

    def __str__(self):
        return self.value
