#/bin/bash
git clone https://github.com/iot-lab/iot-lab.git
cd iot-lab
make setup-iot-lab-contiki-ng
cd ..
cp firmwares-code/Makefile.include iot-lab/parts/iot-lab-contiki-ng/contiki-ng/Makefile.include
mkdir iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria

# Copy the no-enc algorithm
cp -r firmwares-code/no-enc-128-0-logs/example/ iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/no-enc-128-0-logs # Has only an 'example' folder, and no 'lib'

# TODO: Copy the clients/servers and libraries of the other algorithms
for algorithm in 'skinny-128-0-logs' 'lilliput-128-0-logs' 'ascona128-128-0-logs' 'gift-cofb-128-0-logs' 'grain-128-0-logs' 'hyena-128-0-logs' 'locus-128-0-logs' 'lotus-128-0-logs' 'pyjamask-128-0-logs' 'sparkle-128-0-logs' 'subterranean-128-0-logs' 'sundae-gift-128-0-logs'
do
	echo $algorithm
	# Copy the library
	cp -r firmwares-code/$algorithm/library/* iot-lab/parts/iot-lab-contiki-ng/contiki-ng/os/lib/
	cp -r firmwares-code/$algorithm/example/ iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/$algorithm
done

# Create the Docker

#cd ..
#cp scripts/docker/Dockerfile ./
#docker build -t test .
#rm Dockerfile

