#for algorithm in 'skinny-128-0-logs' 'lilliput-128-0-logs' 'ascona128-128-0-logs' 'gift-cofb-128-0-logs' 'grain-128-0-logs' 'hyena-128-0-logs' 'locus-128-0-logs' 'lotus-128-0-logs' 'pyjamask-128-0-logs' 'sparkle-128-0-logs' 'subterranean-128-0-logs' 'sundae-gift-128-0-logs'
algorithms=( 'ascona128-512-0-logs' 'gift-cofb-512-0-logs' 'grain-512-0-logs' 'hyena-512-0-logs' 'lilliput-512-0-logs' 'locus-512-0-logs' 'lotus-512-0-logs' 'no-enc-512-0-logs' 'pyjamask-512-0-logs' 'skinny-512-0-logs' 'sparkle-512-0-logs' 'subterranean-512-0-logs' 'sundae-gift-512-0-logs' )

for algorithm in ${algorithms[@]}
do
        echo $algorithm
        # Copy the library
        cp -r firmwares-code/$algorithm/library/* iot-lab/parts/iot-lab-contiki-ng/contiki-ng/os/lib/
        cp -r firmwares-code/$algorithm/example/ iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/$algorithm

done

cd iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria
#for algorithm in 'skinny-128-0-logs' 'lilliput-128-0-logs' 'ascona128-128-0-logs' 'gift-cofb-128-0-logs' 'grain-128-0-logs' 'hyena-128-0-logs' 'locus-128-0-logs' 'lotus-128-0-logs' 'pyjamask-128-0-logs' 'sparkle-128-0-logs' 'subterranean-128-0-logs' 'sundae-gift-128-0-logs'
for algorithm in ${algorithms[@]}
do
        echo $algorithm
	# Go to the example directory associated to this algorithm
	cd $algorithm
        # Compile the firmware 
        ARCH_PATH=../../../../arch make TARGET=iotlab BOARD=m3 savetarget
	ARCH_PATH=../../../../arch make
	# Go back to the inria examples directory
	cd ..
done

cd ../../../../../../

for algorithm in ${algorithms[@]}
do
	cp iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/$algorithm/client.iotlab firmwares/contiki-ng/m3-$algorithm-client.iotlab
	cp iot-lab/parts/iot-lab-contiki-ng/contiki-ng/examples/inria/$algorithm/server.iotlab firmwares/contiki-ng/m3-$algorithm-server.iotlab
done
