/*
 * HYENA_GIFT-128
 * 
 * 
 * HYENA_GIFT-128 is a nonce-based AEAD based on Hybrid Feedback mode of
 * operation and GIFT-128 block cipher.
 * 
 * Test Vector (in little endian format):
 * Key	: 0f 0e 0d 0c 0b 0a 09 08 07 06 05 04 03 02 01 00
 * PT 	:
 * AD	: 
 * CT	: 
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "hyena.h"


/////////////////////////gift-128.c///////////////////////////////
/*
 * GIFT-128
 * 
 * GIFT-128 is a lightweight block cipher (by Banik et al. [1,2]). We
 * have reused the reference code for GIFT-128-128 (available at [3]) by
 * the GIFT team.
 * 
 * 1. https://link.springer.com/chapter/10.1007/978-3-319-66787-4_16
 * 2. https://eprint.iacr.org/2017/622.pdf
 * 3. https://sites.google.com/view/giftcipher/ 
 * 
 * The main modification from our side is some minor code cleaning and
 * restructuring in order to make the code consistent with our
 * notations and conventions.
 * 
 */


/* 
 * Sbox and its inverse
 */ 
const u8 _gift_sbox[16] = { 1, 10, 4, 12, 6, 15, 3, 9, 2, 13, 11, 7, 5, 0, 8, 14 };
const u8 _gift_sbox_inv[16] = { 13, 0, 8, 6, 2, 12, 4, 11, 14, 7, 1, 10, 3, 9, 15, 5 };

/*
 * bit permutation and its inverse
 */ 
const u8 _gift_perm[] = {
	0, 33, 66, 99, 96, 1, 34, 67, 64, 97, 2, 35, 32, 65, 98, 3,
	4, 37, 70, 103, 100, 5, 38, 71, 68, 101, 6, 39, 36, 69, 102, 7,
	8, 41, 74, 107, 104,  9, 42, 75, 72, 105, 10, 43, 40, 73, 106, 11,
	12, 45, 78, 111, 108, 13, 46, 79, 76, 109, 14, 47, 44, 77, 110, 15,
	16, 49, 82, 115, 112, 17, 50, 83, 80, 113, 18, 51, 48, 81, 114, 19,
	20, 53, 86, 119, 116, 21, 54, 87, 84, 117, 22, 55, 52, 85, 118, 23,
	24, 57, 90, 123, 120, 25, 58, 91, 88, 121, 26, 59, 56, 89, 122, 27,
	28, 61, 94, 127, 124, 29, 62, 95, 92, 125, 30, 63, 60, 93, 126, 31
};
const u8 _gift_perm_inv[] = {
	0, 5, 10, 15, 16, 21, 26, 31, 32, 37, 42, 47, 48, 53, 58, 63,
	64, 69, 74, 79, 80, 85, 90, 95, 96, 101, 106, 111, 112, 117, 122, 127,
	12, 1, 6, 11, 28, 17, 22, 27, 44, 33, 38, 43, 60, 49, 54, 59,
	76, 65, 70, 75, 92, 81, 86, 91, 108, 97, 102, 107, 124, 113, 118, 123,
	8, 13, 2, 7, 24, 29, 18, 23, 40, 45, 34, 39, 56, 61, 50, 55,
	72, 77, 66, 71, 88, 93, 82, 87,104,109, 98,103,120,125,114,119,
	4, 9, 14, 3, 20, 25, 30, 19, 36, 41, 46, 35, 52, 57, 62, 51,
	68, 73, 78, 67, 84, 89, 94, 83, 100, 105, 110, 99, 116, 121, 126, 115
};

/*
 * round constants
 */ 
const u8 _gift_round_constants[62] = {
    0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
    0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
    0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
    0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A,
    0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13,
    0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a, 0x15, 0x2a, 0x14, 0x28,
    0x10, 0x20
};

/**********************************************************************
 * 
 * @name	:	bytes_to_nibbles
 * 
 * @note	:	Convert byte oriented "src" to nibble oriented "dest".
 * 
 **********************************************************************/
void bytes_to_nibbles(u8 *dest, const u8 *src, u8 src_len)
{
    u8 i;
    for(i=0; i<src_len; i++)
    {
        dest[2*i] = src[i]&0xF;
        dest[2*i+1] = (src[i]&0xF0)>>4;
    }
}

/**********************************************************************
 * 
 * @name	:	nibbles_to_bytes
 * 
 * @note	:	Convert nibble oriented "src" to byte oriented "dest".
 * 
 **********************************************************************/
void nibbles_to_bytes(u8 *dest, const u8 *src, u8 dest_len)
{
    u8 i;
    for(i=0; i<dest_len; i++)
	{
		dest[i] = src[2*i+1]<<4 | src[2*i];
	}
}

/**********************************************************************
 * 
 * @name	:	nibbles_to_bits
 * 
 * @note	:	Convert nibble oriented "src" to bit oriented "dest".
 * 
 **********************************************************************/
void nibbles_to_bits(u8 *dest, const u8 *src, u8 src_len)
{
        u8 i;
        u8 j;
	for(i=0; i<src_len; i++)
	{
		for(j=0; j<4; j++)
		{
			dest[4*i+j] = (src[i] >> j) & 0x1;
		}
	}
}

/**********************************************************************
 * 
 * @name	:	bits_to_nibbles
 * 
 * @note	:	Convert bit oriented "src" to nibble oriented "dest".
 * 
 **********************************************************************/
void bits_to_nibbles(u8 *dest, const u8 *src, u8 dest_len)
{
    u8 i;
    u8 j;
    for(i=0; i<dest_len; i++)
	{
		dest[i]=0;
		for(j=0; j<4; j++)
		{
			 dest[i] ^= src[4*i+j] << j;
		}
	}
}

/**********************************************************************
 * 
 * @name	:	generate_round_keys
 * 
 * @note	:	Generate and store the round key nibbles using the
 * 				master key.
 * 
 **********************************************************************/
void generate_round_keys(u8 (*round_key_nibbles)[32], const u8 *key_bytes)
{
	u8 key_nibbles[32];
	//memcpy(&key_nibbles_[0], &key_nibbles[0], 32);
	
	// convert master key from byte-oriented
	// to nibble-oriented
	bytes_to_nibbles(key_nibbles, key_bytes, 16);
	
	//copy the first round key
        u8 i;
	for (i=0; i<32; i++)
	{
		round_key_nibbles[0][i] = key_nibbles[i];
	}
	
	// update round key and store the rest of the round keys
	u8 temp[32];
        u8 r;
	for (r=1; r<CRYPTO_BC_NUM_ROUNDS; r++)
	{
		//key update
		//entire key>>32
		for(i=0; i<32; i++)
		{
			temp[i] = key_nibbles[(i+8)%32];
		}
		for(i=0; i<24; i++)
		{
			key_nibbles[i] = temp[i];
		}
		//k0>>12
		key_nibbles[24] = temp[27];
		key_nibbles[25] = temp[24];
		key_nibbles[26] = temp[25];
		key_nibbles[27] = temp[26];
		//k1>>2
		key_nibbles[28] = ((temp[28]&0xc)>>2) ^ ((temp[29]&0x3)<<2);
		key_nibbles[29] = ((temp[29]&0xc)>>2) ^ ((temp[30]&0x3)<<2);
		key_nibbles[30] = ((temp[30]&0xc)>>2) ^ ((temp[31]&0x3)<<2);
		key_nibbles[31] = ((temp[31]&0xc)>>2) ^ ((temp[28]&0x3)<<2);
		
		//copy the key state
		for (i=0; i<32; i++)
		{
			round_key_nibbles[r][i] = key_nibbles[i];
		}
	}
}

/**********************************************************************
 * 
 * @name	:	sub_cells
 * 
 * @note	:	SubCells operation.
 * 
 **********************************************************************/
void sub_cells(u8 *state_nibbles)
{
        u8 i;
	for(i=0; i<32; i++)
	{
		state_nibbles[i] = _gift_sbox[state_nibbles[i]];
	}
}

/**********************************************************************
 * 
 * @name	:	sub_cells_inv
 * 
 * @note	:	Inverse SubCells operation.
 * 
 **********************************************************************/
void sub_cells_inv(u8 *state_nibbles)
{
        u8 i;
	for(i=0; i<32; i++)
	{
		state_nibbles[i] = _gift_sbox_inv[state_nibbles[i]];
	}
}

/**********************************************************************
 * 
 * @name	:	perm_bits
 * 
 * @note	:	PermBits operation.
 * 
 **********************************************************************/
void perm_bits(u8 *state_bits)
{	
	//nibbles to bits
	u8 bits[128];
	
	//permute the bits
        u8 i;
	for(i=0; i<128; i++)
	{
		bits[_gift_perm[i]] = state_bits[i];
	}
	memcpy(&state_bits[0], &bits[0], 128);
}

/**********************************************************************
 * 
 * @name	:	perm_bits_inv
 * 
 * @note	:	Inverse PermBits operation.
 * 
 **********************************************************************/
void perm_bits_inv(u8 *state_bits)
{	
	//nibbles to bits
	u8 bits[128];
	
	//permute the bits
        u8 i;
	for(i=0; i<128; i++)
	{
		bits[_gift_perm_inv[i]] = state_bits[i];
	}
	memcpy(&state_bits[0], &bits[0], 128);
}

/**********************************************************************
 * 
 * @name	:	add_round_key
 * 
 * @note	:	Add round key operation.
 * 
 **********************************************************************/
void add_round_key(u8 *state_bits, const u8 *round_key_nibbles)
{		
	//round key nibbles to bits
	u8 key_bits[128];
	nibbles_to_bits(&key_bits[0], &round_key_nibbles[0], 32);

	//add round key
        u8 i;
	for (i=0; i<32; i++){
		state_bits[4*i+1] ^= key_bits[i];
		state_bits[4*i+2] ^= key_bits[i+64];
	}
}

/**********************************************************************
 * 
 * @name	:	add_round_constant
 * 
 * @note	:	Add round constant operation.
 * 
 **********************************************************************/
void add_round_constants(u8 *state_bits, u8 round)
{
	//add constant
	state_bits[3] ^= _gift_round_constants[round] & 0x1;
	state_bits[7] ^= (_gift_round_constants[round]>>1) & 0x1;
	state_bits[11] ^= (_gift_round_constants[round]>>2) & 0x1;
	state_bits[15] ^= (_gift_round_constants[round]>>3) & 0x1;
	state_bits[19] ^= (_gift_round_constants[round]>>4) & 0x1;
	state_bits[23] ^= (_gift_round_constants[round]>>5) & 0x1;
	state_bits[127] ^= 1;
}

/**********************************************************************
 * 
 * @name	:	gift_enc
 * 
 * @note	:	GIFT-128 encryption function.
 * 
 **********************************************************************/
void gift_enc(u8 *ct, u8 (*round_keys)[32], const u8 *pt)
{
    u8 in[32];
    
	// convert input plaintext from
	// byte-oriented to nibble-oriented
	bytes_to_nibbles(&in[0], &pt[0], 16);
    
	u8 in_bits[128];

	u8 r;
	for(r=0; r < CRYPTO_BC_NUM_ROUNDS; r++)
	{
		// subcells operation
		sub_cells(&in[0]);

		// permbits operation
		nibbles_to_bits(&in_bits[0], &in[0], 32);
		perm_bits(&in_bits[0]);
		
		// addroundkey operation
		add_round_key(&in_bits[0], round_keys[r]);
		
		// addroundconstant operation
		add_round_constants(&in_bits[0], r);
		
		// input bits to nibbles
		bits_to_nibbles(&in[0], &in_bits[0], 32);
	}
	
	//convert ciphertext from nibble-oriented to byte-oriented
	nibbles_to_bytes(ct, in, 16);
}

/////////////////////////////////////////////////////////////////////////



u64 load64(u8 *Bytes)
{
    int i; u64 Block;
 
    Block=0;
 
    Block = (u64)(Bytes[0]);
     
    for(i = 1; i < 8; i++) {Block <<= 8; Block = (Block)^(u64)(Bytes[i]);}
 
    return Block;
}

void store64(u8 *Bytes, u64 Block)
{ 
    int i; 
     
    for (i = 7; i >= 0 ; i--) {Bytes[i] = (u8)Block; Block >>= 8; }
}



/**********************************************************************
 * 
 * @name	:	mult_by_alpha
 * 
 * @note	:	Multiplies given field element in "src" with \alpha,
 * 				the primitive element corresponding to the primitive
 * 				polynomial p(x) as defined in PRIM_POLY_MOD_128, and
 * 				stores the result in "dest".
 * 
 **********************************************************************/	
void mult_by_alpha(u8 *dest, u8 *src)
{
	u64 b;
	b =  load64(src);

        if(((b>>63)&1)==1) b = (b<<1)^(0x000000000000001B); 
                         else b = b<<1;

	store64(dest, b);	
		
}


/**********************************************************************
 * 
 * @name	:	memcpy_and_zero_one_pad
 * 
 * @note	:	Copies src bytes to dest and pads with 10* to create
 * 				CRYPTO_BLOCKBYTES-oriented data.
 * 
 **********************************************************************/
void memcpy_and_zero_one_pad(u8* dest, const u8 *src, u8 len)
{
	memset(dest, 0, 16);
	memcpy(dest, src, len);
	dest[len] ^= 0x01;
}



/**********************************************************************
 * 
 * @name	:	Feedback_TXT_Enc
 * 
 * @note	:	The FB+ module
 * 
 **********************************************************************/
void Feedback_TXT_Enc(u8 *State, u8 *output, const u8 *Delta, const u8 *input, const u64 inputlen)
{
	u32 i;
	u8 pad1[16], pad2[16], feedback[16];

	for(i = 0 ; i < inputlen;i++)  output[i] = input[i]^State[i];


	if(inputlen < 16) 
	{
		memcpy_and_zero_one_pad(&pad1[0], input, inputlen);
		memcpy_and_zero_one_pad(&pad2[0], output, inputlen);
	}
	else 
	{
		for(i = 0 ; i < 16 ; i++)
		{
		pad1[i] = input[i];
		pad2[i] = output[i];
		}
	}

     for(i=0; i<8 ;i++)
     { 
		feedback[i] = pad1[i];
           feedback[i+8] = pad2[i+8];

     }
     for(i=8; i<15 ;i++)
     { 
		feedback[i] ^= Delta[i-8];
     }


     
	for(i = 0 ; i < 16 ; i++) State[i] ^= feedback[i];
      
}		

/**********************************************************************
 * 
 * @name	:	Feedback_TXT_Dec
 * 
 * @note	:	The FB- module
 * 
 **********************************************************************/
void Feedback_TXT_Dec(u8 *State, u8 *output, const u8 *Delta, const u8 *input, const u64 inputlen)
{
	u32 i;
	u8 pad1[16], pad2[16], feedback[16];
	for(i = 0 ; i < inputlen;i++)   output[i] = input[i]^State[i];

	if(inputlen < 16) 
	{
		memcpy_and_zero_one_pad(&pad1[0], output, inputlen);
		memcpy_and_zero_one_pad(&pad2[0], input, inputlen);
	}
	else 
	{
		for(i = 0 ; i < 16 ; i++)
		{
		pad1[i] = output[i];
		pad2[i] = input[i];
		}
	}
     	for(i=0; i<8 ;i++)
     	{ 
		feedback[i] = pad1[i];
        	feedback[i+8] = pad2[i+8];
        }
    	for(i=8; i<15 ;i++)
     	{ 
		feedback[i] ^= Delta[i-8];
     	}
     
	for(i = 0 ; i < 16 ; i++) State[i] ^= feedback[i];
      
}



/**********************************************************************
 * 
 * @name	:	INIT
 * 
 * @note	:	Derives nonce-dependent initial state and mask.
 * 
 **********************************************************************/
void INIT(u8 *State, u8 * Delta, const u8 *npub, const u32 cntrl, u8 (*round_keys)[32])
{
	u32 i;
	for(i = 4 ; i < 16 ; i++) 
		State[i] = npub[i-4];
	for(i = 0 ; i < 4 ; i++)
		State[i] = 0;
	State[0] ^= (u8)cntrl;		
	gift_enc(State, &round_keys[0], State);	
	for(i = 0 ; i < 8 ; i++)
		Delta[i] = State[i+8];
}

/**********************************************************************
 * 
 * @name	:	PROC_AD
 * 
 * @note	:	Processes associated data.
 * 
 **********************************************************************/
void PROC_AD(u8 *State, u8 * Delta,  const u8 *input,  u64 inputlen, u8 (*round_keys)[32])
{
      u8 output[16] = { 0 };
      u64 outputlen = 0;

	while(inputlen > 16)
	{
		mult_by_alpha(Delta, Delta);
		Feedback_TXT_Enc(State, output, Delta, input+outputlen, 16);
		gift_enc(State, &round_keys[0], State);	
		inputlen -= 16; outputlen += 16;
	}		
	mult_by_alpha(Delta, Delta);
	
	if(inputlen < 16)
	{
		mult_by_alpha(Delta, Delta); 
		mult_by_alpha(Delta, Delta);
	}
	else
	{
		mult_by_alpha(Delta, Delta);
	}

	Feedback_TXT_Enc(State, output, Delta, input+outputlen, inputlen);			
	//gift_enc(State, &round_keys[0], State);			
}

/**********************************************************************
 * 
 * @name	:	Proc_TXT
 * 
 * @note	:	Generates ciphertext/plaintext by encrypting/decrypting
 * 				plaintext/ciphertext.
 * 
 **********************************************************************/
void Proc_TXT(u8 *State, u8 *Delta,  u8 *output, u64 *outputlen, u8 *input, u64 inputlen, u8 (*round_keys)[32], const u32 direction)
{
	if(inputlen != 0)
	{
		while(inputlen > 16)
		{		
			mult_by_alpha(Delta, Delta);
			gift_enc(State, &round_keys[0], State);	
			if(direction==0)
				Feedback_TXT_Enc(State, output + *outputlen, Delta, input + *outputlen, 16);
                 	else 
				Feedback_TXT_Dec(State, output + *outputlen, Delta, input + *outputlen, 16);
			inputlen -= 16; *outputlen += 16; 
		}
		mult_by_alpha(Delta, Delta);
		if(inputlen < 16)
		{
			mult_by_alpha(Delta, Delta); mult_by_alpha(Delta, Delta); 	 
		}
		else
		{
			mult_by_alpha(Delta, Delta);
		}
		gift_enc(State, &round_keys[0], State);			
		if(direction==0)
			Feedback_TXT_Enc(State, output + *outputlen, Delta, input + *outputlen, inputlen);
                else 
			Feedback_TXT_Dec(State, output + *outputlen, Delta, input + *outputlen, inputlen);
		*outputlen = *outputlen + inputlen; 		
	}
}

void swap(u8 *a, u8 *b)
{
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}

/**********************************************************************
 * 
 * @name	:	Tag_Gen
 * 
 * @note	:	Tag generator.
 * 
 **********************************************************************/
void Tag_Gen(u8 *State, /*const */u8 (*round_keys)[32])
{ 	
	u32 i;
	for(i = 0 ; i < 8 ; i++)
		swap(&State[i], &State[i+8]);	
	gift_enc(State, &round_keys[0], State);
}

/**********************************************************************
 * 
 * @name	:	crypto_aead_encrypt
 * 
 * @note	:	Main encryption function.
 * 
 **********************************************************************/
int crypto_aead_encrypt(
	unsigned char *ct, unsigned long long *ctlen,
	const unsigned char *pt, unsigned long long ptlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *nsec,
	const unsigned char *npub,
	const unsigned char *k
)
{
	// to bypass unused warning on nsec
	nsec = nsec;
	
	u32 i;

	u32 cntrl;

	*ctlen = 0;
	cntrl = 0;

	u8  HYENA_State[16], Delta[8], round_keys[CRYPTO_BC_NUM_ROUNDS][32];

      

	for(i = 0 ; i < 16 ; i++) HYENA_State[i] = 0;
        for(i = 0 ; i < 8 ; i++) Delta[i] = 0;

	if(adlen == 0 && ptlen == 0) cntrl = 0x03;
	if(adlen == 0 && ptlen > 0) cntrl = 0x01;

	_GIFT_ENC_ROUND_KEY_GEN(&round_keys[0], k);

	INIT(&HYENA_State[0], &Delta[0], npub, cntrl, &round_keys[0]);
	


      PROC_AD(&HYENA_State[0], &Delta[0], ad, adlen, &round_keys[0]);
	if(ptlen != 0)
	{				
  		Proc_TXT(&HYENA_State[0], &Delta[0], ct, ctlen, (u8 *)pt, ptlen, &round_keys[0], 0);
                

	}


	Tag_Gen(&HYENA_State[0], &round_keys[0]);

     

	for(i = 0 ; i< CRYPTO_ABYTES ; i++) ct[*ctlen + i] = HYENA_State[i];
      
        *ctlen  += CRYPTO_ABYTES;


	return 0;
}

/**********************************************************************
 * 
 * @name	:	crypto_aead_decrypt
 * 
 * @note	:	Main decryption function.
 * 
 **********************************************************************/
int crypto_aead_decrypt(
	unsigned char *pt, unsigned long long *ptlen,
	unsigned char *nsec,
	const unsigned char *ct, unsigned long long ctlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *npub,
	const unsigned char *k
)
{
	// to bypass unused warning on nsec
	nsec = nsec;
	
	int pass;
	u32 i;
	u8  tag[16], HYENA_State[16], Delta[8], cntrl, round_keys[CRYPTO_BC_NUM_ROUNDS][32];

      cntrl = 0;
      *ptlen = 0;
	for(i = 0 ; i < 16 ; i++) HYENA_State[i] = 0;
      for(i = 0 ; i < 8 ; i++) Delta[i] = 0;

	if(adlen == 0 && ctlen == 16) cntrl = 0x03;

	if(adlen == 0 && ctlen != 16) cntrl = 0x01;
	_GIFT_ENC_ROUND_KEY_GEN(&round_keys[0], k);

	INIT(&HYENA_State[0], &Delta[0], npub, cntrl, &round_keys[0]);


        PROC_AD(&HYENA_State[0], &Delta[0], ad, adlen, &round_keys[0]);

	if(ctlen != 16)
	{
  		Proc_TXT(&HYENA_State[0], &Delta[0], pt, ptlen, (u8 *)ct, ctlen-16, &round_keys[0], 1);
        }


	Tag_Gen(&HYENA_State[0], &round_keys[0]);
	pass = 0;

	for(i = 0 ; i< CRYPTO_ABYTES ; i++) 
	{
		tag[i] = HYENA_State[i];
		if(tag[i]!=ct[*ptlen + i]) pass = -1;
	}
	
	return pass;
}


