#ifndef SKINNY_AE_H
#define SKINNY_AE_H

//#include "contiki.h"
//#include "lib/skinny/constants.h"

#include <stddef.h>
//#include <stdbool.h>
#include <stdint.h>

void skinny_aead_encrypt(
	const uint8_t *ass_data, 
	size_t ass_data_len,                         
	const uint8_t *message, 
	size_t m_len,                        
	const uint8_t *key,                        
	const uint8_t *nonce,                    
	uint8_t *ciphertext, 
	size_t *c_len);


int skinny_aead_decrypt(
	const uint8_t *ass_data,
	size_t ass_data_len,                      
	uint8_t *message, 
	size_t *m_len,                      
	const uint8_t *key,                     
	const uint8_t *nonce,                   
	const uint8_t *ciphertext, 
	size_t c_len);

#endif /* SKINNY_AE_H */

#define BLOCK_SIZE 16 /* Replace with the cipher block size in bytes */
#define BLOCK_LENGTH_BITS         128
#define KEY_BYTES 16 /* Replace with the cipher key size in bytes */

#define ROUND_KEYS_SIZE 320 /* Replace with the cipher round keys size in bytes */
#define NUMBER_OF_ROUNDS 40 /* Replace with the cipher number of rounds */
#define TAG_SIZE           128 /* 128-bit authentication tag */
#define TAG_BYTES           (TAG_SIZE/8)
///////////////////////////////////////////
//#define TWEAKEY_LENGTH_BITS       (TWEAK_LENGTH_BITS+KEY_LENGTH_BITS)
//#define ROUND_TWEAKEY_LENGTH_BITS 64

#define NONCE_LENGTH_BITS         128
#define TAG_LENGTH_BITS           128

//#define TWEAK_BYTES         (TWEAK_LENGTH_BITS/8)
//#define KEY_BYTES           (KEY_LENGTH_BITS/8)
//#define TWEAKEY_BYTES       (TWEAKEY_LENGTH_BITS/8)
//#define LANE_BYTES          (LANE_BITS/8)
//#define ROUND_TWEAKEY_BYTES (ROUND_TWEAKEY_LENGTH_BITS/8)
//#define BLOCK_BYTES         (BLOCK_LENGTH_BITS/8)
#define NONCE_BYTES         (NONCE_LENGTH_BITS/8)
