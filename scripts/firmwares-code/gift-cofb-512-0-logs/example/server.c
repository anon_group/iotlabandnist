/* C include */
#include <stdio.h>
#include <stdlib.h>
/* Contiki include */
#include "contiki.h"
#include "net/ipv6/uip.h"
#include "net/routing/rpl-classic/rpl.h"

/* ascon-ae security */
#include "lib/Gift-cofb/gift-cofb.h"

/* Debug msg */
#define DEBUG DEBUG_PRINT
#include "net/ipv6/uip-debug.h"

/* UDP PORT connexion */
#define UDP_CLIENT_PORT	8765
#define UDP_SERVER_PORT	5678

static struct uip_udp_conn *server_conn;

/*---------------------------------------------------------------------------*/
PROCESS(server_process, "server process");
AUTOSTART_PROCESSES(&server_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{


  uint8_t key[CRYPTO_KEYBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
          };
  uint8_t nonce[CRYPTO_NPUBBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,0x0f
          };

  if(uip_newdata()) {


    typedef struct app_data
    {
      int message_number;
      uint8_t ciphertext[512+CRYPTO_ABYTES];
     } app_data ;


    app_data *d = (app_data *)uip_appdata;




/////////////////added/////////////////////////

uint8_t *obtained_plain= malloc((512)*sizeof(uint8_t));


    unsigned long long len_obtained_plain;//not-initialized, so send the &
    //PRINTF("TOTO\n");

crypto_aead_decrypt(obtained_plain, &len_obtained_plain,
  CRYPTO_NSECBYTES, d->ciphertext,
  512+CRYPTO_ABYTES, NULL, 0,
  nonce, key);


PRINTF("[Server] Message: %d\n", d->message_number);

free(obtained_plain);
  }
}
/*---------------------------------------------------------------------------*/
static void
create_rpl_dag(uip_ipaddr_t *ipaddr)
{
  struct uip_ds6_addr *root_if;

  root_if = uip_ds6_addr_lookup(ipaddr);
  if(root_if != NULL) {
    rpl_dag_t *dag;
    uip_ipaddr_t prefix;

    rpl_set_root(RPL_DEFAULT_INSTANCE, ipaddr);
    dag = rpl_get_any_dag();
    uip_ip6addr(&prefix, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
    rpl_set_prefix(dag, &prefix, 64);
    PRINTF("created a new RPL dag\n");
  } else {
    PRINTA("failed to create a new RPL DAG\n");
  }
}
/*---------------------------------------------------------------------------*/
static uip_ipaddr_t *
set_global_address(void)
{
  static uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

  return &ipaddr;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(server_process, ev, data)
{
  uip_ipaddr_t *ipaddr;

  PROCESS_BEGIN();

  ipaddr = set_global_address();

  create_rpl_dag(ipaddr);

  /* new connection with server host */
  server_conn = udp_new(NULL, UIP_HTONS(UDP_CLIENT_PORT), NULL);
  if(server_conn == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(server_conn, UIP_HTONS(UDP_SERVER_PORT));

  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
