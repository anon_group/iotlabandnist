/* C include */
#include <stdio.h>
#include <stdlib.h>

/* Contiki include */
#include "contiki.h"
#include "net/ipv6/uip-udp-packet.h"
#include "net/routing/rpl-classic/rpl.h"

/* ascon-ae security */
#include "lib/Subterranean/subterranean.h"

/* Debug msg */
#define DEBUG DEBUG_PRINT
#include "net/ipv6/uip-debug.h"

/* Timer */
#define DATA_INTERVAL_TRANS		(0 * CLOCK_SECOND)

/* UDP PORT connexion */
#define UDP_CLIENT_PORT	8765
#define UDP_SERVER_PORT	5678

static struct uip_udp_conn *unicast_connection;

/*---------------------------------------------------------------------------*/
PROCESS(client_process, "client process");
AUTOSTART_PROCESSES(&client_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *appdata;
  if(uip_newdata()) {
    appdata = (char *)uip_appdata;
    appdata[uip_datalen()] = 0;
    PRINTF("Data received from -");
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF(" on port %d from port %d with length %d: '%s'\n",
          UIP_HTONS(UIP_UDP_BUF->destport), UIP_HTONS(UIP_UDP_BUF->srcport), uip_datalen(), appdata);
  }
}
/*---------------------------------------------------------------------------*/
PROCESS(process_send_data, "Send data process");
PROCESS_THREAD(process_send_data, ev, data)
{

  uint8_t key[CRYPTO_KEYBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
          };
  uint8_t nonce[CRYPTO_NPUBBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,0x0f
          };
  uint8_t expectedPlaintext[128] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 16
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 32
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 48
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 64
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 80
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 96
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, // 112
                                    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f}; // 128

  static int message_number = -1;

  typedef struct message
  {
   int message_number;
   uint8_t ciphertext[CRYPTO_ABYTES+128];
  } message;

  message *data_to_send = malloc(sizeof(int)+(CRYPTO_ABYTES+128)*sizeof(uint8_t));


 PROCESS_BEGIN();

 // Save the message number
 message_number++;
 data_to_send->message_number = message_number;

   unsigned long long len=0;
   uint8_t *cipher;
   cipher= malloc((128+CRYPTO_ABYTES)*sizeof(uint8_t));


crypto_aead_encrypt(cipher,&len,expectedPlaintext,128,NULL,0,CRYPTO_NSECBYTES,nonce,key);


 int i;
 for (i=0; i<CRYPTO_ABYTES+128; i++){
	data_to_send->ciphertext[i] = cipher[i];
 }

  PRINTF("[Client] Message number: %d\n", message_number);

  uip_udp_packet_sendto(unicast_connection, data_to_send,sizeof(int)+CRYPTO_ABYTES+128,//, cleartext, cleartext_len, //ciphertext, outlen,
                        data, UIP_HTONS(UDP_SERVER_PORT));




  free(cipher);
  free(data_to_send);
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(client_process, ev, data)
{
  static struct etimer send_data_timer;
  rpl_dag_t *dag;

  PROCESS_BEGIN();

  /* new connection with server host */
  unicast_connection = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL);
  if(unicast_connection == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(unicast_connection, UIP_HTONS(UDP_CLIENT_PORT));

  etimer_set(&send_data_timer, DATA_INTERVAL_TRANS);

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }

    if(etimer_expired(&send_data_timer)) {
      etimer_reset(&send_data_timer);

      dag = rpl_get_any_dag();
      if(dag != NULL) {
        process_start(&process_send_data, &dag->dag_id);
      }
    }
  }

  PROCESS_END();
}
