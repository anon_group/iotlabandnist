///////////////////////////////////////////////////////////////////////////////
// sparkle_ref.c: Reference C implementation of the SPARKLE permutation.     //
// This file is part of the SPARKLE submission to NIST's LW Crypto Project.  //
// Version 1.0.0 (2019-03-29), see <http://www.cryptolux.org/> for updates.  //
// Authors: The SPARKLE Group (C. Beierle, A. Biryukov, L. Cardoso dos       //
// Santos, J. Groszschaedl, L. Perrin, A. Udovenko, V. Velichkov, Q. Wang).  //
// License: GPLv3 (see LICENSE file), other licenses available upon request. //
// Copyright (C) 2019 University of Luxembourg <http://www.uni.lu/>.         //
// ------------------------------------------------------------------------- //
// This program is free software: you can redistribute it and/or modify it   //
// under the terms of the GNU General Public License as published by the     //
// Free Software Foundation, either version 3 of the License, or (at your    //
// option) any later version. This program is distributed in the hope that   //
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied     //
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  //
// GNU General Public License for more details. You should have received a   //
// copy of the GNU General Public License along with this program. If not,   //
// see <http://www.gnu.org/licenses/>.                                       //
///////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sparkle_ref.h"

//#include "api.h"
//#include "crypto_aead.h"
#include "stdint.h"
#include "schwaemmconfig.h"
//#include "util.h"
#include "string.h" //for memcpy

//#ifdef _DEBUG
//#include "stdio.h"
//#endif





#define ROT(x, n) (((x) >> (n)) | ((x) << (32-(n))))
#define ELL(x) (ROT(((x) ^ ((x) << 16)), 16))



// 4-round ARX-box
#define ARXBOX(x, y, c)                     \
  (x) += ROT((y), 31), (y) ^= ROT((x), 24), \
  (x) ^= (c),                               \
  (x) += ROT((y), 17), (y) ^= ROT((x), 17), \
  (x) ^= (c),                               \
  (x) += (y),          (y) ^= ROT((x), 31), \
  (x) ^= (c),                               \
  (x) += ROT((y), 24), (y) ^= ROT((x), 16), \
  (x) ^= (c)

// Inverse of 4-round ARX-box
#define ARXBOX_INV(x, y, c)                 \
  (x) ^= (c),                               \
  (y) ^= ROT((x), 16), (x) -= ROT((y), 24), \
  (x) ^= (c),                               \
  (y) ^= ROT((x), 31), (x) -= (y),          \
  (x) ^= (c),                               \
  (y) ^= ROT((x), 17), (x) -= ROT((y), 17), \
  (x) ^= (c),                               \
  (y) ^= ROT((x), 24), (x) -= ROT((y), 31)

// Round constants
static const uint32_t RCON[MAX_BRANCHES] = {      \
  0xB7E15162, 0xBF715880, 0x38B4DA56, 0x324E7738, \
  0xBB1185EB, 0x4F7C7B57, 0xCFBFA1C8, 0xC2B3293D  \
};



/////////////////////////////////////////////////////////////////////
#ifndef UTIL_H
#define UTIL_H

//#include "stdint.h"
//#include "schwaemmconfig.h"

//#include "stdint.h"
//#include "util.h"
//#include "schwaemmconfig.h"
//#include "string.h"
//#include "sparkle_ref.h"

#ifdef _DEBUG
    #define INLINE
#else
    #define INLINE inline
#endif /*_DEBUG*/

#define u8 unsigned char
#define u64 long long unsigned int

#define WORDSIZE (32)
#define BYTE(X) (X/8)
#define WORD(X) (X/WORDSIZE)
#define ROT32(x, n) ((x >> n) | (x << (WORDSIZE-n)))
#define INJECTCONST(x, y) x[WORD(STATESIZE)-1] ^= (y) << 24

#define SWAPu32(X, Y, TMP) {TMP = X; X=Y; Y=TMP;}

#define RATEWHITENING(S) do{ \
    int _i;                          \
    for (_i=0; _i<WORD(RATE); _i++){               \
        S[_i] ^= S[(WORD(RATE))+(_i%(WORD(CAPACITY)))];   \
    }                                                 \
} while(0)


extern const uint32_t C_SEED[8];

uint32_t load32( unsigned char *in);
   void store32(unsigned char *out, uint32_t in);
    void feistelSwap(uint32_t *state);
   void rho1(uint32_t *state, uint32_t *D);
    void rho2(uint32_t *state, uint32_t *D);
    void rhop1(uint32_t *state, uint32_t *D);
    void pad(uint32_t *out, u8 *in, u8 inlen);
    void sparklePermutation(uint32_t state[WORD(STATESIZE)], int Ns);

#endif /*UTIL_H*/
//////////////////////////////////////////////////////////////////////

const uint32_t C_SEED[8] = {0xB7E15162, 0x8AED2A6A, 0xBF715880, 0x9CF4F3C7, 0x62E7160F, 0x38B4DA56, 0xA784D904, 0x5190CFEF};

uint32_t load32( unsigned char *in){
    uint32_t out;
    memcpy (&out, in, sizeof(out));
    return out;
}

void store32(unsigned char *out, uint32_t in){
     memcpy (out, &in, sizeof(in));
}

void feistelSwap(uint32_t *state){
    //feistelSwap works on the rate part of the state, and is defined as
    // S1 || S2 = S2 ||(S2 \xor S1), with S1 and S2 being equal halfs of the rate.
    uint32_t tmp;
    int i;
    for(i=0; i<WORD(RATE/2); i++){
        tmp = state[i];
        state[i] = state[WORD(RATE/2)+i];
        state[WORD(RATE/2)+i] ^= tmp;
    }
}

void rho1(uint32_t *state, uint32_t *D){
    feistelSwap(state);
    int i;
    for(i=0; i<WORD(RATE); i++)
        state[i] ^= D[i];
}

void rho2(uint32_t *state, uint32_t *D){ /*rho2 is equal to rho'2*/
    int i;
    for(i=0; i<WORD(RATE); i++)
        state[i] ^= D[i];
}

void rhop1(uint32_t *state, uint32_t *D){ /*rho prime*/
    uint32_t tmp[BYTE(RATE)];
    memcpy (tmp, state, BYTE(RATE));
    feistelSwap(state);
    int i;
    for(i=0; i<WORD(RATE); i++)
        state[i] ^= D[i] ^ tmp[i];
}


void pad(uint32_t *out, u8 *in, u8 inlen){
    //If the padding isn't necessary, then only copy.
    memcpy(out, in, inlen);
    uint8_t *o;
    o=(uint8_t *)(out);
    if (inlen!=BYTE(RATE)){
        o[inlen]=0x80;
        memset(o+inlen+1, 0, BYTE(RATE)-inlen-1);
    }
}
//wrapper funcion for the permutation
void sparklePermutation(uint32_t state[WORD(STATESIZE)], int Ns){
    state_t S={{0},{0}};
    int i;
    for (i=0; i<B*2; i++){
        S.x[i] = state[2*i];
        S.y[i] = state[2*i+1];
    }

    sparkle_ref(&S, B*2, Ns); //Number of 32-bit branches, hence B*4

    for (i=0; i<B*2; i++){
        state[2*i]   = S.x[i];
        state[2*i+1] = S.y[i];
    }

}



////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// encrypt.c: C implementation of the SCHWAEMM AEAD algorithm                //
// This file is part of the NIST's submission "Schwaemm and Esch: Lightweight//
// Authenticated Encryption and Hashing using the Sparkle Permutation Family //
// Version 1.0.0 (2019-03-29), see <http://www.cryptolux.org/> for updates.  //
// Authors: The SPARKLE Group (C. Beierle, A. Biryukov, L. Cardoso dos       //
// Santos, J. Groszschaedl, L. Perrin, A. Udovenko, V. Velichkov, Q. Wang).  //
// License: GPLv3 (see LICENSE file), other licenses available upon request. //
// Copyright (C) 2019 University of Luxembourg <http://www.uni.lu/>.         //
// ------------------------------------------------------------------------- //
// This program is free software: you can redistribute it and/or modify it   //
// under the terms of the GNU General Public License as published by the     //
// Free Software Foundation, either version 3 of the License, or (at your    //
// option) any later version. This program is distributed in the hope that   //
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied     //
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  //
// GNU General Public License for more details. You should have received a   //
// copy of the GNU General Public License along with this program. If not,   //
// see <http://www.gnu.org/licenses/>.                                       //
///////////////////////////////////////////////////////////////////////////////


// should be compiled with
// -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2

// gencat_aead.c shall be used to generate the test vector output file. The test vector output
// file shall be provided in the corresponding crypto_aead/[algorithm]/ directory



/*___________________________Helper debug functions___________________________*/
#ifdef _DEBUG
    void pstate(uint32_t *state){
        int i;
        for(i=0; i<WORD(STATESIZE); i++){
            printf("%02d:%08x ", i, state[i]);
            if(i%4==3) printf("\n");
        }
        printf("\n");
    }

    void countstate(uint32_t *state){
        int i;
        for(i=0; i<WORD(STATESIZE); i++)
            state[i]=i;
    }

    void p8state(uint32_t *state){
        u8 *s;
        s=(u8 *)(state);
        int i;
        for(i=0; i<BYTE(STATESIZE); i++){
            printf("%02x ", s[i]);
            if(i%4==3) printf(" ");
            //if (i%16==15) printf("\n");
        }
        printf("\n");
    }
#endif

/*____________________________Low Level Functions_____________________________*/

/*
 The initialize function loads nonce and key into the internal state, and
 executes a SPARKLE permutation.
*/
//INLINE 
void initialize(uint32_t *state, const u8 *key, const u8 *nonce){
    //load nonce into state.
    int i;
    for(i=0; i<CRYPTO_NPUBWORDS; i++)
        state[i]=load32((u8 *)nonce+(4*i));
    //load key into state.
    for(i=0; i<CRYPTO_KEYWORDS; i++)
        state[i+CRYPTO_NPUBWORDS]=load32((u8 *)(key)+(4*i));
    //Apply permutation to state
    sparklePermutation(state, STEPSBIG);
}

/*
processAD absorbs additional data into the sponge.
*/
//INLINE 
void  processAD(uint32_t *state, const u8 *ad,  u64 adlen){
    if(adlen != 0){
        int constA = (adlen % BYTE(RATE) != 0) ? PADADCONST : NOPADADCONST;
        //absorption loop
        while (adlen > BYTE(RATE)){
            rho1(state, (uint32_t *)ad);
            RATEWHITENING(state);
            sparklePermutation(state, STEPSSLIM);
            ad += BYTE(RATE);
            adlen -= BYTE(RATE);
        }
        //pad lBlock
        uint32_t lBlock[WORD(RATE)];
        pad(lBlock, (u8*)(ad), (u8)(adlen));

        //process last block
        rho1(state, lBlock);
        INJECTCONST(state, constA);
        RATEWHITENING(state);
        sparklePermutation(state, STEPSBIG);
    }
}
/*
encryptPT absorbs message blocks of size RATE, and generates the respective
ciphertext blocks. At the end of the encryption operation, an authentication tag
is generated and appended to the end of the cyphertext. It is expected that the
memory allocated for the ciphertext is CRYPTO_ABYTES larger than the message buffer
*/
//INLINE 
void  encryptPT(uint32_t *state, u8 *c, u64 *clen, const u8 *m, u64 mlen, const unsigned char *k){
    *clen = mlen + CRYPTO_ABYTES;
    if (mlen != 0){
        int constM = (mlen % BYTE(RATE) != 0) ? PADPTCONST : NOPADPTCONST;

        /*main encryption loop*/
        while (mlen > BYTE(RATE)){
            // $C_j \leftarrpow$C_j \leftarrow \rho_2(S_L , M_j)$
            memcpy(c, m, BYTE(RATE));
            rho2((uint32_t *)(c), state);
            // $S_L \parallel S_r \leftarrow \text{SparkleRATE}_{slim} (\rho_1 (S_L, M_j) \parallel S_R)$
            rho1(state, (uint32_t *)(m));
            RATEWHITENING(state);
            sparklePermutation(state, STEPSSLIM);
            m += BYTE(RATE);
            c += BYTE(RATE);
            mlen -= BYTE(RATE);
        }
        //pad last block
        uint32_t lBlock[WORD(RATE)];
        pad(lBlock, (u8 *)(m), mlen);

        //process last lBlock
        // $C_{\ell_{M-1}} \leftarrow \text{trunc}_t(\rho_2(S_L, M_{\ell_{M-1}}))$
        rho2(lBlock, state);
        memcpy(c, lBlock, mlen);
        // $S_L \parallel S_R \leftarrow \text{SparkleRATE}_{big}(\rho_1 (S_L, M_{\ell_{M-1}}) \parallel S_R \oplus \text{Const}_M))$
        pad(lBlock, (u8 *)(m), mlen);
        rho1(state, lBlock);
        INJECTCONST(state, constM);
        RATEWHITENING(state);
        sparklePermutation(state, STEPSBIG);
    }
        //write tag to ciphertext
        memcpy(c+mlen, (u8*)(state)+BYTE(RATE), CRYPTO_ABYTES);

        int i;
        for(i=0; i<CRYPTO_ABYTES; i++){
            (c+mlen)[i] ^= k[i];
        }
}
/*
Simple constant-time comparison. Returns 0 equal inputs. Considers that the
arguments are CRYPTO_ABYTES long.
*/
//INLINE
 int verifyTag(uint32_t *state, u8 *tag){
    //constant time. 0 for sucess, -1 for diffence
    u8 *tag1;
    tag1=(u8*)(state);
    tag1 += BYTE(RATE);
    unsigned int r = 0;
    int i;
    for (i=0; i<CRYPTO_ABYTES; i++)
        r |= tag1[i] ^ tag[i];
    return (((r-1) >> 8) &1)-1;
}
/*
The decryptCT function processes a cyphertext of mlen+CRYPTO_ABYTES and generates
the respective plaintext. It also verifies the authentication tag on the last
CRYPTO_ABYTES bytes of the buffer *c. If the tag is valid, the function returns 0.
If it fails, the function zeros the message buffer and returns -1.
*/
//INLINE  
int  decryptCT(uint32_t *state, u8 *m, u64 *mlen, const u8 *c, u64 clen,  const unsigned char *k){
    clen -= CRYPTO_ABYTES;
    *mlen = clen;
    if (clen != 0){
        //main decryption loop
        while (clen > BYTE(RATE)){
            //$M_j \leftarrow p^\prime_2 (S_L, C_j)$
            int i;
            for(i=0; i<BYTE(RATE); i++)
                m[i]=c[i];
            rho2((uint32_t*)(m), state);
            //$S_L \parallel S_R \leftarrow \text{SparkleRATE}_{slim}(\rho^\prime_1(S_L, C_j) \parallel S_R)$
            rhop1(state, (uint32_t*)(c));
            RATEWHITENING(state);
            sparklePermutation(state, STEPSSLIM);
            //Move pointers
            clen -= BYTE(RATE); m += BYTE(RATE); c += BYTE(RATE);
        }
        //decrypt last block
        uint32_t lBlock[WORD(RATE)];
        pad(lBlock, (u8 *)(c), clen);
        rho2(lBlock, state);
        memcpy(m, lBlock, clen);
        //Finalization
        if (clen < BYTE(RATE)){
            pad(lBlock, m, clen);
            rho1(state, lBlock);
            INJECTCONST(state, PADPTCONST);
            RATEWHITENING(state);
            sparklePermutation(state, STEPSBIG);
        }
        else {
            rhop1(state, (uint32_t *)(c));
            INJECTCONST(state, NOPADPTCONST);
            RATEWHITENING(state);
            sparklePermutation(state, STEPSBIG);
        }
    }
    c+=clen; //move c to point to tag.

    int i;
    for(i=0; i<CRYPTO_ABYTES; i++){ //xor key to tag location on state
        ((uint8_t *)(state) + BYTE(RATE) )[i] ^= k[i];
    }

    if (verifyTag(state, (u8 *)(c)) == 0)
        return 0;
    else{
        #ifndef _DEBUG
            //Zero generated plaintext in case of failure to authenticate
            unsigned long long j;
            for (j=0; j < *mlen; j++) m[j]=0;
        #endif
        return -1;
    }
}
////////////////////////////////////////////////////////////////////////////////





void linear_layer(state_t *state, int nb)
{
  int i, b = nb/2;
  uint32_t *x = state->x, *y = state->y;
  uint32_t tmp;
  
  // Feistel function (adding to y part)
  tmp = 0;
  for(i = 0; i < b; i ++)
    tmp ^= x[i];
  tmp = ELL(tmp);
  for(i = 0; i < b; i ++)
    y[i+b] ^= (tmp ^ y[i]);
  
  // Feistel function (adding to x part)
  tmp = 0;
  for(i = 0; i < b; i ++)
    tmp ^= y[i];
  tmp = ELL(tmp);
  for(i = 0; i < b; i ++)
    x[i+b] ^= (tmp ^ x[i]);
  
  // Branch swap with 1-branch left-rotation of right side
  // <------- left side --------> <------- right side ------->
  //    0    1    2 ...  B-2  B-1    B  B+1  B+2 ... 2B-2 2B-1
  //  B+1  B+2  B+3 ... 2B-1    B    0    1    2 ...  B-2  B-1
  
  // Branch swap of the x part
  tmp = x[0];
  for (i = 0; i < b-1; i ++) {
    x[i] = x[i+b+1];
    x[i+b+1] = x[i+1];
  }
  x[b-1] = x[b];
  x[b] = tmp;
  
  // Branch swap of the y part
  tmp = y[0];
  for (i = 0; i < b-1; i ++) {
    y[i] = y[i+b+1];
    y[i+b+1] = y[i+1];
  }
  y[b-1] = y[b];
  y[b] = tmp;
}


void sparkle_ref(state_t *state, int nb, int ns)
{
  int i, j;  // Step and branch counter
  
  // The number of branches (nb) must be even and not bigger than MAX_BRANCHES.
  assert(((nb & 1) == 0) && (nb >= 4) && (nb <= MAX_BRANCHES));
  
  for(i = 0; i < ns; i ++) {
    // Add step counter
    state->y[0] ^= RCON[i%MAX_BRANCHES];
    state->y[1] ^= i;
    // ARXBox layer
    for(j = 0; j < nb; j ++)
      ARXBOX(state->x[j], state->y[j], RCON[j]);
    // Linear layer
    linear_layer(state, nb);
  }
}


void linear_layer_inv(state_t *state, int nb)
{
  int i, b = nb/2;
  uint32_t *x = state->x, *y = state->y;
  uint32_t tmp;
  
  // Branch swap with 1-branch right-rotation of left side
  // <------- left side --------> <------- right side ------->
  //    0    1    2 ...  B-2  B-1    B  B+1  B+2 ... 2B-2 2B-1
  //    B  B+1  B+2 ... 2B-2 2B-1  B-1    0    1 ...  B-3  B-2
  
  // Branch swap of the x part
  tmp = x[b-1];
  for (i = b-1; i > 0; i --) {
    x[i] = x[i+b];
    x[i+b] = x[i-1];
  }
  x[0] = x[b];
  x[b] = tmp;
  
  // Branch swap of the y part
  tmp = y[b-1];
  for (i = b-1; i > 0; i --) {
    y[i] = y[i+b];
    y[i+b] = y[i-1];
  }
  y[0] = y[b];
  y[b] = tmp;
  
  // Feistel function (adding to x part)
  tmp = 0;
  for(i = 0; i < b; i ++)
    tmp ^= y[i];
  tmp = ELL(tmp);
  for(i = 0; i < b; i ++)
    x[i+b] ^= (tmp ^ x[i]);
  
  // Feistel function (adding to y part)
  tmp = 0;
  for(i = 0; i < b; i ++)
    tmp ^= x[i];
  tmp = ELL(tmp);
  for(i = 0; i < b; i ++)
    y[i+b] ^= (tmp ^ y[i]);
}


void sparkle_inv_ref(state_t *state, int nb, int ns)
{
  int i, j;  // Step and branch counter
  
  // The number of branches (nb) must be even and not bigger than MAX_BRANCHES.
  assert(((nb & 1) == 0) && (nb >= 4) && (nb <= MAX_BRANCHES));
  
  for(i = ns-1; i >= 0; i --) {
    // Linear layer
    linear_layer_inv(state, nb);
    // ARXbox layer
    for(j = 0; j < nb; j ++)
      ARXBOX_INV(state->x[j], state->y[j], RCON[j]);
    // Add step counter
    state->y[1] ^= i;
    state->y[0] ^= RCON[i%MAX_BRANCHES];
  }
}


void print_state_ref(const state_t *state, int nb)
{
  uint8_t *xbytes = (uint8_t *) state->x;
  uint8_t *ybytes = (uint8_t *) state->y;
  int i, j;
  
  for (i = 0; i < nb; i ++) {
    j = 4*i;
    printf("(%02x%02x%02x%02x %02x%02x%02x%02x)",     \
    xbytes[j], xbytes[j+1], xbytes[j+2], xbytes[j+3], \
    ybytes[j], ybytes[j+1], ybytes[j+2], ybytes[j+3]);
    if (i < nb-1) printf(" ");
  }
  printf("\n");
}


void test_sparkle_ref(int nb, int ns)
{
  state_t state = {{0}, {0}};
  
  printf("input:\n");
  print_state_ref(&state, nb);
  sparkle_ref(&state, nb, ns);
  printf("sparkle:\n");
  print_state_ref(&state, nb);
  sparkle_inv_ref(&state, nb, ns);
  printf("sparkle inv:\n");
  print_state_ref(&state, nb);
  printf("\n");
}


///////////////////////////////////////////////////////////////////////////////////
/*_____________________________Main API functions_____________________________*/
//nsec is kept for compatibility with SUPERCOP, but is not used.
int crypto_aead_encrypt(
unsigned char *c,unsigned long long *clen,
const unsigned char *m,unsigned long long mlen,
const unsigned char *ad,unsigned long long adlen,
const unsigned char *nsec,
const unsigned char *npub,
const unsigned char *k
)
{
    #ifdef _DEBUG
        uint32_t state[WORD(STATESIZE)]={0};
    #else
        uint32_t state[WORD(STATESIZE)];
    #endif
    initialize(state, k, npub);
    processAD(state, ad, adlen);
    encryptPT(state, c, clen, m, mlen, k);

    return 0;
}

int crypto_aead_decrypt(
unsigned char *m,unsigned long long *mlen,
unsigned char *nsec,
const unsigned char *c,unsigned long long clen,
const unsigned char *ad,unsigned long long adlen,
const unsigned char *npub,
const unsigned char *k
)
{
	//returns -1 if the ciphertext is not valid
    int decSucess;
    uint32_t state[WORD(STATESIZE)];
    initialize(state, k, npub);
    processAD(state, ad, adlen);
    decSucess = decryptCT(state, m, mlen, c, clen, k);
    return decSucess;
}











