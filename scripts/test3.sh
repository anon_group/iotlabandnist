#!/usr/bin/env bash

#include <unistd.h>

# -- Tests -- #
# Contiki NG + M3 + Lilliput: OK
# Contiki NG + M3 + Auth CCM: TODO
# Contiki NG + M3 + CCM: TODO
# Contiki NG + M3 + Simple app: OK

# Default values
DEFAULT_SERVER_FIRMWARE="" #TODO
DEFAULT_CLIENT_FIRMWARE="" #TODO
DEFAULT_TOPOLOGY="line"
DEFAULT_DURATION=20

# Display help
display_help() {
  echo -e "\e[1mUsage:\e[0m `basename $0` [-h] --architecture <architecture> --serverfirmware <server_firmware_file> --clientfirmware <client_firmware_file> --topology <topology_to_use> --duration <duration in minutes>

\e[1mDESCRIPTION\e[0m
           Launch an IoT-LAB experimentation with M3 nodes on RIOT platform.
           (For now, the firmware file has to be provided, and the topology are not implemented yet.)

           \e[1m-h, --help
           \e[0mDisplay the usage of the command line

           \e[1m--default
           \e[0mLaunch a default experiment

           \e[1m-a, --architecture
           \e[0mSelect the architecture to use
           \e[1m/!\\ \e[0mFor now, the only available architecture is: \"m3\"

           \e[1m-sf, --serverfirmware
           \e[0mSpecify the file to flash the server node with

           \e[1m-cf, --clientfirmware
           \e[0mSpecify the file to flash the client nodes with

           \e[1m-d, --duration
           \e[0mSpecify the duration of the experiment in minutes

           \e[1m-t, --topology
           \e[0mSpecify the topology to use
           Can be:
           - \"line\"
           - \"grid\"
           - \"star\""
}

# Parse the arguments
parse_args() {
  while true; do
    case "$1" in
      -h | --help ) display_help; exit 0; shift ;;
      -a | --architecture ) architecture="$2"; shift 2;;
      -sf | --serverfirmware ) serverfirmware="$2"; shift 2 ;;
      -cf | --clientfirmware ) clientfirmware="$2"; shift 2 ;;
      -t | --topology ) topology="$2"; shift 2 ;;
      -d | --duration ) duration="$2"; shift 2 ;;
      --default ) default=true; shift 1 ;;
      * ) break ;;
    esac
  done
}

# Check some of the passed arguments
check_args() {
  # Default
  if [ $default ]
  then
    $architecture=$DEFAULT_ARCHITECTURE
    $serverfirmware=$DEFAULT_SERVER_FIRMWARE
    $clientfirmware=$DEFAULT_CLIENT_FIRMWARE
    $duration=$DEFAULT_DURATION
    $topology=$DEFAULT_TOPOLOGY
  fi
  # All
  if [ ! $architecture ] || [ ! $serverfirmware ] || [ ! $clientfirmware ] || [ ! $topology ] || [ ! $duration ]
  then
    echo -e "\e[1;31m[Warning] One or more arguments are missing\e[0;0m"
    display_help
    exit 1
  fi

  # Architecture
  if [ $architecture != "m3" ]
  then
    echo -e "\e[1;31m[Warning] The architecture must be \"m3\""
    exit 1
  fi

  # Server firmware
  if [ ! -e $serverfirmware ]
  then
    echo -e "\e[1;31m[Warning] The server firmware file is missing\e[0;0m"
    display_help
    exit 1
  fi

  # Client firmware
  if [ ! -e $clientfirmware ]
  then
    echo -e "\e[1;31m[Warning] The client firmware file is missing\e[0;0m"
    display_help
    exit 1
  fi

  # Topology
  if [ $topology != "grid" ] && [ $topology != "line" ] && [ $topology != "star" ]
  then
    echo -e "\e[1;31m[Warning] The topology must be \"grid\", \"line\" or \"star\"\e[0;0m"
    display_help
    exit 1
  fi

  # Duration
  regexinteger='^[0-9]+$'
  if ! [[ $duration =~ $regexinteger ]] ; then
    echo -e "\e[1;31m[Warning] The duration must be an integer\e[0;0m"
    display_help
    exit 1
  fi

  echo "Args checked"
}

# Topology functions

set_nodes() {
  local architecture="$1"
  local topology="$2"

  # end to end
  # return "strasbourg,m3,1+33" # m3_33(sink) m3_1(client)

  # line
  if [ $architecture == "m3" ] && [ $topology == "line" ]
  then
    site="grenoble"
    dataclientnodes=(318 326 334 342 350)
    dataservernode=358
    clientnodes="grenoble,m3,318+326+334+342+350" # m3_358(sink) m3_350+m3_342+m3_334+m3_326+m3_318(client) # TODO: Lille ((166||256)-->90<--1<--45<--40)
    servernode="grenoble,m3,358"
  fi

  # grid
  if [ $architecture == "m3" ] && [ $topology == "grid" ]
  then
    site="lille"
    dataclientnodes=(21 30 40 63 73)
    dataservernode=32
    clientnodes="lille,m3,21+30+40+63+73" # Sink: 32    #"strasbourg,m3,1+5+9+29+33+27+53+57+61" # m3_33(sink) m3_1+m3_5+m3_9+m3_29+m3_37+m3_53+m3_57+m3_61(client)
    servernode="lille,m3,32"
  fi

  # star # TODO
  echo "TODO" # TODO
}

set_topology() {
  local architecture="$1"
  local os="$2"
  local topology="$3"

  # M3 nodes
  if [ $architecture == "m3" ]
  then
    # Set RPL routing
    # ... if RIOT has been chosen
    if [ $os == "riot" ]
    then
      # TODO: end-to-endline, grid, star
      if [ $topology == "end-to-end" ]
      then
        echo "TODO"
      fi

      # Line
      if [ $topology == "line" ]
      then
        echo "TODO"
      fi

      # Grid
      if [ $topology == "grid" ]
      then
        local site="lille"
        ssh -i /home/soline/.ssh/soline_iot-lab.pub blanc@$site.iot-lab.info
      fi

      # Star
      if [ $topology == "star" ]
      then
        echo "TODO"
      fi
    fi
    # ... if Contiki 3.0 has been chosen
    # TODO: end-to-end (?), line, grid, star
    # ... if Contiki NG has been chosen
    # TODO: end-to-end (?) line, grid, star
    echo "TODO"
  fi
}

# ... Line topology with RIOT
set_riot_line() {
  echo "serial_aggregator"
  echo "ifconfig+parsing"
  echo "rpl init M"
  # DAG root
  echo "m3-N;ifconfig M add 2001:db8::1"
  # Start TPL DODAG on the root node
  echo "m2-N;rpl root 1 2001:db8::1"
  echo "sudo ebtables -A FORWARD --in-interface tapX --out-interface tapY -j DROP"
  echo "sudo ebtables -A FORWARD --in-interface tapY --out-interface tapX -j DROP"
  echo "// Question : comment définir les différentes adresses IPv6 des noeuds ?"
}

# ... Grid topology with RIOT
set_riot_grid() {
  echo "TODO"
}

# ... Star topology with RIOT
set_riot_star() {
  echo "TODO"
}

# ... Line topology with Contiki 3.0
set_contiki3_line() {
  echo "TODO"
}

# ... Grid topology with Contiki 3.0
set_contiki3_grid() {
  echo "TODO"
}

# ... Star topology with Contiki 3.0
set_contiki3_star() {
  echo "TODO"
}

# ... Line topology with Contiki NG
set_contiking_line() {
  echo "TODO"
}

# ... Grid topology with Contiki NG
set_contiking_grid() {
  echo "TODO"
}

# ... Star topology with Contiki NG
set_contiking_star() {
  echo "TODO"
}

# Get the results of the different metrics
get_measures() {
  scp -r -i /home/soline/.ssh/soline_iot-lab.pub blanc@grenoble.iot-lab.info:/senslab/users/blanc/.iot-lab/193345/ exp-193345
}

# Main function
main() {
  # Parse the input arguments
  parse_args "$@"
  # Check the values of the input
  check_args

  if [ $architecture ] && [ $serverfirmware ] && [ $clientfirmware ] && [ $topology ] && [ $duration ]
  then
    # Chose the nodes regarding the topology
    set_nodes "$architecture" "$topology"

    # Launch the experiment
    echo "Launch the experiment"
    profile="m3_voltage_sniffer11"
    # TODO: do profile
    launching_exp="$(iotlab-experiment submit \
    -d $duration \
    -l $clientnodes,$clientfirmware,$profile\
    -l $servernode,$serverfirmware,$profile\
    -n test20200204)"
    result="${launching_exp}"
    experiment_id="$(echo $result | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')"
    echo $experiment_id

    # Set logical topology
    # TODO set_topology "$architecture" "$firmware" "$topology"

    # TODO
    #set_riot_line
    # Set RPL routing
    # ... if RIOT has been chosen
    # TODO: line, grid, star
    # ... if Contiki 3.0 has been chosen
    # TODO: line, grid, star
    # ... if Contiki NG has been chosen
    # TODO: line, grid, star

    # TODO: Wait for the end of the experiment (duration+5)
    echo "Waiting..."
    while true
    do
      #"$(iotlab-experiment get --id $experiment_id --exp-state)"
      state=$(iotlab-experiment get --id $experiment_id --exp-state  | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["state"]')
      # "state": "Waiting"/"Running"/"Terminated"
      if [ $state == "Terminated" ]
      then
        break
      else
        echo "Current state: $state"
        sleep 300 # Wait 5 minutes
      fi
    done

    # TODO: Get the results
    scp -i /home/soline/.ssh/soline_iot-lab.pub -r blanc@$site.iot-lab.info:/senslab/users/blanc/.iot-lab/$experiment_id $experiment_id-data
    echo $experiment_id-data
    # TODO: Display/Analyze the results
    # TODO: Modifier le script de maniere a ce qu'il recupere deux arguments
    #dataclientnodes=[ "21", "30", "40", "63", "73"]
    #dataservernode="32"
    #python3 plot/consumption/consum2.py -i1 "${experiment_id}-data/consumption/${architecture}_${dataservernode}.oml" -i2 "${experiment_id}-data/consumption/${architecture}_${dataclientnodes[0]}.oml"
  fi
}

main "$@"
