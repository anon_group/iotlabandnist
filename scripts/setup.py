#!/usr/bin/env python3

import database as Database

## Main function
def main(args=None):
    print('[Set up] Starting...')
    Database.create_all_tables()
    print('[Set up] End.')


if __name__ == "__main__":
    main()
