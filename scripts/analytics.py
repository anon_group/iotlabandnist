#!/usr/bin/env python3

import common
import matplotlib.pyplot as plt
import pylab
import sys
from datetime import timedelta
from datetime import datetime
import numpy as np
import time
import os.path
import pprint
import paramiko
import utils as Utils
import pyshark
import json
import time
from fpdf import FPDF
import database as Database
import experiment as Experiment

class Analytics():

#####################
#  Analyse several  #
#####################

    def analyse_last_experiments(self, duration, architecture, topology,
                                    os, algorithms, experiments_number, output_path):
        experiments_ids_dict = {}
        traffic_dict = {}

        # Connect to the database
        db = Database.DatabaseConnection.Instance()

        # For each algorithm, get the X last experiments
        for algorithm in algorithms:
            print('[Analytics - {0}] Get last experiments'.format(algorithm))
            print(time.time())
            experiments_ids_dict[algorithm] = db.get_last_experiments(os, algorithm, architecture, duration, topology, experiments_number)
            print("Number of last experiments for algorithm {0}: {1}".format(algorithm, len(experiments_ids_dict[algorithm])))
            print(experiments_ids_dict[algorithm])
            print('')

            # For each experiment, save the measures and the traffic data if it is not done
            for experiment_id in experiments_ids_dict[algorithm]:
                print('[Analytics - {0} - {1}] Save the measures'.format(algorithm, experiment_id))
                exp = Experiment.Experiment(experiment_id)
                exp.load_from_database()
                exp.save_measures() # Save the measures
                exp.get_traffic_measures() # Save the traffic measures
                exp.compute_all_latency()

        # Generate the graphs comparing the power used by the different algorithms
        print('[Analytics - {0}] Compare power'.format(algorithm))
        print(time.time())
        self.compare_power_of_algorithms(experiments_ids_dict, output_path)

        # Create a PDF from the data stored in the output file
        self.create_report(experiments_ids_dict, output_path)



    def get_traffic_measures(self, experiments_ids):
        # For each experiment, get the number of exchanged packets,
        # the time interval, the sum of the lengths of the packets and
        # the number of watts used
        packets_per_second = 0
        bytes_per_second = 0
        packet_mean_length = 0
        watts_per_packet = 0
        for experiment_id in experiments_ids:
            # Get the data
            exp = Experiment.Experiment(experiment_id)
            (nb_packets, time_interval, sum_packets_lengths) = exp.get_traffic_measures()
            sum_watts = exp.get_total_power()
            print(exp.experiment_id)
            print("Nb packets: {0}\nSum packets: {1}\nSum watts: {2}\nTime interval: {3}".format(nb_packets, sum_packets_lengths, sum_watts, time_interval))

            # Add the experiment measures to the global data
            packets_per_second += nb_packets/time_interval
            bytes_per_second += sum_packets_lengths/time_interval
            packet_mean_length += sum_packets_lengths/nb_packets
            watts_per_packet += sum_watts/nb_packets

        # Compute the global measures
        packets_per_second = packets_per_second/len(experiments_ids)
        bytes_per_second = bytes_per_second/len(experiments_ids)
        packet_mean_length = packet_mean_length/len(experiments_ids)
        watts_per_packet = watts_per_packet/len(experiments_ids)

        print('packets per second')

        # Return the values
        return {"packets_per_second": packets_per_second,
                "bytes_per_second": bytes_per_second,
                "packet_mean_length": packet_mean_length,
                "watts_per_packet": watts_per_packet}

    def compare_power_of_algorithms(self, experiments_ids_dict, output_path):
        measures_dict = {}
        # For each algorithm
        print(experiments_ids_dict)
        for algorithm in experiments_ids_dict:
            print("\n\nAlgorithm {0}".format(algorithm))
            # Get the power measures
            power_measures = self.get_power_measures_dict(experiments_ids_dict[algorithm])
            measures_dict[algorithm] = {}
            measures_dict[algorithm]['timestamps'] = power_measures[0]
            measures_dict[algorithm]['power_values'] = power_measures[1]

        print("Measures dict\n\n")
        print(measures_dict)
        print("\n\n")

        # Draw power graph
        self.draw_graph(measures_dict, output_path)
        # Draw power boxplot
        self.draw_boxplot(measures_dict, output_path)
        # Draw latencies boxplots
        self.draw_boxplots_latency(experiments_ids_dict, output_path)
        # Draw the packets lengths boxplots
        self.draw_boxplots_packets_lengths(experiments_ids_dict, output_path)
        # Draw the other data in a easy way to compare the algorithms
        self.draw_data_points(experiments_ids_dict, output_path)

        # Draw power boxplot
        # TODO


    def get_power_measures_dict(self, experiments_ids):
        # Connect to the database
        db = Database.DatabaseConnection.Instance()

        # For each experiment, get the first timestamp
        min_timestamps = []
        for experiment_id in experiments_ids:
            min_timestamps.append(db.get_min_timestamp(experiment_id))

        # Get 5% of the experiment's duration
        exp = Experiment.Experiment(experiments_ids[0])
        exp.load_from_database()
        duration = exp.duration*60 # Duration in seconds
        duration_gap = 5/100*duration # 5% of the duration

        # Create time intervals to gather the measures
        interval_window = timedelta(minutes=2).total_seconds() # Timestamp window for the interval
        last_interval = duration_gap # First counted time from the beginning of the experiment in seconds
        timestamps = []
        power_values = []
        interval_cnt = 0
        while last_interval < (duration-duration_gap):
            print('Interval {0}'.format(interval_cnt))
            interval_length = min(interval_window, ((duration-duration_gap)-last_interval))
            timestamps.append((last_interval+(last_interval+interval_length))/2)
            power_values.append(db.get_mean_power_for_interval(experiments_ids, last_interval, interval_length))
            last_interval = last_interval+interval_window
            interval_cnt += 1

        return (timestamps, power_values)


    def draw_graph(self, measures_dict, output_path):
        fig = pylab.figure()
        figlegend = pylab.figure()
        ax = fig.add_subplot(111)

        lines = []
        i=0
        for algorithm_name in measures_dict:
            # Add it to the graph
            #lines.append(plt.plot(measures_dict[algorithm_name]['timestamps'], measures_dict[algorithm_name]['power_values'], label=algorithm_name)[0]) # TODO : abscisse stop a 900
            lines.append(ax.plot(measures_dict[algorithm_name]['timestamps'], measures_dict[algorithm_name]['power_values'], label=algorithm_name)[0])
            i += 1

        algorithms_names = [algorithm_name for algorithm_name in measures_dict]
        figlegend.legend(lines,algorithms_names,'center')

        fig.savefig('{0}/power_mean_graph.pdf'.format(output_path))
        fig.savefig('{0}/power_mean_graph.png'.format(output_path))
        figlegend.savefig('{0}/power_mean_graph.pdf'.format(output_path))
        figlegend.savefig('{0}/power_mean_graph_legend.png'.format(output_path))


    def draw_data_points(self, experiments_ids_dict, output_path):
        # Initialization
        packets_per_second_for_all_algos = []
        packet_length_for_all_algos = []
        bytes_per_second_for_all_algos = []
        watts_per_packet_for_all_algos = []

        for algorithm in experiments_ids_dict:
            # Initialization
            packets_per_second_for_algo = []
            packet_length_for_algo = []
            bytes_per_second_for_algo = []

            traffic_dict_for_algo = self.get_traffic_measures(experiments_ids_dict[algorithm])

            """
            for experiment_id in experiments_ids_dict[algorithm]:
                # Connect to the database
                db = Database.DatabaseConnection.Instance()
                # Retrieve the traffic values from it
                traffic_dict = db.get_traffic_data_from_db(experiment_id)
                # Compute the mean packets per second and append it to the
                # corresponding array
                packets_per_second_for_algo.append(traffic_dict['nb_packets']/traffic_dict['time_interval'])
                # Compute the mean length of a packet and append it to the
                # corresponding array
                packet_length_for_algo.append(traffic_dict['sum_packets_lengths']/traffic_dict['nb_packets'])
                # Compute the mean of bytes per second and append it to the
                # corresponding array
                bytes_per_second_for_algo.append(traffic_dict['sum_packets_lengths']/traffic_dict['time_interval'])
            """

            # Compute the mean of the packets per second for the algorithm and
            # append it to the global array
            ##packets_per_second_for_all_algos.append(np.mean(packets_per_second_for_algo))
            packets_per_second_for_all_algos.append(traffic_dict_for_algo['packets_per_second'])
            # Compute the mean length of a packet for the algorithm and
            # append it to the global array
            ##packet_length_for_all_algos.append(np.mean(packet_length_for_algo))
            packet_length_for_all_algos.append(traffic_dict_for_algo['packet_mean_length'])
            # Compute the mean value of the bytes per second for the algorithm
            # and append it to the global array
            ##bytes_per_second_for_all_algos.append(np.mean(bytes_per_second_for_algo))
            bytes_per_second_for_all_algos.append(traffic_dict_for_algo['bytes_per_second'])
            # Compute the mean watts per packet for the algorithm and append it
            # to the global array
            watts_per_packet_for_all_algos.append(traffic_dict_for_algo['watts_per_packet'])

        algorithms = [ algorithm for algorithm in experiments_ids_dict ]

        # Create the graph presenting the packets per second for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, packets_per_second_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/packets_per_second.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

        # Create the graph presenting the mean packet length for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, packet_length_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/packet_length.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

        # Create the graph presenting the bytes per second for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, bytes_per_second_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/bytes_per_second.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

        # Create the graph presenting the watts per packet for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, watts_per_packet_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/watts_per_packet.png'.format(output_path))

        # Initialization
        lim_packets_per_second_for_all_algos = []
        loss_rate_for_all_algos = []
        mean_latency_for_all_algos = []

        for algorithm in experiments_ids_dict:
            # Initialization
            lim_packets_per_second_for_algo = []
            loss_rate_for_algo = [];
            mean_latency_for_algo = [];

            # For each experiment associated to the algorithm
            for experiment_id in experiments_ids_dict[algorithm]:
                # Retrieve the experiment data
                exp = Experiment.Experiment(experiment_id)
                exp.load_from_database()
                # Retrieve the latency data associated to the experiment
                latency_values = exp.compute_latency()
                lim_packets_per_second_for_algo.append(latency_values['lim_packets_per_second'])
                loss_rate_for_algo.append(latency_values['loss_rate'])
                mean_latency_for_algo.append(latency_values['mean_latency'])

            # Compute the mean of latency values for the algorithm and
            # add it to the corresponding array
            lim_packets_per_second_for_all_algos.append(np.mean(lim_packets_per_second_for_algo))
            loss_rate_for_all_algos.append(np.mean(loss_rate_for_algo))
            mean_latency_for_all_algos.append(np.mean(mean_latency_for_algo))

        # Create the graph presenting the limit of packets per second
        # for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, lim_packets_per_second_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/lim_packets_per_second.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

        # Create the graph presenting the loss rate for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, loss_rate_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/loss_rate.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

        # Create the graph presenting the mean latency for each algorithm
        fig, ax = plt.subplots(1,1)
        ax.scatter(algorithms, mean_latency_for_all_algos)
        plt.xticks(rotation='vertical')
        plt.grid()
        plt.gcf().subplots_adjust(bottom=0.5)
        fig.savefig('{0}/mean_latency.png'.format(output_path))
        # TODO: save the figure (+ clear the figure ?)

    # TODO
    def draw_boxplot(self, measures_dict, output_path):
        plt.figure()
        plt.grid() # Draw a grid
        plt.xlabel('Experiments ID') # Name x axis
        plt.xticks(rotation=90) # Rotate the text of the x axis
        plt.gcf().subplots_adjust(bottom=0.5)
        plt.ylabel('Power (in watts)') # Name y axis
        plt.title('Means of the power (in watts) of all nodes for two experiments') # Set the title of the figure

        # Cut 5% highest and 5% lowest values
        for algorithm in measures_dict:
            power_values = measures_dict[algorithm]['power_values']
            min_value = min(power_values)
            max_value = max(power_values)
            interval = max_value-min_value
            min_kept_value = min_value+interval*5/100
            max_kept_value = max_value-interval*5/100

            kept_values = []
            for value in power_values:
                if (value > min_kept_value and value < max_kept_value):
                    kept_values.append(value)

            measures_dict[algorithm]['power_values'] = kept_values

        algorithms = [algorithm for algorithm in measures_dict]
        plt.boxplot([measures_dict[algorithm_name]['power_values'] for algorithm_name in algorithms])
        pylab.xticks(range(1,len(algorithms)+1), algorithms) # Associate to each boxplot the experiment id

        # Save the file
        plt.savefig('{0}/power_mean_boxplot.pdf'.format(output_path))
        plt.savefig('{0}/power_mean_boxplot.png'.format(output_path))

    """
    For each algorithm, retrieve the packets lengths of the messages of the
    experiments related to it, and present them in a boxplot.
    Save the boxplots (one per algorithm) in the output folder undef the name
    of packets_lengths_boxplots.png

    @type self: Analytics
    @param self: The current instance of the Analytics module

    @type algorithms: dict
    @param algorithms: Dictionary associating to each key (algorithm) an array
                       of experiments IDs

    @type output_folder_path: string
    @param output_folder_path: the path of the output folder
    """
    def draw_boxplots_packets_lengths(self, algorithms, output_folder_path):
        # Initialize the global dictionary storing all the values
        packets_lengths_values = {}
        # For each algorithm
        for algorithm in algorithms:
            # Initialize the array storing the values related to this algorithm
            packets_lengths_values[algorithm] = []
            # For each experiment associated to the algorithm:
            for experiment_id in algorithms[algorithm]:
                # Instanciate an experiment object from its ID
                experiment = Experiment.Experiment(experiment_id)
                experiment.load_from_database()
                # Retrieve the values to add in the boxplots
                packets_lengths = experiment.get_packets_lengths()
                # Append these values in the array
                packets_lengths_values[algorithm].extend(packets_lengths)

        # Draw the boxplots
        # Initialize the parameters of the figure
        plt.figure()
        plt.grid() # Draw a grid
        plt.xlabel('Algorithms') # Name x axis
        plt.xticks(rotation=90) # Rotate the text of the x axis
        plt.gcf().subplots_adjust(bottom=0.5)
        plt.ylabel('Packets lengths (in bytes)') # Name y axis
        plt.title('Boxplots of the packets lengths (in bytes) of the algorithms') # Set the title of the figure

        # Draw a boxplot for each algorithm
        # TODO: transformer le dict en array
        plt.boxplot([packets_lengths_values[algorithm] for algorithm in algorithms])
        pylab.xticks(range(1,len(algorithms)+1), algorithms) # Associate to each boxplot the experiment id

        # Save the file
        plt.savefig('{0}/packets_lengths_boxplots.png'.format(output_folder_path))


    """
    For each algorithm, retrieve the values of the latencies of the messages of the
    experiments related to it, and present them in a boxplot.
    Save the boxplots (one per algorithm) in the output folder under
    the name of latency_boxplots.png

    @type self: Analytics
    @param self: The current instance of the Analytics module

    @type algorithms: dict
    @param algorithms: Dictionary associating to each key (algorithm) an array
                       of experiments IDs

    @type output_folder_path: string
    @param output_folder_path: the path of the output folder
    """
    def draw_boxplots_latency(self, algorithms, output_folder_path):
        # Initialize the global dictionary storing all the values
        latencies_values = {}
        # For each algorithm:
        for algorithm in algorithms:
            # Initialize the array storing the values related to this algorithm
            latencies_values[algorithm] = []
            # For each experiment associated to the algorithm:
            for experiment_id in algorithms[algorithm]:
                # Instanciate an experiment object from its ID
                experiment = Experiment.Experiment(experiment_id)
                experiment.load_from_database()
                # Retrieve the values to add in the boxplot
                latencies = experiment.get_latencies()
                # Append these values in the array
                latencies_values[algorithm].extend(latencies)

        # Draw the boxplots
        # Initialize the parameters of the figure
        plt.figure()
        plt.grid() # Draw a grid
        plt.xlabel('Algorithms') # Name x axis
        plt.xticks(rotation=90) # Rotate the text of the x axis
        plt.gcf().subplots_adjust(bottom=0.5)
        plt.ylabel('Latency (in seconds)') # Name y axis
        plt.title('Boxplots of the latencies (in seconds) of the algorithms') # Set the title of the figure

        # Draw a boxplot for each algorithm
        # TODO: transformer le dict en array
        plt.boxplot([latencies_values[algorithm] for algorithm in algorithms])
        pylab.xticks(range(1,len(algorithms)+1), algorithms) # Associate to each boxplot the experiment id

        # Save the file
        plt.savefig('{0}/latency_boxplots.png'.format(output_folder_path))



    def create_report(self, experiments_ids_dict, output_path): # TODO: set the output path to the graphs
        print('[Analytics] Create report')
        print(time.time())
        # Generate the page
        pdf = FPDF()
        pdf.add_page()

        pdf.set_font("Arial", size=12)
        col_width = pdf.w/4.5
        row_height = pdf.font_size
        spacing = 1.5

        # Summary
        pdf.cell(200, 10, txt="Experiments", ln=1, align="C")
        pdf.ln(5)
        for algorithm in experiments_ids_dict:
            pdf.ln(5)
            pdf.cell(200, 10, txt='Algorithm {0}'.format(algorithm), ln=1, align="C")

            exp = Experiment.Experiment(experiments_ids_dict[algorithm][0])
            exp.load_from_database()
            # Number of experiments
            pdf.cell(col_width, row_height*spacing, txt='Number of experiments', border=1)
            pdf.cell(col_width, row_height*spacing, txt=str(len(experiments_ids_dict[algorithm])), border=1)
            pdf.ln(row_height*spacing)
            # Duration
            pdf.cell(col_width, row_height*spacing, txt='Duration', border=1)
            pdf.cell(col_width, row_height*spacing, txt=str(exp.duration), border=1)
            pdf.ln(row_height*spacing)
            # OS
            pdf.cell(col_width, row_height*spacing, txt='Operating system', border=1)
            pdf.cell(col_width, row_height*spacing, txt=exp.os, border=1)
            pdf.ln(row_height*spacing)
            # Architecture
            pdf.cell(col_width, row_height*spacing, txt='Architecture', border=1)
            pdf.cell(col_width, row_height*spacing, txt=exp.architecture, border=1)
            pdf.ln(row_height*spacing)
            # Topology
            pdf.cell(col_width, row_height*spacing, txt='Topology', border=1)
            pdf.cell(col_width, row_height*spacing, txt=exp.topology, border=1)
            pdf.ln(row_height*spacing)

        # Power data
        pdf.add_page()
        pdf.cell(200, 10, txt="Graphs", ln=1, align="C")
        pdf.ln(5)
        pdf.image('{0}/power_mean_graph.png'.format(output_path), w=pdf.w*0.75)
        pdf.ln(5)
        pdf.image('{0}/power_mean_graph_legend.png'.format(output_path), w=pdf.w*0.75)
        pdf.ln(5)
        pdf.add_page()
        pdf.image('{0}/power_mean_boxplot.png'.format(output_path), w=pdf.w*0.75)


        # Traffic data
        pdf.add_page()

        is_first_line = True
        for algorithm in experiments_ids_dict:
            # Get the traffic data related to the experiments of the algorithm
            traffic_dict = self.get_traffic_measures(experiments_ids_dict[algorithm])

            pdf.cell(200, 10, txt='Algorithm {0}'.format(algorithm), ln=1, align="C")

            # First array (packets per second and bytes per second)
            data_names = ["Packets per second", "Bytes per second", "Packet mean length", "Watts per packet"]
            data_types = ["packets_per_second", "bytes_per_second", "packet_mean_length", "watts_per_packet"]
            #data_names.insert(0, "Algorithms")
            # Headers
            for data_name in data_names:
                pdf.cell(col_width, row_height*spacing, txt=data_name, border=1)
            pdf.ln(row_height*spacing)

            # Content
            #pdf.cell(col_width, row_height*spacing, txt=algorithm, border=1)
            for data_type in data_types:
                pdf.cell(col_width, row_height*spacing, txt=str(traffic_dict[data_type]), border=1)
            pdf.ln(row_height*spacing)

        # Latency
        pdf.add_page()

        #data_names = ['Packets per second', 'Packets loss when no wait before sending packets', 'Latency']
        data_names = ['Packets per second', 'Packets loss', 'Latency']
        data_types = ['lim_packets_per_second', 'loss_rate', 'mean_latency']

        for algorithm in experiments_ids_dict:
            # - Title
            pdf.cell(200, 10, txt='Algorithm {0}'.format(algorithm), ln=1, align="C")

            # Headers
            for data_name in data_names:
                pdf.cell(col_width, row_height*spacing, txt=data_name, border=1)
            pdf.ln(row_height*spacing)

            # Content
            contents = {}
            for data_type in data_types:
                print('-- DATATYPE: {0} --'.format(data_type))
                contents[data_type] = 0
                # For each experiment related to the algorithm
                for experiment_id in experiments_ids_dict[algorithm]:
                    exp = Experiment.Experiment(experiment_id)
                    exp.load_from_database()
                    latency_values = exp.compute_latency()
                    print('Latency_values')
                    print(latency_values)
                    if (latency_values[data_type] == None):
                        content = 'None'
                    else:
                        content = latency_values[data_type]/len(experiments_ids_dict[algorithm])
                    print('[Script - Latency Analysis] Number of experiments for algorithm {0}: {1}'.format(algorithm, len(experiments_ids_dict[algorithm])))
                    print('[Script - Latency Analysis] Mean of "{0}" for algorithm {1}: {2}'.format(data_type, algorithm, content))
                pdf.cell(col_width, row_height*spacing, txt=str(content), border=1)
            pdf.ln(row_height*spacing)

        # Save the file
        pdf.output('{0}/report.pdf'.format(output_path))




    # TODO: def get_latency(node_id, experiment_id):
        # TODO: regarder les timestamps affiches pour le chiffrement/dechiffrement
        # dans le fichier data/<experiment_name>/log/<node_name>.log
