# Charger un programme sous Contiki

```
git clone https://github.com/iot-lab/iot-lab.git
cd iot-lab/
make setup-contiki

cd parts/contiki/core/lib
cp -r ~/Bureau/Gasper/Git/lightweigt-cryptography-benchmark/lilliput-ae-contiki/Contiki-3.0/core/lib/lilliputae/ ./
cd ../../examples/iotlab/
cp -r ~/Bureau/Gasper/Git/lightweigt-cryptography-benchmark/lilliput-ae-contiki/Contiki-3.0/examples/encrypt/lilliput-app/ ./
cd lilliput-app/
/* Modifier le Makefile pour le mettre comme sur le Git si non réinitialisé */
/* Appliquer les modifications de https://gitlab.inria.fr/lahmadi/lightweigt-cryptography-benchmark/blob/master/lilliput-ae-contiki/Contiki-3.0/lilliput%20to%20contiki%20modif.txt dans le Makefile.include de Contiki

Pour AES-CCM, enlever la ligne où "TAG_BYTES" apparaît dans `server.c` (c'est un print)
 */
make TARGET=iotlab-m3
```
