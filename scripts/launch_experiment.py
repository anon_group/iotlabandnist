#!/usr/bin/env python3

from threading import Thread
import subprocess
import time
import pprint
import paramiko
from paramiko import SSHClient
from scp import SCPClient
import experiment as Experiment
import json
import iotlabcli
from iotlabcli import experiment
import iotlabcli.parser.common
import iotlabcli.parser.node
import utils as Utils
import sys
import queue
import shutil
import os

# https://github.com/iot-lab/iot-lab-client/blob/master/iotlabclient/client_README.md
# https://api.iot-lab.info/#/

pp = pprint.PrettyPrinter(indent=4)

## Class Experiment
# Launch an experiment on IoT-Lab
class ExperimentToLaunch(Thread):

    ## **Constructor**
    # Initialize the parameters of the experiment
    #
    # @param self The object pointer
    # @param architecture The architecture of the nodes on which th experiment is launched
    # @param topology The topology to be used in the experiment
    # @param server_firmware The firmware to flash on the server node(s)
    # @param client_firmware The firmware to flash on the client node(s)
    # @param duration Duration of the experiment in minutes
    def __init__(self, username, password, ssh_key_file, site, architecture, topology, os, algorithm, #server_firmware, client_firmware,
                 server_node, client_nodes, duration, profile, threads_queue):
        Thread.__init__(self)
        # Set variables
        self.username = username
        self.password = password
        self.ssh_key_file = ssh_key_file
        self.site = site
        self.architecture = architecture
        self.topology = topology
        self.algorithm = algorithm
        self.server_firmware = Utils.get_firmware_path(algorithm, 'server', architecture, os)
        self.client_firmware = Utils.get_firmware_path(algorithm, 'client', architecture, os)
        self.duration = duration
        self.os = os
        self.experiment_id = 0
        self.original_experiment_id = 0
        self.associated_experiment_id = 0
        self.profile = profile
        self.threads_queue = threads_queue

        # Get the nodes from the architecture and the topology
        self.server_node = Utils.get_server_node(self.architecture, self.topology)
        self.client_nodes = Utils.get_client_nodes(self.architecture, self.topology)

    ## Launch the experiment
    #
    # @param self The object pointer
    def run(self):
        print("[Experiment] Launching experiment...")
        # Launch the experiment through IoT-LAB API
        self.launch_experiment()
        print("[Experiment] End.")


    ## Save a JSON description of the experiment
    #
    # @param self The object pointer
    def save_json_description(self):
        experiment_description = {
            'experiment_id': self.experiment_id,
            'os': self.os,
            'site': self.site,
            'architecture': self.architecture,
            'server_firmware': self.server_firmware,
            'client_firmware': self.client_firmware,
            'server_node': self.server_node,
            'client_nodes': self.client_nodes,
            'duration': self.duration,
            'profile': self.profile,
        }
        with open(Utils.get_json_description_path(self.experiment_id), 'w') as f:
            json.dump(experiment_description,f)

    ## Launch the experiment through the IoT-LAB API
    #
    # @param self The object pointer
    def launch_experiment(self):
        # Define the API and the associated credentials
        api = iotlabcli.Api(self.username,self.password)

        # Connect
        with iotlabcli.parser.common.catch_missing_auth_cli():
            # Add resources related to the client nodes
            client_resources = experiment.exp_resources(self.client_nodes, self.client_firmware)
            client_resources['profile'] = self.profile
            resources = [client_resources]

            # Add resources related to the server nodes
            server_resources = experiment.exp_resources([self.server_node], self.server_firmware)
            server_resources['profile'] = self.profile
            resources.append(server_resources)

            # Launch the experiment
            new_experiment = experiment.submit_experiment(api,
                                                          "", # name
                                                          self.duration, # TODO: add 5min before and 5min after?
                                                          resources)

            # Get the state of the experiment
            self.experiment_id = new_experiment['id']
            print("[Experiment] The experiment {0} has been launched.".format(self.experiment_id))
            self.save_logs()
            print("[Experiment {0}] Capturing logs...".format(self.experiment_id))
            #self.set_pcap2()
            #print("[Experiment {0}] Capturing PCAP...".format(self.experiment_id))

            new_experiment_data = experiment.get_experiment(api, self.experiment_id, '')
            state = new_experiment_data['state']

            pcap_launched = False
            # Wait until the experiment is terminated
            while state != "Terminated":
                # TODO: wait duration quand l experience est lancee
                time.sleep(30) # Wait 30 seconds
                new_experiment_data = experiment.get_experiment(api, self.experiment_id, '')

                state = new_experiment_data['state']

                if (state == 'Running' and pcap_launched == False):
                    print('Launching pcap creation')
                    # TODO
                    self.set_pcap()
                    #self.save_logs()
                    pcap_launched = True

                print("[Experiment {0}] State: {1}.".format(self.experiment_id, state))

            # Save the measures files locally
            self.get_measures(str(self.experiment_id))
            self.save_json_description()
            data_folder_path = Utils.get_experiment_folder_path(self.experiment_id)

            # Save the experiment into the database
            exp = Experiment.Experiment(self.experiment_id)
            print('ORIG: {0}'.format(self.original_experiment_id))
            exp.set_properties(self.os, self.architecture, self.topology,
                               self.algorithm, self.duration, self.profile,
                               data_folder_path, self.original_experiment_id, self.associated_experiment_id)
            print('Original exp id: {0}'.format(self.original_experiment_id))
            exp.save_in_database()

            # Return the experiment id
            self.threads_queue.put(self.experiment_id)
            return self.experiment_id


    ## Get PCAP file from an experiment
    # TODO
    def set_pcap(self):
        #https://github.com/iot-lab/aggregation-tools/blob/997bba9e08d51b779a90159b43aca7fe19b2e5ab/iotlabaggregator/sniffer.py
        #nodes_list = SnifferAggregator.select_nodes(opts)
        # Run the aggregator
        #with SnifferAggregator(nodes_list, opts.outfd, opts.raw) as aggregator:
        #    aggregator.run()
        #https://pypi.org/project/iotlabcli/
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        k = paramiko.RSAKey.from_private_key_file(self.ssh_key_file)
        ssh.connect('{0}.iot-lab.info'.format(self.site),username=self.username,password=self.password, pkey=k)
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('sniffer_aggregator -l {0},{1},{2} -o {3}-server.pcap'.format(self.site,self.architecture,Utils.get_server_node_id(self.architecture,self.topology), self.experiment_id))
        #ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('sniffer_aggregator -o {0}-server.pcap'.format(self.experiment_id))
        for node_id in Utils.get_client_nodes_ids(self.architecture, self.topology):
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('sniffer_aggregator -l {0},{1},{2} -o {3}-client{4}.pcap'.format(self.site,self.architecture,node_id, self.experiment_id,node_id))
        ssh.close()

    def set_pcap2(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        k = paramiko.RSAKey.from_private_key_file(self.ssh_key_file)
        ssh.connect('{0}.iot-lab.info'.format(self.site),username=self.username,password=self.password, pkey=k)
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('screen -dmS {0}-{1} bash -cx "iotlab-experiment wait -i {0} && sniffer_aggregator -l {2},{3},{1} -o {0}-server.pcap"'.format(self.experiment_id,Utils.get_server_node_id(self.architecture,self.topology), self.site, self.architecture))
        for node_id in Utils.get_client_nodes_ids(self.architecture, self.topology):
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('screen -dmS {3}-{2} bash -cx "sniffer_aggregator -l {0},{1},{2} -o {3}-client{2}.pcap"'.format(self.site,self.architecture,node_id, self.experiment_id))
        ssh.close()

    def save_logs(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        k = paramiko.RSAKey.from_private_key_file(self.ssh_key_file)
        ssh.connect('{0}.iot-lab.info'.format(self.site),username=self.username,password=self.password, pkey=k)
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('screen -dmS {0} bash -cx "iotlab-auth -u {0} -p {1} && iotlab-experiment wait -i {2} && serial_aggregator -i {2} > {2}.log"'.format(self.username, self.password, self.experiment_id))
        print('iotlab-auth -u {0} -p {1} &&iotlab-experiment wait -i {2} && serial_aggregator -i {2} > {2}.log'.format(self.username, self.password, self.experiment_id))
        print('Waited.')
        print('-----')
        print('-----')
        ssh.close()

    ## Retrieve the measurement associated to an experiment
    #
    # @param self The object pointer
    # @param experiment_id The ID of the experiment
    def get_measures(self, experiment_id): # 194985
        ssh = SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        k = paramiko.RSAKey.from_private_key_file(self.ssh_key_file)
        ssh.connect(hostname=self.site+'.iot-lab.info', username=self.username, pkey=k)
        scp = SCPClient(ssh.get_transport())
        scp.get(remote_path='/senslab/users/{0}/.iot-lab/{1}'.format(self.username,experiment_id), local_path='data/{0}'.format(experiment_id), recursive=True)
        scp.get(remote_path='/senslab/users/{0}/{1}-server.pcap'.format(self.username,experiment_id), local_path=Utils.get_server_pcap_path(experiment_id).format(experiment_id), recursive=True)
        scp.get(remote_path='/senslab/users/{0}/{1}.log'.format(self.username,experiment_id), local_path=Utils.get_serial_aggregator_log_path(experiment_id), recursive=True)
        for node_id in Utils.get_client_nodes_ids(self.architecture, self.topology):
            scp.get(remote_path='/senslab/users/{0}/{1}-client{2}.pcap'.format(self.username,experiment_id, node_id), local_path=Utils.get_server_pcap_path(experiment_id).format(experiment_id), recursive=True)
        scp.close()
        #client = scp.Client(host='site.iot-lab.info', user='blanc', keyfile='keyfile')

    ## Get the ID of the experiment related to this class
    def get_experiment_id(self):
        return self.experiment_id

    def set_specific_client_firmware(self, new_client_firmware_path):
        self.client_firmware = new_client_firmware_path

    def set_specific_server_firmware(self, new_server_firmware_path):
        self.server_firmware = new_server_firmware_path

    def create_new_firmware(self, experiment, time_between_packets):
        # Generate the files
        # Folders initialization
        input_folder = Utils.get_default_firmware_folder_path(experiment)
        output_folder = Utils.get_custom_firmware_folder_path(experiment, time_between_packets)

        print("Input folder: {0}".format(input_folder))
        print("Output folder: {0}".format(output_folder))

        # Copy the server file and the Makefile
        shutil.copyfile('{0}/server.c'.format(input_folder), '{0}/server.c'.format(output_folder))
        shutil.copyfile('{0}/Makefile'.format(input_folder), '{0}/Makefile'.format(output_folder))
        if os.path.exists('{0}/project-conf.h'.format(input_folder)):
            shutil.copyfile('{0}/project-conf.h'.format(input_folder), '{0}/project-conf.h'.format(output_folder))

        # Copy the client and modify the delay between the packets
        # Open the default firmware file to read it
        with open('{0}/client.c'.format(input_folder), 'r') as infile:
            # Open the new firmware file, to write
            with open('{0}/client.c'.format(output_folder), 'w') as outfile:
                for line in infile:
                    outfile.write(line.replace('#define DATA_INTERVAL_TRANS		(0 * CLOCK_SECOND)', '#define DATA_INTERVAL_TRANS		({0} * CLOCK_SECOND)'.format(time_between_packets)))

        print(output_folder)
        # Compile it
        process = subprocess.Popen('ARCH_PATH=../../../../arch make TARGET=iotlab BOARD=m3 savetarget',
                                   cwd=output_folder,
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()

        process = subprocess.Popen('ARCH_PATH=../../../../arch make',
                                   cwd=output_folder,
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        return output_folder



    def set_original_experiment(self, experiment_id):
        print('Set original experiment: {0}'.format(experiment_id))
        self.original_experiment_id = experiment_id

    ## TODO
    # Returns the number of packets per second computed to avoid packets loss
    def compute_latency(self):
        # Initialization
        client_messages = []
        server_messages = []
        topology_name = '{0}_{1}'.format(self.architecture, self.topology)

        # Open the log file associated to the experiment
        # NB: this is done for experiments with one client and one server
        with open(Utils.get_serial_aggregator_log_path(self.experiment_id), 'r') as infile:
            for line in infile:
                line1 = line.split(';')

                if (len(line1) > 2):
                    # Client
                    if (line1[1] in ["{0}-{1}".format(self.architecture, client_id_number) for client_id_number in Utils.get_clients_ids_numbers(topology_name)]):
                        print("Line:")
                        print(line)
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            client_messages.append(int(line2[1][:-1]))

                    # Server
                    elif (line1[1] == "{0}-{1}".format(self.architecture, Utils.get_server_id_number(topology_name))):
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            server_messages.append(int(line2[1][:-1]))

        total_sent_packets = len(client_messages)
        total_received_packets = len(server_messages)
        lim_packets_per_second = total_received_packets/(self.duration*60)
        loss_rate = (total_sent_packets-total_received_packets)*100/total_sent_packets

        # TODO
        Utils.store_lim_packets_per_second(self.experiment_id, lim_packets_per_second)
        Utils.store_packets_loss(self.experiment_id, loss_rate)

        print('[Experiment {0}] Total received messages: {1}'.format(self.experiment_id, total_received_packets))
        print('[Experiment {0}] Lim packets per second: {1}'.format(self.experiment_id, lim_packets_per_second))
        return lim_packets_per_second
