#!/usr/bin/env python3
import argparse
import sys
import configargparse
from enum import Enum
import launch_experiment as LaunchExperiment
import analytics as Analytics
import concurrent.futures
import queue
import time
import os
import utils as Utils
import database as Database
import experiment as Experiment



# Parse the input arguments
PARSER = configargparse.ArgParser()
PARSER.add('-c', '--config', required=True, is_config_file=True,
           help='Configuration file path')

# Credentials
PARSER.add('-u', '--username', dest='username', #required=True,
           help='Username to connect on IoT-LAB')
PARSER.add('-pwd', '--password', dest='password', #required=True,
           help='Password to connect on IoT-LAB')
PARSER.add('-k', '--ssh-key-file', dest='ssh_key_file', #required=True,
           help='SSH key file to perform a SCP on the IoT-LAB user space')

# Option to only launch experiments, or analytics, or both of them ("launch-only", "analyse-only", "launch-and-analyse")
PARSER.add('-o', '--operation', dest='operation', required=True, # TODO: for each choice, check if all the required arguments are here
           type=Utils.Operation, help='Kind of operation on the experiment (can be "launch-and-analyse", "launch-only" or "analyse-only")')



# To analyse several
PARSER.add('-n', '--experiments-number', dest='experiments_number', type=int,
           help='Max number of experiments in order to do a mean')

PARSER.add('--output', dest='output',
           help='Output directory')

# To launch
PARSER.add('-a', '--architecture', dest='architecture', #required=True,
          type=Utils.Architecture, help='Architecture of the nodes (can be "m3")')
PARSER.add('-t', '--topology', dest='topology', #required=True,
           type=Utils.Topology, help='Topology of the nodes (can be "line", "grid" or "star")')
PARSER.add('-d', '--duration', dest='duration', type=int, #required=True,
           help='Duration of the experiment')
PARSER.add('--os', dest='os', #required=True,
           type=Utils.OperatingSystem, help='OS on the nodes')
PARSER.add('-p', '--profile', dest='profile',
           type=Utils.ProfileType, help='Profile of the nodes')
PARSER.add('--algorithms', dest='algorithms', type=Utils.Algorithm,
            metavar='N', nargs='+',
            help='Names of the algorithms to use')

# To analyze only
PARSER.add('-e', '--experiments', dest='experiments_ids', type=int,#required?
           metavar='N', nargs='+',
           help='IDs of the experiments to compare')
PARSER.add('--data-processing', dest='data_processing',
           type=Utils.DataProcessing, help='How to process the experiment data')

## For each type of operation, check if the required inputs are set
#
# @param arguments The arguments provided by the user
def get_missing_inputs(arguments):
    missing_inputs = []

    if (arguments.operation.__str__() == 'launch-only'):
        if (not hasattr(arguments, 'username')):
            missing_inputs.append('username')
        if (not hasattr(arguments, 'password')):
            missing_inputs.append('password')
        if (not hasattr(arguments, 'os')):
            missing_inputs.append('os')
        if (not hasattr(arguments, 'duration')):
            missing_inputs.append('duration')
        if (not hasattr(arguments, 'architecture')):
            missing_inputs.append('architecture')
        if (not hasattr(arguments, 'topology')):
            missing_inputs.append('topology')
        if (not hasattr(arguments, 'profile')):
            missing_inputs.append('profile')
        if (not hasattr(arguments, 'algorithms')):
            missing_inputs.append('algorithms')

    elif (arguments.operation.__str__() == 'analyse-only'):
        if (not hasattr(arguments, 'experiments_ids')):
            missing_inputs.append('experiments')
        # TODO: pour l instant
        if (not hasattr(arguments, 'architecture')):
            missing_inputs.append('architecture')
        if (not hasattr(arguments, 'topology')):
            missing_inputs.append('topology')
        if (not hasattr(arguments, 'data_processing')):
            missing_inputs.append('data_processing')

    elif (arguments.operation.__str__() == 'launch-and-analyse'):
        if (not hasattr(arguments, 'username')):
            missing_inputs.append('username')
        if (not hasattr(arguments, 'password')):
            missing_inputs.append('password')
        if (not hasattr(arguments, 'ssh_key_file')):
            missing_inputs.append('ssh-key-file')
        if (not hasattr(arguments, 'os')):
            missing_inputs.append('os')
        if (not hasattr(arguments, 'duration')):
            missing_inputs.append('duration')
        if (not hasattr(arguments, 'architecture')):
            missing_inputs.append('architecture')
        if (not hasattr(arguments, 'topology')):
            missing_inputs.append('topology')
        if (not hasattr(arguments, 'profile')):
            missing_inputs.append('profile')
        if (not hasattr(arguments, 'algorithms')):
            missing_inputs.append('algorithms')
        if (not hasattr(arguments, 'data_processing')):
            missing_inputs.append('data_processing')

    elif (arguments.operation.__str__() == 'analyse-several'):
        # Credentials to load experiments data if not already done
        if (not hasattr(arguments, 'username')):
            missing_inputs.append('username')
        if (not hasattr(arguments, 'password')):
            missing_inputs.append('password')
        if (not hasattr(arguments, 'ssh_key_file')):
            missing_inputs.append('ssh-key-file')
        # Parameters of the experiments
        if (not hasattr(arguments, 'os')):
            missing_inputs.append('os')
        if (not hasattr(arguments, 'duration')):
            missing_inputs.append('duration')
        if (not hasattr(arguments, 'architecture')):
            missing_inputs.append('architecture')
        if (not hasattr(arguments, 'topology')):
            missing_inputs.append('topology')
        if (not hasattr(arguments, 'algorithms')):
            missing_inputs.append('algorithms')
        if (not hasattr(arguments, 'experiments_number')):
            missing_inputs.append('experiments-number')
        if (not hasattr(arguments, 'output')):
            missing_inputs.append('output')

    else:
        sys.exit('An error occurred while checking the arguments.')

    return missing_inputs



## Launch the list of experiments
#
# @param username The username of the IoT-LAB account
# @param password The password of the IoT-LAB account
# @param ssh_key_file The key of the SSH connection to IoT-LAB
# @param algorithms The list of algorithms to use
# @param architecture The architecture of the nodes to use
# @param os The OS used on the nodes
# @param topology The topology of the nodes to use
# @param duration The duration of each experiment (in minutes)
# @param profile The profile set on the nodes
def launch_experiments(username, password, ssh_key_file, algorithms, architecture,
                       os, topology, duration, profile):
    # Initialize the list of experiments ids
    experiments_ids = []

    # Get the nodes
    client_nodes = Utils.get_client_nodes(architecture, topology)
    server_node = Utils.get_server_node(architecture, topology)

    # Get the site
    site = Utils.get_site(architecture, topology)

    # Create a thread queue
    threads_queue = queue.Queue()

    # Create the threads (each one of them is related to an experiment)
    experiments = []
    for algorithm in algorithms:
        print('SSH KEY FILE: {0}'.format(ssh_key_file))
        experiment = LaunchExperiment.ExperimentToLaunch(username, password, ssh_key_file,
                                                  site, architecture, topology, os, algorithm,
                                                  server_node, client_nodes,
                                                  duration, profile, threads_queue)
        experiments.append(experiment)

    # Launch the experiments
    for experiment in experiments:
        experiment.start()

    print('Waiting for join...')
    print(experiments)

    # Wait for the experiments to end
    for experiment in experiments:
        experiment.join()

    print('Joined.')

    # Launch corresponding experiments to compute the latency
    experiments2 = []
    for experiment in experiments:
        # Compute the latency
        lim_packets_per_second = experiment.compute_latency()
        # Modify and compile the firmware to compute latency
        new_firmware_folder = experiment.create_new_firmware(experiment, 1/lim_packets_per_second)

        # TODO

        # Launch the new experiment
        experiment2 = LaunchExperiment.ExperimentToLaunch(username, password, ssh_key_file,
                                                          site, architecture, topology, os, '{0}-custom'.format(algorithm),
                                                          server_node, client_nodes,
                                                          duration, profile, threads_queue)

        new_client_firmware_path = '{0}/client.iotlab'.format(new_firmware_folder)
        print("[Experiment {0}] New client firmware path: {1}".format(experiment.experiment_id, new_client_firmware_path))
        experiment2.set_specific_client_firmware(new_client_firmware_path)
        new_server_firmware_path = '{0}/server.iotlab'.format(new_firmware_folder)
        experiment2.set_specific_server_firmware(new_server_firmware_path)
        print("[Experiment {0}] New server firmware path: {1}".format(experiment.experiment_id, new_server_firmware_path))
        experiment2.set_original_experiment(experiment.experiment_id)
        # Add to the database?
        # TODO
        experiments2.append(experiment2)

    for experiment in experiments2:
        experiment.start()

    for experiment in experiments2:
        experiment.join()



    experiments_ids = list(threads_queue.queue)
    return experiments_ids





## Main function
def main(args=None):
    print('[Global] Starting...')
    print(time.time())



    # Create the database if it does not exist
    if (not Database.database_exists()):
        # Connect to the database
        db = Database.DatabaseConnection.Instance()
        print('[Global] Creating the database...')
        db.create_all_tables()
    db = Database.DatabaseConnection.Instance()


    print('[Global] Checking inputs...')
    # Get the input variables
    opts = PARSER.parse_args()
    # Check if all the required variables are set
    missing_inputs = get_missing_inputs(opts)
    if (len(missing_inputs) > 0):
        error_message = "The following arguments are missing for such an operation:"
        for missing_input in missing_inputs:
            error_message += " "+missing_input
        sys.exit(error_message)
    operation = opts.operation.__str__()

    # Create the experiments
    # If the experiments have to be launched
    if (operation == 'launch-only' or operation == 'launch-and-analyse'):
        print('[Global] Launching experiments...')
        algorithms = [algorithm.__str__() for algorithm in opts.algorithms]

        experiments_ids = launch_experiments(opts.username, opts.password,
                                             opts.ssh_key_file,
                                             algorithms,
                                             opts.architecture.__str__(),
                                             opts.os.__str__(),
                                             opts.topology.__str__(),
                                             opts.duration,
                                             opts.profile.__str__())

    # Compute them
    # If the results have to be displayed
    if (operation == 'analyse-only' or operation == 'launch-and-analyse'):
        if ('experiments_ids' not in locals()) and hasattr(opts, 'experiments_ids'):
            experiments_ids = opts.experiments_ids
        print('[Global] Computing results...')
        analytics = Analytics.Analytics()
        analytics.display_results(experiments_ids)

    # If several experiments are considered to be analysed
    if (operation == 'analyse-several'):
        print('[Global] Analyse the last experiments...')
        print(time.time())
        algorithms = [algorithm.__str__() for algorithm in opts.algorithms]
        if not os.path.exists(opts.output):
            os.makedirs(opts.output)
        analytics = Analytics.Analytics()
        analytics.analyse_last_experiments(opts.duration,
                                           opts.architecture.__str__(),
                                           opts.topology.__str__(),
                                           opts.os.__str__(),
                                           algorithms,
                                           opts.experiments_number,
                                           opts.output)

    # Close the connection to the database
    db.close()

    print('[Global] End.')
    print(time.time())


if __name__ == "__main__":
    main()
