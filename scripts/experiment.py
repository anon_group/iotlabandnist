import database as Database
import pyshark
import common
import subprocess
import utils as Utils
from datetime import datetime

class Experiment:

    # Initializer
    def __init__(self, experiment_id):
        self.experiment_id = experiment_id


    # Set the properties of the experiment
    def set_properties(self, os, architecture, topology, algorithm, duration, profile,
                       data_folder_path, original_experiment_id, associated_experiment_id):
        self.os = os
        self.architecture = architecture
        self.topology = topology
        self.algorithm = algorithm
        self.duration = duration
        self.profile = profile
        self.data_folder_path = data_folder_path
        if (original_experiment_id == None):
            self.original_experiment_id = 0
        else:
            self.original_experiment_id = original_experiment_id
        if (associated_experiment_id == None):
            self.associated_experiment_id = 0
        else:
            self.associated_experiment_id = associated_experiment_id

    # Save in database
    def save_in_database(self):
        db = Database.DatabaseConnection.Instance()
        Database.save_experiment_in_database(self.experiment_id, self.os, self.architecture,
                           self.topology, self.algorithm, self.duration,
                           self.profile, self.data_folder_path,
                           self.original_experiment_id, self.associated_experiment_id)

    def save_measures(self):
        # Check if at least one measure has been stored in the database
        db = Database.DatabaseConnection.Instance()
        if (db.get_nb_measures_for_experiment(self.experiment_id) < 1):
            # If not, extract the measure from the file and store them
            self.__save_all_measures()

    # Private method
    def __save_all_measures(self):
        # If the experiment properties are not stored in the object,
        # load them from the database
        if not hasattr(self, 'os'):
            self.load_from_database()

        # Initialize the data array
        #all_nodes_data = np.ndarray((0,5), dtype=[('timestamp',float),('power',float),('node_id',object)])

        # Store the names of the nodes related to the experiment in a list
        experiments_nodes = Utils.get_client_nodes_names(self.architecture, self.topology)
        experiments_nodes.append(Utils.get_server_node_name(self.architecture, self.topology))

        # For each node, store its measures in the database
        db = Database.DatabaseConnection.Instance()
        for node_id in experiments_nodes:
            print('\n\nNode {}'.format(node_id))

            # Get its data from the OML file
            print("Begin get_experiment_data")
            node_data = self.get_experiment_data(node_id)
            print("End get_experiment_data")

            # Add the measure in the database
            for data in node_data:
                if (str(data['power']) != "nan"):
                    db.save_measure_without_commit(data['timestamp'], data['power'], node_id, self.experiment_id)

        # Commit the changes
        db.commit()

    def get_experiment_data(self, node_id):
        # Bounds of the graph's axis
        begin = 0
        end = -1

        # Types of measures
        MEASURES_D = Utils.get_measures()

        filename = 'data/{0}/consumption/{1}.oml'.format(self.experiment_id, node_id)
        print(filename)
        print("Begin common")
        data = common.oml_load(filename, 'consumption', MEASURES_D.values())[begin:end]
        print("End common")
        return data

    # Load from database
    def load_from_database(self):
        # Get the experiment data
        db = Database.DatabaseConnection.Instance()
        experiment_data = db.get_experiment_data(self.experiment_id)
        # Set experiment properties
        self.set_properties(experiment_data[0], experiment_data[1], experiment_data[2],
            experiment_data[3], experiment_data[4], experiment_data[5],
            experiment_data[6], None, experiment_data[7])

    def get_total_power(self):
        db = Database.DatabaseConnection.Instance()
        return db.get_total_power_for_experiment(self.experiment_id)

    def get_traffic_measures(self):
        # Get the traffic measure from the database
        db = Database.DatabaseConnection.Instance()
        traffic_measure = db.get_traffic_measures_for_experiment(self.experiment_id)

        # Save the traffic measures if they are not stored into the database
        if traffic_measure is None:
            self.__save_all_traffic_measures()
            traffic_measure = db.get_traffic_measures_for_experiment(self.experiment_id)

        # Return the result
        return traffic_measure

    def __save_all_traffic_measures(self):
        # Read the PCAP file
        pcap_path = Utils.get_server_pcap_path(self.experiment_id)
        packets = pyshark.FileCapture(pcap_path)

        nb_packets = 0
        last_time = 0
        for packet in packets:
            nb_packets = nb_packets+1
            last_time = datetime.fromtimestamp(float(packet.sniff_timestamp))

        first_time = datetime.fromtimestamp(float(packets[0].sniff_timestamp))

        # 5% margin
        time_margin = (last_time-first_time)*5/100
        first_time = first_time + time_margin
        last_time = last_time - time_margin

        time_interval_in_seconds = (last_time-first_time).total_seconds()
        sum_udp_packets = 0
        print("First time: {0}\nLast time: {1}\n".format(first_time, last_time))

        nb_udp_packets = 0
        for packet in packets:
            if (packet.layers[-1].layer_name == "data" and packet.layers[-2].layer_name == "udp"):
                # Check time
                packet_timestamp = datetime.fromtimestamp(float(packet.sniff_timestamp))

                if (packet_timestamp > first_time) and (packet_timestamp < last_time):
                    sum_udp_packets += int(packet.length)
                    nb_udp_packets += 1

        db = Database.DatabaseConnection.Instance()
        db.save_traffic(self.experiment_id, nb_udp_packets, time_interval_in_seconds, sum_udp_packets)
        packets.close()

    def compute_latency(self):
        latency_values = {}

        # Get the lim_packets_per_second
        latency_values['lim_packets_per_second'] = Utils.get_from_latency(self.experiment_id, 'lim_packets_per_second')

        # Compute the loss_rate
        latency_values['loss_rate'] = Utils.get_from_latency(self.experiment_id, 'loss_rate')

        # Compute the mean_latency
        latency_values['mean_latency'] = Utils.get_from_latency(self.experiment_id, 'mean_latency')
        if (latency_values['mean_latency'] == None):
            mean_latency = self.compute_mean_latency()
            latency_values['mean_latency'] = mean_latency
            Utils.store_mean_latency(self.experiment_id, mean_latency)

        return latency_values

    def compute_all_latency(self):
        # Check if the experiment has an associated experiment
        if (self.original_experiment_id > 0):
            experiment_id = self.original_experiment_id
        else:
            experiment_id = self.experiment_id

        # Initialization
        client_messages = []
        server_messages = []
        #topology_name = '{0}_{1}'.format(self.architecture, self.topology)
        topology_name = self.topology

        # Open the log file associated to the experiment
        # TODO: this is done for experiments with one client and one server
        with open(Utils.get_serial_aggregator_log_path(experiment_id), 'r') as infile:
            for line in infile:
                line1 = line.split(';')

                if (len(line1) > 2):
                    # Client
                    if (line1[1] in ["{0}-{1}".format(self.architecture, client_id_number) for client_id_number in Utils.get_clients_ids_numbers(topology_name)]):
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            client_messages.append(int(line2[1][:-1]))

                    # Server
                    elif (line1[1] == "{0}-{1}".format(self.architecture, Utils.get_server_id_number(topology_name))):
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            server_messages.append(int(line2[1][:-1]))

        total_sent_packets = len(client_messages)
        total_received_packets = len(server_messages)
        lim_packets_per_second = total_received_packets/(self.duration*60)
        loss_rate = (total_sent_packets-total_received_packets)*100/total_sent_packets

        # TODO
        Utils.store_lim_packets_per_second(experiment_id, lim_packets_per_second)
        Utils.store_packets_loss(experiment_id, loss_rate)

        print('[Experiment {0}] Total received messages: {1}'.format(experiment_id, total_received_packets))
        print('[Experiment {0}] Lim packets per second: {1}'.format(experiment_id, lim_packets_per_second))
        return lim_packets_per_second


    """
    Read the PCAP file presenting the traffic sniffed on the server side during
    the experiment in order to retrieve the size of the data packets.
    These values are then stored in an array, which is returned by the function.

    @type self: Experiment
    @param self: The considered experiment

    @rtype: int array
    @return: The array storing the computed packets lengths
    """
    def get_packets_lengths(self):
        # Read the PCAP file
        pcap_path = Utils.get_server_pcap_path(self.experiment_id)
        packets = pyshark.FileCapture(pcap_path)

        # Initialize the array of packets lengths values
        packets_lengths = []

        # Iterate over the packets
        for packet in packets:
            # Filter the packet to only consider the data packets we are
            # interested in
            if (packet.layers[-1].layer_name == "data" and packet.layers[-2].layer_name == "udp"):
                # Append the length of the current packet to the packets
                # lengths array
                packets_lengths.append(int(packet.length))

        packets.close()
        return packets_lengths

    """
    Parse the file presenting the logs of the client and server of the
    experiment in order to compute, for each message, its latency.
    These values are then stored in an array, which is returned by the function.

    @type self: Experiment
    @param self: The considered experiment

    @rtype: float array
    @return: The array storing the computed latencies
    """
    def get_latencies(self):
        # Initialization
        sent_messages = {}
        latencies = []

        # Get the path of the file storing the logs of the nodes
        serial_aggregator_log_path = Utils.get_serial_aggregator_log_path(self.associated_experiment_id)
        # Open the log
        with open(serial_aggregator_log_path, 'r') as infile:
            # For each line
            for line in infile:
                # Parse the line
                line1 = line.split(';')
                if (len(line1) > 2):
                    # If client: store in sent_messages
                    if (line1[1] in ["{0}-{1}".format(self.architecture, client_id_number) for client_id_number in Utils.get_clients_ids_numbers(self.topology)]):
                        line2 = line1[2].split(": ")
                        sent_messages[line2[1][:-1]] = float(line1[0])

                    # If server: compute latency
                    elif (line1[1] == "{0}-{1}".format(self.architecture, Utils.get_server_id_number(self.topology))):
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            if line2[1][:-1] in sent_messages.keys():
                                sent_timestamp = sent_messages[line2[1][:-1]]
                                received_timestamp = float(line1[0])
                                print('\n\nLatency of {0}: {1}'.format(line2[1][:-1], (datetime.utcfromtimestamp(received_timestamp)-datetime.utcfromtimestamp(sent_timestamp)).total_seconds()))
                                latencies.append((datetime.utcfromtimestamp(received_timestamp)-datetime.utcfromtimestamp(sent_timestamp)).total_seconds())

        return latencies


    def compute_mean_latency(self):
        # Initialization
        sent_messages = {}
        received_messages = {}
        mean_latency = 0
        nb_messages = 0
        # Open the log
        #serial_aggregator_log_path = Utils.get_serial_aggregator_log_path(self.experiment_id)
        serial_aggregator_log_path = Utils.get_serial_aggregator_log_path(self.associated_experiment_id)
        print(serial_aggregator_log_path)
        with open(serial_aggregator_log_path, 'r') as infile:
            #For each line:fail
            for line in infile:
                print(line)
                line1 = line.split(';')
                if (len(line1) > 2):
                    # If client: store in sent_messages
                    if (line1[1] in ["{0}-{1}".format(self.architecture, client_id_number) for client_id_number in Utils.get_clients_ids_numbers(self.topology)]):
                        line2 = line1[2].split(": ")
                        sent_messages[line2[1][:-1]] = float(line1[0])

                    # If server: compute latency
                    elif (line1[1] == "{0}-{1}".format(self.architecture, Utils.get_server_id_number(self.topology))):
                        line2 = line1[2].split(": ")
                        if (len(line2) > 1):
                            if line2[1][:-1] in sent_messages.keys():
                                sent_timestamp = sent_messages[line2[1][:-1]]
                                received_timestamp = float(line1[0])
                                print('\n\nLatency of {0}: {1}'.format(line2[1][:-1], (datetime.utcfromtimestamp(received_timestamp)-datetime.utcfromtimestamp(sent_timestamp)).total_seconds()))
                                mean_latency += (datetime.utcfromtimestamp(received_timestamp)-datetime.utcfromtimestamp(sent_timestamp)).total_seconds()
                                nb_messages += 1

        print('[Script - Latency analytics] Mean latency of experiment {0}: {1}'.format(self.experiment_id, mean_latency/nb_messages))
        return mean_latency/nb_messages


    def set_associated_experiment(self, associated_experiment_id):
        self.associated_experiment_id = associated_experiment_id




# ------------------------------------------------------------ #
"""
    # TODO
    def get_traffic_data(self, c, conn):
        # If no traffic data is store in the database, add it
        if not self.has_traffic_data():
            self.compute_traffic_data()

        # Get the traffic data from the database
        db = Database.DatabaseConnection.Instance()
        traffic_data = Database.get_traffic_data_from_db(c, conn, self.experiment_id)

        return traffic_data

    # TODO
    def has_traffic_data(self):
        return Database.has_traffic_data(self.experiment_id)


    def blbl(self):


        print('[Analytics] Number of packets: {0}'.format(nb_udp_packets))

        print('[Analytics] Time interval (in seconds): {0}'.format(time_interval_in_seconds))
        print('[Analytics] Sum of the packets\' length: {0}'.format(sum_udp_packets))

        print('\n[Analytics] Packets per second: {0}'.format(nb_udp_packets/time_interval_in_seconds))
        print('[Analytics] Bytes per second: {0}'.format(sum_udp_packets/time_interval_in_seconds))
        print('[Analytics] Mean of the lengths for the packets: {0}'.format(sum_udp_packets/nb_udp_packets))
"""
