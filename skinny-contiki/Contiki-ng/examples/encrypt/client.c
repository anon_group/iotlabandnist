/* C include */
#include <stdio.h>
#include <stdlib.h>

/* Contiki include */
#include "contiki.h"
#include "net/ipv6/uip-udp-packet.h"
#include "net/routing/rpl-classic/rpl.h"

/* skinny-ae security */
#include "lib/skinny/skinny-ae.h"

/* Debug msg */
#define DEBUG DEBUG_PRINT
#include "net/ipv6/uip-debug.h"

/* Timer */
#define DATA_INTERVAL_TRANS		(1 * CLOCK_SECOND)

/* UDP PORT connexion */
#define UDP_CLIENT_PORT	8765
#define UDP_SERVER_PORT	5678

static struct uip_udp_conn *unicast_connection;

/*---------------------------------------------------------------------------*/
PROCESS(client_process, "client process");
AUTOSTART_PROCESSES(&client_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *appdata;
  if(uip_newdata()) {
    appdata = (char *)uip_appdata;
    appdata[uip_datalen()] = 0;
    PRINTF("Data received from -");
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF(" on port %d from port %d with length %d: '%s'\n",
          UIP_HTONS(UIP_UDP_BUF->destport), UIP_HTONS(UIP_UDP_BUF->srcport), uip_datalen(), appdata);
  }
}
/*---------------------------------------------------------------------------*/
PROCESS(process_send_data, "Send data process");
PROCESS_THREAD(process_send_data, ev, data)
{
  //char buf[UIP_APPDATA_SIZE];

  //static int count = 0;

  uint8_t key[KEY_BYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
          };
  uint8_t nonce[NONCE_BYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,0x0f
          };
  uint8_t expectedPlaintext[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

  
  //size_t message_len;

  //uint8_t *message;

  PROCESS_BEGIN();

  //snprintf(buf, sizeof(buf), "Hello server %d", count++);


  //PRINTF("Sending unicast to ");
  //PRINT6ADDR(data);
  //PRINTF(" with length %d: '%s'\n", strlen(buf), buf);

  //message=(uint8_t*)buf;

  //message_len = strlen(buf);



  //uint8_t message[message_len];


  
  //int i_message;
 // for (i_message = 0; i_message < message_len; i_message++) {
 //     message[i_message] = (uint8_t) buf[i_message];
  //}

//typedef struct app_data
 // {
  //  uint8_t tag[TAG_BYTES];
   // uint8_t ciphertext[message_len];
  //} app_data ;

  //app_data d;

  //uint8_t ciphertext[message_len];//+KEY_BYTES];
  //uint8_t *ciphertext;
  //ciphertext= malloc((BLOCK_SIZE+TAG_BYTES)*sizeof(uint8_t));

   size_t outlen = 0;
   uint8_t *ciphertext;
   ciphertext= malloc((BLOCK_SIZE+TAG_BYTES)*sizeof(uint8_t)); 


  skinny_aead_encrypt(NULL, 0, // Associated data
                      expectedPlaintext, 16, // Message
                      key,
                      nonce,
                      ciphertext, &outlen); // ciphertext

    PRINTF("length cipher is:%d\n", outlen);
    //PRINTF("length cipher is:%d\n", message_len+TAG_BYTES);

for (int i=0; i<outlen;i++)
   PRINTF("ciphertext sent is:%d\n", ciphertext[i]);


/*
    uint8_t *obtained_plain=0;

    //obtained_plain= malloc((message_len+TAG_BYTES)*sizeof(uint8_t));
    obtained_plain= malloc((BLOCK_SIZE)*sizeof(uint8_t));
    //obtained_plain= malloc(BLOCK_SIZE*sizeof(uint8_t));

    size_t len_obtained_plain;//not-initialized, so send the &
     PRINTF("TOTO\n");


  skinny_aead_decrypt(NULL, 0,
                        obtained_plain, &len_obtained_plain,//sizeof(d->ciphertext),
                        key,
                        nonce,
                        ciphertext,outlen);


for(int i=0;i<BLOCK_SIZE;i++)
  PRINTF("deciphered obtained-plain: %d\n", obtained_plain[i]);*/

 /* uint8_t cleartext[message_len];
  size_t cleartext_len = message_len;*/

 /* int result = skinny_aead_decrypt(NULL, 0,
                                    (uint8_t*)cleartext, &cleartext_len,
                                    (uint8_t*)key,
                                    (uint8_t*)nonce,
                                    ciphertext, outlen);*/

  uip_udp_packet_sendto(unicast_connection, ciphertext,outlen,//, cleartext, cleartext_len, //ciphertext, outlen,
                        data, UIP_HTONS(UDP_SERVER_PORT));




    //printf("cipher text is %s\n", d.ciphertext);
  free(ciphertext);
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(client_process, ev, data)
{
  static struct etimer send_data_timer;
  rpl_dag_t *dag;

  PROCESS_BEGIN();

  /* new connection with server host */
  unicast_connection = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL);
  if(unicast_connection == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(unicast_connection, UIP_HTONS(UDP_CLIENT_PORT));

  etimer_set(&send_data_timer, DATA_INTERVAL_TRANS);

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }

    if(etimer_expired(&send_data_timer)) {
      etimer_reset(&send_data_timer);

      dag = rpl_get_any_dag();
      if(dag != NULL) {
        process_start(&process_send_data, &dag->dag_id);
      }
    }
  }

  PROCESS_END();
}
