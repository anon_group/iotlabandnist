/*
 *
 * University of Luxembourg
 * Laboratory of Algorithmics, Cryptology and Security (LACS)
 *
 * FELICS - Fair Evaluation of Lightweight Cryptographic Systems
 *
 * Copyright (C) 2015 University of Luxembourg
 *
 * Written in 2015 by Daniel Dinu <dumitru-daniel.dinu@uni.lu>
 *
 * This file is part of FELICS.
 *
 * FELICS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FELICS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "api.h"

//#include "crypto_aead.h"
#include "lib/skinny/constants.h"
#include "lib/skinny/tweakey.h"
#include "lib/skinny/skinny_reference.h"
#include "lib/skinny/skinny-ae.h"
//#include "felics/cipher.h"

/*
** Encryption call to the TBC primitive used in the mode
*/
void skinny_enc(const uint8_t* input, const uint8_t* tweakey, uint8_t* output) {

   // if(SKINNY_AEAD_MEMBER == 1)      
    enc(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 2) enc(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 3) enc(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 4) enc(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
   // else if(SKINNY_AEAD_MEMBER == 5) enc(input, tweakey, output, 4); /* SKINNY-128-256 (48 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 6) enc(input, tweakey, output, 4); /* SKINNY-128-256 (48 rounds) */

}

///////////////////////////////////////////////////
void enc(const uint8_t* input, const uint8_t* tweakey, uint8_t* output, const int ver) {

	uint8_t state[4][4];
	uint8_t keyCells[3][4][4];
	int i;

//#ifdef DEBUG
    //fic = fopen("SKINNY_TBC_detailed_TV.txt", "a");
//#endif

    /* Set state and keyCells */
	memset(keyCells, 0, 48);
	for(i = 0; i < 16; i++) {
        if (versions[ver][0]==64) {
            if(i&1) {
                state[i>>2][i&0x3] = input[i>>1]&0xF;

                keyCells[0][i>>2][i&0x3] = tweakey[i>>1]&0xF;
                if (versions[ver][1]>=128) keyCells[1][i>>2][i&0x3] = tweakey[(i+16)>>1]&0xF;
                if (versions[ver][1]>=192) keyCells[2][i>>2][i&0x3] = tweakey[(i+32)>>1]&0xF;
            } else {
                state[i>>2][i&0x3] = (input[i>>1]>>4)&0xF;

                keyCells[0][i>>2][i&0x3] = (tweakey[i>>1]>>4)&0xF;
                if (versions[ver][1]>=128) keyCells[1][i>>2][i&0x3] = (tweakey[(i+16)>>1]>>4)&0xF;
                if (versions[ver][1]>=192) keyCells[2][i>>2][i&0x3] = (tweakey[(i+32)>>1]>>4)&0xF;
            }
        } else if (versions[ver][0]==128) {
            state[i>>2][i&0x3] = input[i]&0xFF;

            keyCells[0][i>>2][i&0x3] = tweakey[i]&0xFF;
            if (versions[ver][1]>=256) keyCells[1][i>>2][i&0x3] = tweakey[i+16]&0xFF;
            if (versions[ver][1]>=384) keyCells[2][i>>2][i&0x3] = tweakey[i+32]&0xFF;
        }
    }

    //#ifdef DEBUG
      //  fprintf(fic,"ENC - initial state:                 ");display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
   // #endif

	for(i = 0; i < versions[ver][2]; i++) {
        if (versions[ver][0]==64)
            SubCell4(state);
        else
            SubCell8(state);
          //  #ifdef DEBUG
          //  fprintf(fic,"ENC - round %.2i - after SubCell:      ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
          //  #endif
        AddConstants(state, i);
           // #ifdef DEBUG
          //  fprintf(fic,"ENC - round %.2i - after AddConstants: ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
            //#endif
        AddKey(state, keyCells, ver);
          //  #ifdef DEBUG
           // fprintf(fic,"ENC - round %.2i - after AddKey:       ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
        ShiftRows(state);
            //#ifdef DEBUG
            //fprintf(fic,"ENC - round %.2i - after ShiftRows:    ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
            //#endif
        MixColumn(state);
           // #ifdef DEBUG
           // fprintf(fic,"ENC - round %.2i - after MixColumn:    ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
	}  //The last subtweakey should not be added

	//#ifdef DEBUG
      //  fprintf(fic,"ENC - final state:                   ");display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
    //#endif

    if (versions[ver][0]==64) {
        for(i = 0; i < 8; i++) {
            output[i] = ((state[(2*i)>>2][(2*i)&0x3] & 0xF) << 4) | (state[(2*i+1)>>2][(2*i+1)&0x3] & 0xF);
        }

    } else if (versions[ver][0]==128) {
        for(i = 0; i < 16; i++) {
            output[i] = state[i>>2][i&0x3] & 0xFF;
        }
    }

//#ifdef DEBUG
   // fclose(fic);
//#endif
}


/*******************************************************************************
** SKINNY-AEAD generic encryption and decryption functions
*******************************************************************************/

/*
** SKINNY-AEAD encryption function
*/
void skinny_aead_encrypt(const uint8_t *ass_data, size_t ass_data_len,
                         const uint8_t *message, size_t m_len,
                         const uint8_t *key,
                         const uint8_t *nonce,
                         uint8_t *ciphertext, size_t *c_len)
{

    uint64_t i;
    uint64_t j;
    uint64_t counter;
    uint8_t tweakey[TWEAKEY_STATE_SIZE/8];
    uint8_t Auth[16];
    uint8_t last_block[16];
    uint8_t Checksum[16];
    uint8_t Final[16];
    uint8_t zero_block[16];
    uint8_t Pad[16];
    uint8_t temp[16];

    /* Fill the tweakey state with zeros */
    memset(tweakey, 0, sizeof(tweakey));

    /* Set the key in the tweakey state */
    set_key_in_tweakey(tweakey, key);

    /* Set the nonce in the tweakey state */
    set_nonce_in_tweakey(tweakey, nonce);

    /* Associated data */
    memset(Auth, 0, 16);

    /* If there is associated data */
    if(ass_data_len) {

        /* Specify in the tweakey that we are processing full AD blocks */
        set_stage_in_tweakey(tweakey, CST_AD_FULL);

        /* For each full input blocks */
        i = 0;
        counter = 1;
        while (16*(i+1) <= ass_data_len) {

            /* Encrypt the current block */
            set_block_number_in_tweakey(tweakey, counter);
            skinny_enc(ass_data+16*i, tweakey, temp);

            /* Update Auth value */
            xor_values(Auth, temp);

            /* Go on with the next block */
            i++;
            counter = lfsr(counter);
        }

        /* Last block if incomplete */
        if ( ass_data_len > 16*i ) {

            /* Prepare the last padded block */
            memset(last_block, 0, 16);
            memcpy(last_block, ass_data+16*i, ass_data_len-16*i);
            last_block[ass_data_len-16*i] = 0x80;

            /* Encrypt the last block */
            set_stage_in_tweakey(tweakey, CST_AD_PARTIAL);
            set_block_number_in_tweakey(tweakey, counter);
            skinny_enc(last_block, tweakey, temp);

            /* Update the Auth value */
            xor_values(Auth, temp);
        }

    }/* if ass_data_len>0 */

    /*
    ** Now process the plaintext
    */

    /* Clear the checksum */
    memset(Checksum, 0, 16);

    /* Specify that we are now handling the plaintext */
    set_stage_in_tweakey(tweakey, CST_ENC_FULL);

    i = 0;
    counter = 1;
    while (16*(i+1) <= m_len) {

        /* Update the checksum with the current plaintext block */
        xor_values(Checksum, message+16*i);

        /* Update the tweakey state with the current block number */
        set_block_number_in_tweakey(tweakey, counter);

        /* Encrypt the current block and produce the ciphertext block */
        skinny_enc(message+16*i, tweakey, ciphertext+16*i);

        /* Update the counter */
        i++;
        counter = lfsr(counter);
    }

   /* Process incomplete block */
   if (m_len > 16*i) {

        /* Prepare the last padded block */
        memset(last_block, 0, 16);
        memcpy(last_block, message+16*i, m_len-16*i);
        last_block[m_len-16*i] = 0x80;

        /* Update the checksum */
        xor_values(Checksum, last_block);

        /* Create the zero block for encryption */
        memset(zero_block, 0, 16);

        /* Encrypt it */
        set_stage_in_tweakey(tweakey, CST_ENC_PARTIAL);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(zero_block, tweakey, Pad);

        /* Produce the partial ciphertext block */
        for (j=0; j<m_len-16*i; ++j) {
            ciphertext[16*i+j] = last_block[j] ^ Pad[j];
        }

        /* Encrypt the checksum */
        set_stage_in_tweakey(tweakey, CST_TAG_PARTIAL);
        counter = lfsr(counter);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(Checksum, tweakey, Final);

    } else {

        /* Encrypt the checksum */
        set_stage_in_tweakey(tweakey, CST_TAG_FULL);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(Checksum, tweakey, Final);

    }

    /* Append the authentication tag to the ciphertext */
    for (i=0; i<TAG_SIZE/8; i++) {
        ciphertext[m_len+i] = Final[i] ^ Auth[i];
    }

    /* The authentication tag is appended to the ciphertext */
    *c_len = m_len + TAG_SIZE/8;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
** Decryption call to the TBC primitive used in the mode
*/
/*
** Decryption call to the TBC primitive used in the mode
*/
void skinny_dec(const uint8_t* input, const uint8_t* tweakey, uint8_t* output) {

   // if(SKINNY_AEAD_MEMBER == 1)      
    dec(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 2) dec(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    // else if(SKINNY_AEAD_MEMBER == 3) dec(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 4) dec(input, tweakey, output, 5); /* SKINNY-128-384 (56 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 5) dec(input, tweakey, output, 4); /* SKINNY-128-256 (48 rounds) */
    //else if(SKINNY_AEAD_MEMBER == 6) dec(input, tweakey, output, 4); /* SKINNY-128-256 (48 rounds) */

}
void dec(const uint8_t* input, const uint8_t* tweakey, uint8_t* output, const int ver) {

	uint8_t state[4][4];
	uint8_t dummy[4][4]={{0}};
	uint8_t keyCells[3][4][4];
	int i;

//#ifdef DEBUG
  //  fic = fopen("SKINNY_TBC_detailed_TV.txt", "a");
//#endif

    /* Set state and keyCells */
    memset(keyCells, 0, 48);
	for(i = 0; i < 16; i++) {
        if (versions[ver][0]==64) {
            if(i&1) {
                state[i>>2][i&0x3] = input[i>>1]&0xF;

                keyCells[0][i>>2][i&0x3] = tweakey[i>>1]&0xF;
                if (versions[ver][1]>=128) keyCells[1][i>>2][i&0x3] = tweakey[(i+16)>>1]&0xF;
                if (versions[ver][1]>=192) keyCells[2][i>>2][i&0x3] = tweakey[(i+32)>>1]&0xF;

            } else {
                state[i>>2][i&0x3] = (input[i>>1]>>4)&0xF;

                keyCells[0][i>>2][i&0x3] = (tweakey[i>>1]>>4)&0xF;
                if (versions[ver][1]>=128) keyCells[1][i>>2][i&0x3] = (tweakey[(i+16)>>1]>>4)&0xF;
                if (versions[ver][1]>=192) keyCells[2][i>>2][i&0x3] = (tweakey[(i+32)>>1]>>4)&0xF;
            }

        } else if (versions[ver][0]==128) {
            state[i>>2][i&0x3] = input[i]&0xFF;

            keyCells[0][i>>2][i&0x3] = tweakey[i]&0xFF;
            if (versions[ver][1]>=256) keyCells[1][i>>2][i&0x3] = tweakey[i+16]&0xFF;
            if (versions[ver][1]>=384) keyCells[2][i>>2][i&0x3] = tweakey[i+32]&0xFF;
        }
    }

    for(i = versions[ver][2]-1; i >=0 ; i--) {
        AddKey(dummy, keyCells, ver);
    }

  //  #ifdef DEBUG
     //   fprintf(fic,"DEC - initial state:                     ");display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
   // #endif

	for(i = versions[ver][2]-1; i >=0 ; i--) {
        MixColumn_inv(state);
           // #ifdef DEBUG
           // fprintf(fic,"DEC - round %.2i - after MixColumn_inv:    ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
        ShiftRows_inv(state);
           // #ifdef DEBUG
            //fprintf(fic,"DEC - round %.2i - after ShiftRows_inv:    ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
        AddKey_inv(state, keyCells, ver);
           // #ifdef DEBUG
           // fprintf(fic,"DEC - round %.2i - after AddKey_inv:       ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
        AddConstants(state, i);
           // #ifdef DEBUG
           // fprintf(fic,"DEC - round %.2i - after AddConstants_inv: ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
        if (versions[ver][0]==64)
            SubCell4_inv(state);
        else
            SubCell8_inv(state);
           // #ifdef DEBUG
           // fprintf(fic,"DEC - round %.2i - after SubCell_inv:      ",i);display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
           // #endif
	}

	//#ifdef DEBUG
    //    fprintf(fic,"DEC - final state:                       ");display_cipher_state(state,keyCells,ver);fprintf(fic,"\n");
   // #endif

    if (versions[ver][0]==64) {
        for(i = 0; i < 8; i++) {
            output[i] = ((state[(2*i)>>2][(2*i)&0x3] & 0xF) << 4) | (state[(2*i+1)>>2][(2*i+1)&0x3] & 0xF);
        }
    } else if (versions[ver][0]==128) {
        for(i = 0; i < 16; i++) {
            output[i] = state[i>>2][i&0x3] & 0xFF;
        }
    }
//#ifdef DEBUG
  //  fclose(fic);
//#endif
}


/*******************************************************************************
** SKINNY-AEAD generic encryption and decryption functions
*******************************************************************************/
/*
** SKINNY-AEAD decryption function
*/
int skinny_aead_decrypt(const uint8_t *ass_data, size_t ass_data_len,
                       uint8_t *message, size_t *m_len,
                       const uint8_t *key,
                       const uint8_t *nonce,
                       const uint8_t *ciphertext, size_t c_len)
{

    uint64_t i;
    uint64_t j;
    uint64_t counter;
    uint8_t tweakey[TWEAKEY_STATE_SIZE/8];
    uint8_t Auth[16];
    uint8_t last_block[16];
    uint8_t Checksum[16];
    uint8_t Final[16];
    uint8_t zero_block[16];
    uint8_t Pad[16];
    uint8_t Tag[16];
    uint8_t temp[16];

    /* Get the tag from the last bytes of the ciphertext */
    memset(Tag, 0, 16);
    memcpy(Tag, ciphertext+c_len-TAG_SIZE/8, TAG_SIZE/8);

    /* Update c_len to the actual size of the ciphertext (i.e., without the tag) */
    c_len -= TAG_SIZE/8;

    /* Fill the tweakey state with zeros */
    memset(tweakey, 0, sizeof(tweakey));

    /* Set the key in the tweakey state */
    set_key_in_tweakey(tweakey, key);

    /* Set the nonce in the tweakey state */
    set_nonce_in_tweakey(tweakey, nonce);

    /* Associated data */
    memset(Auth, 0, 16);

    /* If there is associated data */
    if(ass_data_len) {

        /* Specify in the tweakey that we are processing full AD blocks */
        set_stage_in_tweakey(tweakey, CST_AD_FULL);

        /* For each full input blocks */
        i = 0;
        counter = 1;
        while (16*(i+1) <= ass_data_len) {

            /* Encrypt the current block */
            set_block_number_in_tweakey(tweakey, counter);
            skinny_enc(ass_data+16*i, tweakey, temp);

            /* Update Auth value */
            xor_values(Auth, temp);

            /* Go on with the next block */
            i++;
            counter = lfsr(counter);
        }

        /* Last block if incomplete */
        if ( ass_data_len > 16*i ) {

            /* Prepare the last padded block */
            memset(last_block, 0, 16);
            memcpy(last_block, ass_data+16*i, ass_data_len-16*i);
            last_block[ass_data_len-16*i] = 0x80;

            /* Encrypt the last block */
            set_stage_in_tweakey(tweakey, CST_AD_PARTIAL);
            set_block_number_in_tweakey(tweakey, counter);
            skinny_enc(last_block, tweakey, temp);

            /* Update the Auth value */
            xor_values(Auth, temp);
        }

    }/* if ass_data_len>0 */

    /*
    ** Now process the ciphertext
    */

    /* Clear the checksum */
    memset(Checksum, 0, 16);

    /* Specify that we are now handling the plaintext */
    set_stage_in_tweakey(tweakey, CST_ENC_FULL);

    i = 0;
    counter = 1;
    while (16*(i+1) <= c_len) {

        /* Update the tweakey state with the current block number */
        set_block_number_in_tweakey(tweakey, counter);

        /* Decrypt the current block and produce the plaintext block */
        skinny_dec(ciphertext+16*i, tweakey, message+16*i);

        /* Update the checksum with the current plaintext block */
        xor_values(Checksum, message+16*i);

        /* Update the counter */
        i++;
        counter = lfsr(counter);
    }

    /* Process last block */

    /* If the block is full, simply encrypts the checksum to get the candidate tag */
    if (c_len == 16*i) {

        /* Decrypt the checksum */
        set_stage_in_tweakey(tweakey, CST_TAG_FULL);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(Checksum, tweakey, Final);

        /* Derive the candidate authentication tag */
        xor_values(Final, Auth);

        /* If the tags does not match, return error -1 */
        if( 0 != memcmp_const(Final, Tag, TAG_SIZE/8) ) {
            memset(message, 0, c_len);
            return -1;
        }

    } else { /* If the last block is a partial block */

        /* Prepare the full-zero block */
        memset(zero_block, 0, 16);

        /* Encrypt the zero block */
        set_stage_in_tweakey(tweakey, CST_ENC_PARTIAL);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(zero_block, tweakey, Pad);

        /* XOR the partial ciphertext */
        memset(last_block, 0, 16);
        memcpy(last_block, ciphertext+16*i, c_len-16*i);

        /* Partial XOR to get the plaintext block */
        for (j=0; j<c_len-16*i; ++j) {
            last_block[j] ^= Pad[j];
            message[16*i+j] = last_block[j];
        }

        /* Update the checksum */
        last_block[c_len-16*i] = 0x80;
        xor_values(Checksum, last_block);

        /* Compute the candidate authentication tag */
        set_stage_in_tweakey(tweakey, CST_TAG_PARTIAL);
        counter = lfsr(counter);
        set_block_number_in_tweakey(tweakey, counter);
        skinny_enc(Checksum, tweakey, Final);

        xor_values(Final, Auth);

        /* If the tags does not match, return error -1 */
        if( 0 != memcmp_const(Final, Tag, TAG_SIZE/8) ) {
            memset(message, 0, c_len);
            return -1;
        }
    }

    /* Returns the plaintext */
    *m_len = c_len;
    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
int crypto_aead_encrypt(
	uint8_t *c, size_t *clen,
	const uint8_t *m, size_t mlen,
	const uint8_t *ad, size_t adlen,
    const uint8_t *npub,
	const uint8_t *k
)
{
	/* Add here the cipher encryption implementation */
  
   size_t outlen = 0;
    skinny_aead_encrypt(ad, adlen, m, mlen, k, npub, c, &outlen);

    *clen = outlen;
    //*clen = mlen + TAG_SIZE/8;
    //(void)nsec;
    return 0;
}



int crypto_aead_decrypt(
	uint8_t *m, size_t *mlen,
	const uint8_t *c, size_t clen,
	const uint8_t *ad, size_t adlen,
	const uint8_t *npub,
	const uint8_t *k
)

{
    int result = skinny_aead_decrypt(ad, adlen, m, (size_t *)mlen, k, npub, c, clen);
   // (void)nsec;
    return result;
}





