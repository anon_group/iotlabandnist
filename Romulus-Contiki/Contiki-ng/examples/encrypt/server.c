/* C include */
#include <stdio.h>
#include <stdlib.h>
/* Contiki include */
#include "contiki.h"
#include "net/ipv6/uip.h"
#include "net/routing/rpl-classic/rpl.h"

/* Romulus security */
#include "lib/Romulusm/api.h"

/* Debug msg */
#define DEBUG DEBUG_PRINT
#include "net/ipv6/uip-debug.h"

/* UDP PORT connexion */
#define UDP_CLIENT_PORT	8765
#define UDP_SERVER_PORT	5678

static struct uip_udp_conn *server_conn;

/*---------------------------------------------------------------------------*/
PROCESS(server_process, "server process");
AUTOSTART_PROCESSES(&server_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{


  uint8_t key[CRYPTO_KEYBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
          };
  uint8_t nonce[CRYPTO_NPUBBYTES] = {
              0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
              0x08, 0x09, 0x0a, 0x0b
          };

 // uint8_t expectedAssociated[MAXTEST_BYTES_AD]= {
				//0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				//0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};


  if(uip_newdata()) {

    printf("New data\n");

    typedef struct app_data
    {
      uint8_t ciphertext[MAXTEST_BYTES_M+CRYPTO_ABYTES];//+TAG_BYTES];
     } app_data ;

    app_data *d = (app_data *)uip_appdata;

for (int i=0; i<MAXTEST_BYTES_M+CRYPTO_ABYTES;i++)
   PRINTF("ciphertext received is:%d\n", d->ciphertext[i]);

     //PRINTF("ciphertext is:%s\n", d->ciphertext);
    //uint8_t cleartext[sizeof(d->ciphertext)];

/////////////////added/////////////////////////

uint8_t *obtained_plain=0;

    //obtained_plain= malloc((message_len+TAG_BYTES)*sizeof(uint8_t));
    obtained_plain= malloc((MAXTEST_BYTES_M)*sizeof(uint8_t));
    //obtained_plain= malloc(BLOCK_SIZE*sizeof(uint8_t));

    unsigned long long len_obtained_plain;//not-initialized, so send the &
     PRINTF("TOTO\n");



crypto_aead_decrypt(obtained_plain,&len_obtained_plain,CRYPTO_NSECBYTES,
d->ciphertext,MAXTEST_BYTES_M+CRYPTO_ABYTES,
NULL,0,nonce,key);


for(int i=0;i<MAXTEST_BYTES_M;i++)
  PRINTF("deciphered obtained-plain: %d\n", obtained_plain[i]);

free(obtained_plain);

    //PRINTF("Data received from ");
    //PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    // PRINTF(" with length %d: '%s'\n", uip_datalen(),cleartext);*/
  }
}
/*---------------------------------------------------------------------------*/
static void
create_rpl_dag(uip_ipaddr_t *ipaddr)
{
  struct uip_ds6_addr *root_if;

  root_if = uip_ds6_addr_lookup(ipaddr);
  if(root_if != NULL) {
    rpl_dag_t *dag;
    uip_ipaddr_t prefix;

    rpl_set_root(RPL_DEFAULT_INSTANCE, ipaddr);
    dag = rpl_get_any_dag();
    uip_ip6addr(&prefix, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
    rpl_set_prefix(dag, &prefix, 64);
    PRINTF("created a new RPL dag\n");
  } else {
    PRINTA("failed to create a new RPL DAG\n");
  }
}
/*---------------------------------------------------------------------------*/
static uip_ipaddr_t *
set_global_address(void)
{
  static uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, UIP_DS6_DEFAULT_PREFIX, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

  return &ipaddr;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(server_process, ev, data)
{
  uip_ipaddr_t *ipaddr;

  PROCESS_BEGIN();

  ipaddr = set_global_address();

  create_rpl_dag(ipaddr);

  /* new connection with server host */
  server_conn = udp_new(NULL, UIP_HTONS(UDP_CLIENT_PORT), NULL);
  if(server_conn == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(server_conn, UIP_HTONS(UDP_SERVER_PORT));

  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
