/*
 * TweGIFT-64_LOCUS-AEAD
 * 
 * 
 * TweGIFT-64_LOCUS-AEAD ia a nonce-based AEAD based on the LOCUS-AEAD
 * mode of operation and TweGIFT-64 tweakable block cipher.
 * 
 * Test Vector (in little endian format):
 * Key	: 0f 0e 0d 0c 0b 0a 09 08 07 06 05 04 03 02 01 00
 * PT 	:
 * AD	: 
 * CT	: e8 8d f3 3f b8 eb f3 37
 * 
 */

#include "crypto_aead.h"
#include "api.h"
#include "locus.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

////////////////////////// TweGift-64///////////////////////////////////
/*
 * TweGift-64
 * 
 * TweGift-64 is a minor extension of GIFT-64-128 block cipher (by Banik
 * et al. [1,2]) in that it accepts a small tweak of size 1 nibble. We
 * have used the reference source code for GIFT-64-128 (available at
 * [3]) by the GIFT team as the base for TweGift-64.
 * 
 * 1. https://link.springer.com/chapter/10.1007/978-3-319-66787-4_16
 * 2. https://eprint.iacr.org/2017/622.pdf
 * 3. https://sites.google.com/view/giftcipher/ 
 * 
 * The major technical change from our side is the addition of tweak
 * expansion and absorption functionalities given in "exp_tweak" and
 * "add_round_tweak". Apart from this we have also done some minor
 * code cleaning and restructuring in order to make the code consistent
 * with our notations and conventions.
 * 
 */

//#include "locus.h"

/* 
 * No. of nibbles in the expanded tweak
 * 
 */ 
#define CRYPTO_EXPTWEAKNIBBLES (4)

/* 
 * No. of rounds between tweak injection
 * i.e. tweak is added at intervals of
 * "CRYPTO_TWEAKING_PERIOD" starting at
 * round "CRYPTO_TWEAKING_PERIOD".
 */ 
#define CRYPTO_TWEAKING_PERIOD (4)

/* 
 * sbox and its inverse
 */ 
const u8 _gift_sbox[16] = { 1, 10, 4, 12, 6, 15, 3, 9, 2, 13, 11, 7, 5, 0, 8, 14 };
const u8 _gift_sbox_inv[16] = { 13, 0, 8, 6, 2, 12, 4, 11, 14, 7, 1, 10, 3, 9, 15, 5 };

/*
 * bit permutation and its inverse
 */ 
const u8 _gift_perm[] = {
  0, 17, 34, 51, 48,  1, 18, 35, 32, 49,  2, 19, 16, 33, 50,  3,
  4, 21, 38, 55, 52,  5, 22, 39, 36, 53,  6, 23, 20, 37, 54,  7,
  8, 25, 42, 59, 56,  9, 26, 43, 40, 57, 10, 27, 24, 41, 58, 11,
 12, 29, 46, 63, 60, 13, 30, 47, 44, 61, 14, 31, 28, 45, 62, 15
};
const u8 _gift_perm_inv[] = {
  0,  5, 10, 15, 16, 21, 26, 31, 32, 37, 42, 47, 48, 53, 58, 63,
 12,  1,  6, 11, 28, 17, 22, 27, 44, 33, 38, 43, 60, 49, 54, 59,
  8, 13,  2,  7, 24, 29, 18, 23, 40, 45, 34, 39, 56, 61, 50, 55,
  4,  9, 14,  3, 20, 25, 30, 19, 36, 41, 46, 35, 52, 57, 62, 51
};

/*
 * round constants
 */ 
const u8 _gift_round_constants[62] = {
    0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
    0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
    0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
    0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A,
    0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13,
    0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a, 0x15, 0x2a, 0x14, 0x28,
    0x10, 0x20
};

/**********************************************************************
 * 
 * @name	:	bytes_to_nibbles
 * 
 * @note	:	Convert byte oriented "src" to nibble oriented "dest".
 * 
 **********************************************************************/
void bytes_to_nibbles(u8 *dest, const u8 *src, u8 src_len)
{
    for(u8 i=0; i<src_len; i++)
    {
        dest[2*i] = src[i]&0xF;
        dest[2*i+1] = (src[i]&0xF0)>>4;
    }
}

/**********************************************************************
 * 
 * @name	:	nibbles_to_bytes
 * 
 * @note	:	Convert nibble oriented "src" to byte oriented "dest".
 * 
 **********************************************************************/
void nibbles_to_bytes(u8 *dest, const u8 *src, u8 dest_len)
{
    for(u8 i=0; i<dest_len; i++)
	{
		dest[i] = src[2*i+1]<<4 | src[2*i];
	}
}

/**********************************************************************
 * 
 * @name	:	nibbles_to_bits
 * 
 * @note	:	Convert nibble oriented "src" to bit oriented "dest".
 * 
 **********************************************************************/
void nibbles_to_bits(u8 *dest, const u8 *src, u8 src_len)
{
	for(u8 i=0; i<src_len; i++)
	{
		for(u8 j=0; j<4; j++)
		{
			dest[4*i+j] = (src[i] >> j) & 0x1;
		}
	}
}

/**********************************************************************
 * 
 * @name	:	bits_to_nibbles
 * 
 * @note	:	Convert bit oriented "src" to nibble oriented "dest".
 * 
 **********************************************************************/
void bits_to_nibbles(u8 *dest, const u8 *src, u8 dest_len)
{
    for(u8 i=0; i<dest_len; i++)
	{
		dest[i]=0;
		for(u8 j=0; j<4; j++)
		{
			 dest[i] ^= src[4*i+j] << j;
		}
	}
}

/**********************************************************************
 * 
 * @name	:	expand_tweak
 * 
 * @note	:	Expand the 4-bit tweak input in "twk" into a
 * 				CRYPTO_EXPTWEAKNIBBLES-nibble expanded tweak in
 * 				"exp_twk".
 * 
 **********************************************************************/
void expand_tweak(u8 *exp_twk, const u8 *twk)
{
	u8 parity = 0x00;
	
	// expand tweak nibble to a byte by copying the
	// given nibble to another nibble.
	memset(&exp_twk[0], twk[0], 2);
	
	// compute parity
	for(u8 i=0; i<4; i++)
	{
		parity ^= ((twk[0]>>i) & 0x1);
	}
	
	if(parity)
	{
		// XOR parity to the second nibble.
		exp_twk[1] ^= 0x0f;
	}
	
	// copy the encoded tweak to the rest of the nibbles to get the
	// expanded tweak.
	for(u8 i=2; i<CRYPTO_EXPTWEAKNIBBLES; i++)
	{
		exp_twk[i] = exp_twk[i%2];
	}
}

/**********************************************************************
 * 
 * @name	:	generate_round_keys
 * 
 * @note	:	Generate and store the round key nibbles using the
 * 				master key.
 * 
 **********************************************************************/
void generate_round_keys(u8 (*round_key_nibbles)[32], const u8 *key_bytes)
{
	u8 key_nibbles[32];
	//memcpy(&key_nibbles_[0], &key_nibbles[0], 32);
	
	// convert master key from byte-oriented
	// to nibble-oriented
	bytes_to_nibbles(key_nibbles, key_bytes, 16);
	
	//copy the first round key
	for (u8 i=0; i<32; i++)
	{
		round_key_nibbles[0][i] = key_nibbles[i];
	}
	
	// update round key and store the rest of the round keys
	u8 temp[32];
	for (u8 r=1; r<CRYPTO_BC_NUM_ROUNDS; r++)
	{
		//key update
		//entire key>>32
		for(u8 i=0; i<32; i++)
		{
			temp[i] = key_nibbles[(i+8)%32];
		}
		for(u8 i=0; i<24; i++)
		{
			key_nibbles[i] = temp[i];
		}
		//k0>>12
		key_nibbles[24] = temp[27];
		key_nibbles[25] = temp[24];
		key_nibbles[26] = temp[25];
		key_nibbles[27] = temp[26];
		//k1>>2
		key_nibbles[28] = ((temp[28]&0xc)>>2) ^ ((temp[29]&0x3)<<2);
		key_nibbles[29] = ((temp[29]&0xc)>>2) ^ ((temp[30]&0x3)<<2);
		key_nibbles[30] = ((temp[30]&0xc)>>2) ^ ((temp[31]&0x3)<<2);
		key_nibbles[31] = ((temp[31]&0xc)>>2) ^ ((temp[28]&0x3)<<2);
		
		//copy the key state
		for (u8 i=0; i<32; i++)
		{
			round_key_nibbles[r][i] = key_nibbles[i];
		}
	}
}

/**********************************************************************
 * 
 * @name	:	sub_cells
 * 
 * @note	:	SubCells operation.
 * 
 **********************************************************************/
void sub_cells(u8 *state_nibbles)
{
	for(u8 i=0; i<16; i++)
	{
		state_nibbles[i] = _gift_sbox[state_nibbles[i]];
	}
}

/**********************************************************************
 * 
 * @name	:	sub_cells_inv
 * 
 * @note	:	Inverse SubCells operation.
 * 
 **********************************************************************/
void sub_cells_inv(u8 *state_nibbles)
{
	for(u8 i=0; i<16; i++)
	{
		state_nibbles[i] = _gift_sbox_inv[state_nibbles[i]];
	}
}

/**********************************************************************
 * 
 * @name	:	perm_bits
 * 
 * @note	:	PermBits operation.
 * 
 **********************************************************************/
void perm_bits(u8 *state_bits)
{	
	//nibbles to bits
	u8 bits[64];
	
	//permute the bits
	for(u8 i=0; i<64; i++)
	{
		bits[_gift_perm[i]] = state_bits[i];
	}
	memcpy(&state_bits[0], &bits[0], 64);
}

/**********************************************************************
 * 
 * @name	:	perm_bits_inv
 * 
 * @note	:	Inverse PermBits operation.
 * 
 **********************************************************************/
void perm_bits_inv(u8 *state_bits)
{	
	//nibbles to bits
	u8 bits[64];
	
	//permute the bits
	for(u8 i=0; i<64; i++)
	{
		bits[_gift_perm_inv[i]] = state_bits[i];
	}
	memcpy(&state_bits[0], &bits[0], 64);
}

/**********************************************************************
 * 
 * @name	:	add_round_key
 * 
 * @note	:	Add round key operation.
 * 
 **********************************************************************/
void add_round_key(u8 *state_bits, const u8 *round_key_nibbles)
{		
	//round key nibbles to bits
	u8 key_bits[128];
	nibbles_to_bits(&key_bits[0], &round_key_nibbles[0], 32);

	//add round key
	for (u8 i=0; i<16; i++)
	{
		state_bits[4*i] ^= key_bits[i];
		state_bits[4*i+1] ^= key_bits[i+16];
	}
}

/**********************************************************************
 * 
 * @name	:	add_round_tweak
 * 
 * @note	:	Add round tweak operation.
 * 
 **********************************************************************/
void add_round_tweak(u8 *state_bits, const u8 *exp_twk_nibbles)
{
	// tweak_nibbles to bits
	u8 twk_bits[4*CRYPTO_EXPTWEAKNIBBLES];
	nibbles_to_bits(&twk_bits[0], &exp_twk_nibbles[0], CRYPTO_EXPTWEAKNIBBLES);

	// add round tweak
	for (u8 i=0; i<16; i++)
	{
		state_bits[4*i+2] ^= twk_bits[i];
	}
}

/**********************************************************************
 * 
 * @name	:	add_round_constant
 * 
 * @note	:	Add round constant operation.
 * 
 **********************************************************************/
void add_round_constants(u8 *state_bits, u8 round)
{
	//add constant
	state_bits[3] ^= _gift_round_constants[round] & 0x1;
	state_bits[7] ^= (_gift_round_constants[round]>>1) & 0x1;
	state_bits[11] ^= (_gift_round_constants[round]>>2) & 0x1;
	state_bits[15] ^= (_gift_round_constants[round]>>3) & 0x1;
	state_bits[19] ^= (_gift_round_constants[round]>>4) & 0x1;
	state_bits[23] ^= (_gift_round_constants[round]>>5) & 0x1;
	state_bits[63] ^= 1;
}

/**********************************************************************
 * 
 * @name	:	twegift_enc
 * 
 * @note	:	TweGift-64 encryption function.
 * 
 **********************************************************************/
void twegift_enc(u8 *ct, const u8 *key, const u8 *twk, const u8 *pt)
{
    u8 in[16];
    
	// convert input plaintext from
	// byte-oriented to nibble-oriented
	bytes_to_nibbles(&in[0], &pt[0], 8);
    
    // generate round keys
    u8 round_keys[CRYPTO_BC_NUM_ROUNDS][32];
	generate_round_keys(&round_keys[0], key);

    u8 exp_twk[CRYPTO_EXPTWEAKNIBBLES] = { 0 };
    
    // expand tweak input if not equal to zero
    if(twk[0] != 0)
	{
		expand_tweak(&exp_twk[0], &twk[0]);
	}
	
	u8 in_bits[64];
	
	for(u8 r=0; r < CRYPTO_BC_NUM_ROUNDS; r++)
	{
		// subcells operation
		sub_cells(&in[0]);

		// permbits operation
		nibbles_to_bits(&in_bits[0], &in[0], 16);
		perm_bits(&in_bits[0]);
		
		// addroundkey operation
		add_round_key(&in_bits[0], round_keys[r]);
		
		// addroundtweak operation after every CRYPTO_TWEAKING_PERIOD
		// only when tweak is non-zero, as zero tweak is same as base
		// block cipher
		if(r != CRYPTO_BC_NUM_ROUNDS-1 && (r+1)%CRYPTO_TWEAKING_PERIOD == 0)
		{
			add_round_tweak(&in_bits[0], &exp_twk[0]);
		}

		// addroundconstant operation
		add_round_constants(&in_bits[0], r);
		
		// input bits to nibbles
		bits_to_nibbles(&in[0], &in_bits[0], 16);
	}
	
	//convert ciphertext from nibble-oriented to byte-oriented
	nibbles_to_bytes(ct, in, 8);
}

/**********************************************************************
 * 
 * @name	:	twegift_dec
 * 
 * @note	:	TweGift-64 decryption function.
 * 
 **********************************************************************/	
void twegift_dec(u8 *pt, const u8 *key, const u8 *twk, const u8 *ct)
{
    u8 in[16];
    
	// convert input ciphertext from
	// byte-oriented to nibble-oriented
    bytes_to_nibbles(&in[0], &ct[0], 8);
    
    // generate round keys
    u8 round_keys[CRYPTO_BC_NUM_ROUNDS][32];
	generate_round_keys(&round_keys[0], key);

    u8 exp_twk[CRYPTO_EXPTWEAKNIBBLES] = { 0 };
    
    // expand tweak input if not equal to zero
    if(twk[0] != 0)
	{
		expand_tweak(&exp_twk[0], &twk[0]);
	}
	
	u8 in_bits[64];
	
	for (u8 r,k = 0; k < CRYPTO_BC_NUM_ROUNDS; k++)
	{
		r=CRYPTO_BC_NUM_ROUNDS-1-k;
		
		//addroundkey operation	
		nibbles_to_bits(&in_bits[0], &in[0], 16);
		add_round_key(&in_bits[0], round_keys[r]);
		
		// addroundtweak operation after every CRYPTO_TWEAKING_PERIOD
		// only when tweak is non-zero, as zero tweak is same as base
		// block cipher
		if(r != CRYPTO_BC_NUM_ROUNDS-1 && (r+1)%CRYPTO_TWEAKING_PERIOD == 0)
		{
			add_round_tweak(&in_bits[0], &exp_twk[0]);
		}
		
		//addroundconstant operation
		add_round_constants(&in_bits[0], r);
		
		//inverse permbits operation
		perm_bits_inv(&in_bits[0]);
		
		//inverse subcells
		bits_to_nibbles(&in[0], &in_bits[0], 16);
		sub_cells_inv(&in[0]);
	}
	
	//convert plaintext from nibble-oriented to byte-oriented
	nibbles_to_bytes(pt, in, 8);
}

////////////////////////////////////////////////////////////////////////////


/**********************************************************************
 * 
 * @name	:	xor_bytes
 * 
 * @note	:	XORs "num" many bytes of "src" to "dest".
 * 
 **********************************************************************/		
void xor_bytes(u8 *dest, const u8 *src, u8 num)
{
	for(u8 i=0; i < num; i++)
	{
		dest[i] ^= src[i];
	}
}

/**********************************************************************
 * 
 * @name	:	mult_by_alpha
 * 
 * @note	:	Multiplies given field element in "src" with \alpha,
 * 				the primitive element corresponding to the primitive
 * 				polynomial p(x) as defined in PRIM_POLY_MOD_128, and
 * 				stores the result in "dest".
 * 
 **********************************************************************/	
void mult_by_alpha(u8 *dest, u8 *src)
{
	u8 mask = 0x00;
	if(src[CRYPTO_KEYBYTES-1] & 0x80){
		mask = PRIM_POLY_MOD_128;
	}
	for(u8 i=CRYPTO_KEYBYTES-1; i>0; i--){
		dest[i] = src[i]<<1 | src[i-1]>>7;
	}
	dest[0] = src[0]<<1;
	dest[0] ^= mask;
}

/**********************************************************************
 * 
 * @name	:	memcpy_and_zero_one_pad
 * 
 * @note	:	Copies src bytes to dest and pads with 10* to create
 * 				CRYPTO_BLOCKBYTES-oriented data.
 * 
 **********************************************************************/
void memcpy_and_zero_one_pad(u8* dest, const u8 *src, u8 len)
{
	memset(dest, 0, CRYPTO_BLOCKBYTES);
	memcpy(dest, src, len);
	dest[len] ^= 0x01;
}

/**********************************************************************
 * 
 * @name	:	init
 * 
 * @note	:	Derives nonce-dependent key and mask.
 * 
 **********************************************************************/
void init(u8 *nonced_key, u8 *nonced_mask, const u8 *key, const u8 *nonce)
{
	u8 twk;
	
	u8 zero[CRYPTO_BLOCKBYTES] = { 0 };
	
	u8 enc_zero[CRYPTO_BLOCKBYTES];
	
	// set control bits to 000.
	twk = 0x00;
	
	// encrypt zero with the master key.
	twegift_enc(enc_zero, key, &twk, zero);
	
	// compute K_N = K + N
	memcpy(nonced_key, key, CRYPTO_KEYBYTES);
	xor_bytes(nonced_key, nonce, CRYPTO_NPUBBYTES);
	
	// set control bits to 001.
	twk = 0x01;
	
	//compute \Delta_N = E^1_{K_N}(E^0_K(0))
	twegift_enc(nonced_mask, nonced_key, &twk, enc_zero);
}

/**********************************************************************
 * 
 * @name	:	proc_ad
 * 
 * @note	:	Processes associated data to generate intermediate
 * 				checksum.
 * 
 **********************************************************************/
void proc_ad(u8 *nonced_key, u8 *vxor, u8 *nonced_mask, const u8 *ad, u64 a, u64 adlen)
{
	u8 twk;
	
	u8 u[CRYPTO_BLOCKBYTES];
	
	u8 v[CRYPTO_BLOCKBYTES];
	
	// set control bits to 010
	twk = 0x02;
	
	// L_0 = K_N \odot \alpha
	mult_by_alpha(nonced_key, nonced_key);
	
	for(u64 i=0; i < a-1; i++)
	{
		// compute U_i = A_i + \Delta_N 
		memcpy(&u[0],&ad[i*CRYPTO_BLOCKBYTES],CRYPTO_BLOCKBYTES);
		xor_bytes(u, nonced_mask, CRYPTO_BLOCKBYTES);
		
		// compute V_i = E^2_{L_i}(U_i)
		twegift_enc(v, nonced_key, &twk, u);
		
		// V_\xor = V_\xor + V_i
		xor_bytes(vxor, v, CRYPTO_BLOCKBYTES);
		
		// L_{i+1} = L_i \odot \alpha
		mult_by_alpha(nonced_key, nonced_key);
	}
	if(adlen%CRYPTO_BLOCKBYTES != 0)
	{
		//partial block processing
		
		// set control bits to 011
		twk = 0x03;
		
		// compute U_{a-1} = 0^*1||A_{a-1} + \Delta_N
		memcpy_and_zero_one_pad(&u[0], &ad[(a-1)*CRYPTO_BLOCKBYTES], PARTIAL_BLOCK_LEN(a,adlen));
		xor_bytes(u, nonced_mask, CRYPTO_BLOCKBYTES);
	}
	else
	{
		// full block processing
		
		// compute U_{a-1} = A_{a-1} + \Delta_N
		memcpy(&u[0], &ad[(a-1)*CRYPTO_BLOCKBYTES], CRYPTO_BLOCKBYTES);
		xor_bytes(u, nonced_mask, CRYPTO_BLOCKBYTES);
	}
	
	// compute V_{a-1} = E^2/3_{L_{a-1}}(U_{a-1})
	twegift_enc(v, nonced_key, &twk, u);
	
	// V_\xor = V_\xor + V_i
	xor_bytes(vxor, v, CRYPTO_BLOCKBYTES);
}

/**********************************************************************
 * 
 * @name	:	proc_pt
 * 
 * @note	:	Generates ciphertext by encrypting plaintext.
 * 
 **********************************************************************/
void proc_pt(u8 *nonced_key, u8 *wxor, u8 *ct, u64 *ctlen, u8 *nonced_mask, const u8 *pt, u64 m, u64 ptlen)
{
	u8 twk;
	
	u8 x[CRYPTO_BLOCKBYTES];
	
	u8 w[CRYPTO_BLOCKBYTES];
	
	u8 y[CRYPTO_BLOCKBYTES];
	
	*ctlen = 0;
	
	// set control bits to 100
	twk = 0x04;
	
	// L_a = K_N \odot \alpha
	mult_by_alpha(nonced_key, nonced_key);
	
	for(u64 i=0; i < m-1; i++)
	{
		// compute X_{i} = M_i + \Delta_N
		memcpy(&x[0], &pt[i*CRYPTO_BLOCKBYTES], CRYPTO_BLOCKBYTES);
		xor_bytes(x, nonced_mask, CRYPTO_BLOCKBYTES);
		
		// compute W_{i} = E^4_{L_{a+i}}(X_{i})
		twegift_enc(w, nonced_key, &twk, x);
		
		// W_\xor = W_\xor + W_{i}
		xor_bytes(wxor, w, CRYPTO_BLOCKBYTES);
		
		// compute Y_{i} = E^4_{L_{a+i}}(W_{i})
		twegift_enc(y, nonced_key, &twk, w);
		
		// compute C_{i} = Y_{i} + \Delta_N
		xor_bytes(y, nonced_mask, CRYPTO_BLOCKBYTES);
		memcpy(&ct[i*CRYPTO_BLOCKBYTES], &y[0], CRYPTO_BLOCKBYTES);
		*ctlen += CRYPTO_BLOCKBYTES;
		
		// L_{a+i+1} = L_{a+i} \odot \alpha
		mult_by_alpha(nonced_key, nonced_key);
	}
	// set control bits to 101
	twk = 0x05;
	
	// compute X_{m-1} = \Delta_N + <|M|-(m-1)n>_n
	memcpy(x, nonced_mask, CRYPTO_BLOCKBYTES);
	x[0] ^= PARTIAL_BLOCK_LEN(m, ptlen);
	
	// compute W_{m-1} = E^5_{L_{a+m-1}}(X_{m-1})
	twegift_enc(w, nonced_key, &twk, x);
	
	// compute Y_{m-1} = E^5_{L_{a+m-1}}(W_{m-1})
	twegift_enc(y, nonced_key, &twk, w);
	
	// compute C_{m-1} = chop(Y_{m-1} + \Delta_N) + M_{m-1}
	xor_bytes(y, nonced_mask, CRYPTO_BLOCKBYTES);
	memcpy(&ct[(m-1)*CRYPTO_BLOCKBYTES], &pt[(m-1)*CRYPTO_BLOCKBYTES], PARTIAL_BLOCK_LEN(m, ptlen));
	xor_bytes(&ct[(m-1)*CRYPTO_BLOCKBYTES], y, PARTIAL_BLOCK_LEN(m, ptlen));
	*ctlen += PARTIAL_BLOCK_LEN(m, ptlen);
	
	// W_\xor = W_\xor + W_{m-1} + M_{m-1}
	xor_bytes(wxor, w, CRYPTO_BLOCKBYTES);
	xor_bytes(wxor, &pt[(m-1)*CRYPTO_BLOCKBYTES], PARTIAL_BLOCK_LEN(m, ptlen));
}

/**********************************************************************
 * 
 * @name	:	proc_ct
 * 
 * @note	:	Generates plaintext by decrypting ciphertext.
 * 
 **********************************************************************/
void proc_ct(u8 *nonced_key, u8 *wxor, u8 *pt, u64 *ptlen, u8 *nonced_mask, const u8 *ct, u64 m, u64 ctlen)
{
	u8 twk;
	
	u8 x[CRYPTO_BLOCKBYTES];
	
	u8 w[CRYPTO_BLOCKBYTES];
	
	u8 y[CRYPTO_BLOCKBYTES];
	
	*ptlen = 0;
	
	// set control bits to 100
	twk = 0x04;
	
	// L_a = K_N \odot \alpha
	mult_by_alpha(nonced_key, nonced_key);
	
	for(u64 i=0; i < m-1; i++)
	{
		// compute Y_{i} = C_i + \Delta_N
		memcpy(&y[0], &ct[i*CRYPTO_BLOCKBYTES], CRYPTO_BLOCKBYTES);
		xor_bytes(y, nonced_mask, CRYPTO_BLOCKBYTES);
		
		// compute W_{i} = E^-4_{L_{a+i}}(Y_{i})
		twegift_dec(w, nonced_key, &twk, y);
		
		// W_\xor = W_\xor + W_{i}
		xor_bytes(wxor, w, CRYPTO_BLOCKBYTES);
		
		// compute X_{i} = E^-4_{L_{a+i}}(W_{i})
		twegift_dec(x, nonced_key, &twk, w);
		
		// compute M_{i} = X_{i} + \Delta_N
		xor_bytes(x, nonced_mask, CRYPTO_BLOCKBYTES);
		memcpy(&pt[i*CRYPTO_BLOCKBYTES], &x[0], CRYPTO_BLOCKBYTES);
		*ptlen += CRYPTO_BLOCKBYTES;
		
		// L_{a+i+1} = L_{a+i} \odot \alpha
		mult_by_alpha(nonced_key, nonced_key);
	}
	// set control bits to 101
	twk = 0x05;
	
	// compute X_{m-1} = \Delta_N + <|C|-(m-1)n>_n
	memset(x, 0, CRYPTO_BLOCKBYTES);
	x[0] = PARTIAL_BLOCK_LEN(m, ctlen);
	xor_bytes(x, nonced_mask, CRYPTO_BLOCKBYTES);
	
	// compute W_{m-1} = E^5_{L_{a+m-1}}(X_{m-1})
	twegift_enc(w, nonced_key, &twk, x);
	
	// compute Y_{m-1} = E^5_{L_{a+m-1}}(W_{m-1})
	twegift_enc(y, nonced_key, &twk, w);
	
	// compute M_{m-1} = chop(Y_{m-1} + \Delta_N) + C_{m-1}
	xor_bytes(y, nonced_mask, CRYPTO_BLOCKBYTES);
	memcpy(&pt[(m-1)*CRYPTO_BLOCKBYTES], &ct[(m-1)*CRYPTO_BLOCKBYTES], PARTIAL_BLOCK_LEN(m, ctlen));
	xor_bytes(&pt[(m-1)*CRYPTO_BLOCKBYTES], y, PARTIAL_BLOCK_LEN(m, ctlen));
	*ptlen += PARTIAL_BLOCK_LEN(m, ctlen);
	
	// W_\xor = W_\xor \xor W_{m-1} \xor M_{m-1}
	xor_bytes(wxor, w, CRYPTO_BLOCKBYTES);
	xor_bytes(wxor, &pt[(m-1)*CRYPTO_BLOCKBYTES], PARTIAL_BLOCK_LEN(m, ctlen));
}

/**********************************************************************
 * 
 * @name	:	proc_tg
 * 
 * @note	:	Tag generator.
 * 
 **********************************************************************/
void proc_tg(u8 *tag, u8 *nonced_key, u8 *nonced_mask, u8 *vxor, u8 *wxor)
{
	u8 twk;
	
	// set control bits to 110
	twk = 0x06;
	
	// L_{a+m} = K_N \odot \alpha
	mult_by_alpha(nonced_key, nonced_key);
	
	// compute T = E^6_{L_{a+m}}(V_\xor + W_\xor + \Delta_N) + \Delta_N
	xor_bytes(vxor, wxor, CRYPTO_BLOCKBYTES);
	xor_bytes(vxor, nonced_mask, CRYPTO_BLOCKBYTES);
	twegift_enc(tag, nonced_key, &twk, vxor);
	xor_bytes(tag, nonced_mask, CRYPTO_BLOCKBYTES);
}

/**********************************************************************
 * 
 * @name	:	crypto_aead_encrypt
 * 
 * @note	:	Main encryption function.
 * 
 **********************************************************************/
int crypto_aead_encrypt(
	unsigned char *ct, unsigned long long *ctlen,
	const unsigned char *pt, unsigned long long ptlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *nsec,
	const unsigned char *npub,
	const unsigned char *k
)
{
	// to bypass unused warning on nsec
	nsec = nsec;
	
	u8 nonced_key[CRYPTO_KEYBYTES];
	u8 nonced_mask[CRYPTO_NPUBBYTES];
	
	u8 tag[CRYPTO_ABYTES];
	
	u8 wxor[CRYPTO_BLOCKBYTES] = { 0 };
	u8 vxor[CRYPTO_BLOCKBYTES] = { 0 };
	
	// initialize and derive nonce-based key and mask
	u64 pt_blocks = ptlen%CRYPTO_BLOCKBYTES ? ((ptlen/CRYPTO_BLOCKBYTES)+1) : (ptlen/CRYPTO_BLOCKBYTES);
	u64 ad_blocks = adlen%CRYPTO_BLOCKBYTES ? ((adlen/CRYPTO_BLOCKBYTES)+1) : (adlen/CRYPTO_BLOCKBYTES);
	
	init(nonced_key, nonced_mask, k, npub);
	
	// process AD, if non-empty
	if(ad_blocks != 0)
	{
		proc_ad(nonced_key, vxor, nonced_mask, ad, ad_blocks, adlen);
	}
	
	// process PT, if non-empty
	if(pt_blocks != 0)
	{
		proc_pt(nonced_key, wxor, ct, ctlen, nonced_mask, pt, pt_blocks, ptlen);
	}
	else
	{
		*ctlen = 0;
	}
	
	// generate tag and append to ciphertext
	proc_tg(tag, nonced_key, nonced_mask, vxor, wxor);
	memcpy(&ct[*ctlen],&tag[0],CRYPTO_ABYTES);
	*ctlen += CRYPTO_ABYTES;
	
	return 0;
}

/**********************************************************************
 * 
 * @name	:	crypto_aead_decrypt
 * 
 * @note	:	Main decryption function.
 * 
 **********************************************************************/
int crypto_aead_decrypt(
	unsigned char *pt, unsigned long long *ptlen,
	unsigned char *nsec,
	const unsigned char *ct, unsigned long long ctlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *npub,
	const unsigned char *k
)
{
	// to bypass unused warning on nsec
	nsec = nsec;
	
	ctlen = ctlen - CRYPTO_ABYTES;
	
	int pass;
	
	u8 nonced_key[CRYPTO_KEYBYTES];
	u8 nonced_mask[CRYPTO_NPUBBYTES];
	
	u8 tag[CRYPTO_ABYTES];
	
	u8 wxor[CRYPTO_BLOCKBYTES] = { 0 };
	u8 vxor[CRYPTO_BLOCKBYTES] = { 0 };
	
	// initialize and derive nonce-based key and mask
	u64 ct_blocks = ctlen%CRYPTO_BLOCKBYTES ? ((ctlen/CRYPTO_BLOCKBYTES)+1) : (ctlen/CRYPTO_BLOCKBYTES);
	u64 ad_blocks = adlen%CRYPTO_BLOCKBYTES ? ((adlen/CRYPTO_BLOCKBYTES)+1) : (adlen/CRYPTO_BLOCKBYTES);
	
	init(nonced_key, nonced_mask, k, npub);
	
	// process AD, if non-empty
	if(ad_blocks != 0)
	{
		proc_ad(nonced_key, vxor, nonced_mask, ad, ad_blocks, adlen);
	}
	
	// process CT, if non-empty
	if(ct_blocks != 0)
	{
		proc_ct(nonced_key, wxor, pt, ptlen, nonced_mask, ct, ct_blocks, ctlen);
	}
	else
	{
		*ptlen = 0;
	}
	
	// generate tag
	proc_tg(tag, nonced_key, nonced_mask, vxor, wxor);
	
	// check computed tag =? received tag (0 if equal)
	pass = memcmp(tag, &ct[*ptlen], CRYPTO_ABYTES);
	
	if(!pass)
	{
		return pass;
	}
	else
	{
		return -1;
	}
}



