/*
Written by: Siang Meng Sim
Email: crypto.s.m.sim@gmail.com
Date: 25 Feb 2019
*/

#include <stdint.h>


#define CRYPTO_KEYBYTES    16
#define CRYPTO_NSECBYTES    0
#define CRYPTO_NPUBBYTES   16
#define CRYPTO_ABYTES      16
#define CRYPTO_NOOVERLAP    1

//added
#define MAXTEST_BYTES_M 16 
#define MAXTEST_BYTES_AD 16

//void giftb128(uint8_t P[16], const uint8_t K[16], uint8_t C[16]);

/*int sundae_enc(const uint8_t* N, unsigned long long Nlen,
                const uint8_t* A, unsigned long long Alen,
                const uint8_t* M, unsigned long long Mlen,
                const uint8_t K[16],
                uint8_t* C,
                int outputTag);*/

/*int sundae_dec(const uint8_t* N, unsigned long long Nlen,
                const uint8_t* A, unsigned long long Alen,
                uint8_t* M,
                const uint8_t K[16],
                const uint8_t* C, unsigned long long Clen);*/


int crypto_aead_encrypt(unsigned char *c, unsigned long long *clen,
                        const unsigned char *m, unsigned long long mlen,
                        const unsigned char *ad, unsigned long long adlen,
                        const unsigned char *nsec,
                        const unsigned char *npub,
                        const unsigned char *k
                        );

int crypto_aead_decrypt(unsigned char *m, unsigned long long *mlen,
                        unsigned char *nsec,
                        const unsigned char *c, unsigned long long clen,
                        const unsigned char *ad, unsigned long long adlen,
                        const unsigned char *npub,
                        const unsigned char *k
                        );
