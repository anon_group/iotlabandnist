/*
SUNDAE-GIFT
Prepared by: Siang Meng Sim
Email: crypto.s.m.sim@gmail.com
Date: 09 Feb 2019
*/

#include <stdlib.h>
#include <stdio.h>
//#include "api.h"
#include "sundae.h"
//#include "gift128.h"
//#include "crypto_aead.h"


/////////////////////////////////gift128.c////////////////////////////////////////////////////

/*Round constants*/
unsigned char GIFT_RC[40] = {
    0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
    0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
    0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
    0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A
};

uint32_t rowperm(uint32_t S, int B0_pos, int B1_pos, int B2_pos, int B3_pos){
    uint32_t T=0;
    int b;
    for(b=0; b<8; b++){
        T |= ((S>>(4*b+0))&0x1)<<(b + 8*B0_pos);
        T |= ((S>>(4*b+1))&0x1)<<(b + 8*B1_pos);
        T |= ((S>>(4*b+2))&0x1)<<(b + 8*B2_pos);
        T |= ((S>>(4*b+3))&0x1)<<(b + 8*B3_pos);
    }
    return T;
}

void giftb128(uint8_t P[16], const uint8_t K[16], uint8_t C[16]){
    int round;
    uint32_t S[4],T;
    uint16_t W[8],T6,T7;

    S[0] = ((uint32_t)P[ 0]<<24) | ((uint32_t)P[ 1]<<16) | ((uint32_t)P[ 2]<<8) | (uint32_t)P[ 3];
    S[1] = ((uint32_t)P[ 4]<<24) | ((uint32_t)P[ 5]<<16) | ((uint32_t)P[ 6]<<8) | (uint32_t)P[ 7];
    S[2] = ((uint32_t)P[ 8]<<24) | ((uint32_t)P[ 9]<<16) | ((uint32_t)P[10]<<8) | (uint32_t)P[11];
    S[3] = ((uint32_t)P[12]<<24) | ((uint32_t)P[13]<<16) | ((uint32_t)P[14]<<8) | (uint32_t)P[15];

    W[0] = ((uint16_t)K[ 0]<<8) | (uint16_t)K[ 1];
    W[1] = ((uint16_t)K[ 2]<<8) | (uint16_t)K[ 3];
    W[2] = ((uint16_t)K[ 4]<<8) | (uint16_t)K[ 5];
    W[3] = ((uint16_t)K[ 6]<<8) | (uint16_t)K[ 7];
    W[4] = ((uint16_t)K[ 8]<<8) | (uint16_t)K[ 9];
    W[5] = ((uint16_t)K[10]<<8) | (uint16_t)K[11];
    W[6] = ((uint16_t)K[12]<<8) | (uint16_t)K[13];
    W[7] = ((uint16_t)K[14]<<8) | (uint16_t)K[15];

    for(round=0; round<40; round++){
        /*===SubCells===*/
        S[1] ^= S[0] & S[2];
        S[0] ^= S[1] & S[3];
        S[2] ^= S[0] | S[1];
        S[3] ^= S[2];
        S[1] ^= S[3];
        S[3] ^= 0xffffffff;
        S[2] ^= S[0] & S[1];

        T = S[0];
        S[0] = S[3];
        S[3] = T;


        /*===PermBits===*/
        S[0] = rowperm(S[0],0,3,2,1);
        S[1] = rowperm(S[1],1,0,3,2);
        S[2] = rowperm(S[2],2,1,0,3);
        S[3] = rowperm(S[3],3,2,1,0);

        /*===AddRoundKey===*/
        S[2] ^= ((uint32_t)W[2]<<16) | (uint32_t)W[3];
        S[1] ^= ((uint32_t)W[6]<<16) | (uint32_t)W[7];

        /*Add round constant*/
        S[3] ^= 0x80000000 ^ GIFT_RC[round];

        /*===Key state update===*/
        T6 = (W[6]>>2) | (W[6]<<14);
        T7 = (W[7]>>12) | (W[7]<<4);
        W[7] = W[5];
        W[6] = W[4];
        W[5] = W[3];
        W[4] = W[2];
        W[3] = W[1];
        W[2] = W[0];
        W[1] = T7;
        W[0] = T6;
    }

    C[ 0] = S[0]>>24;
    C[ 1] = S[0]>>16;
    C[ 2] = S[0]>>8;
    C[ 3] = S[0];
    C[ 4] = S[1]>>24;
    C[ 5] = S[1]>>16;
    C[ 6] = S[1]>>8;
    C[ 7] = S[1];
    C[ 8] = S[2]>>24;
    C[ 9] = S[2]>>16;
    C[10] = S[2]>>8;
    C[11] = S[2];
    C[12] = S[3]>>24;
    C[13] = S[3]>>16;
    C[14] = S[3]>>8;
    C[15] = S[3];

return;}

///////////////////////////////////sundea.c/////////////////////////////////////////////////
void doubling(uint8_t* A){
/*doubling uses x^{16} + x^5 + x^3 + x + 1 at byte level*/
uint8_t ADD=A[0];
int i;
for(i=0; i<15; i++){
    A[i] = A[i+1];
}
A[15] = ADD;
A[14] ^= ADD;
A[12] ^= ADD;
A[10] ^= ADD;

return;
}

int sundae_enc(const uint8_t* N, unsigned long long Nlen,
                const uint8_t* A, unsigned long long Alen,
                const uint8_t* M, unsigned long long Mlen,
                const uint8_t K[16],
                uint8_t* C,
                int outputTag){
/*
member 1 takes 96-bit nonce   (Nlen = 12)
member 2 does not take in nonce (Nlen = 0)
member 3 takes 128-bit nonce  (Nlen = 16)
member 4 takes 64-bit nonce   (Nlen = 8)
*/

/*
return -1 if invalid parameter
return 0 if encryption successful
*/

unsigned long long i;
const uint8_t* startM = M;
uint8_t V[16],*AS;
uint8_t ib[16]={};

if(Alen!=0) ib[0] |= 0x80;
if(Mlen!=0) ib[0] |= 0x40;;

if(Nlen==16) ib[0] |= 0xb0;
else if(Nlen==12) ib[0] |= 0xa0;
else if(Nlen==8) ib[0] |= 0x90;
else if(Nlen!=0) return -1; /*Invalid tag length*/

/*Prepend N to A*/
unsigned long long ADlen = Alen+Nlen;

uint8_t* AD = (uint8_t*)malloc(ADlen*sizeof(uint8_t));
AS=AD;

for(i=0; i<Nlen; i++){
    AD[i] = N[i];
}
for(i=0; i<Alen; i++){
    AD[Nlen+i] = A[i];
}

//uint8_t* V = (uint8_t*)malloc(16*sizeof(uint8_t));

/*Initialisation*/
giftb128(ib,K,V);

/*Process AD*/
while(ADlen>16){
    for(i=0; i<16; i++){
        V[i]^=AD[i];
    }

    giftb128(V,K,V);

    AD+=16;
    ADlen-=16;
}

if(ADlen==16){
    for(i=0; i<16; i++){
        V[i]^=AD[i];
    }

    doubling(V);
    doubling(V);
    giftb128(V,K,V);
}
else if(ADlen>0){
    for(i=0; i<ADlen; i++){
        V[i]^=AD[i];
    }

    /*10*-padding*/
    V[ADlen]^=0x80;

    doubling(V);
    giftb128(V,K,V);
}
AD=AS;
free(AD);
/*Process M*/
unsigned long long Clen = Mlen;
while(Mlen>16){
    for(i=0; i<16; i++){
        V[i]^=M[i];
    }

    giftb128(V,K,V);

    M+=16;
    Mlen-=16;
}

if(Mlen==16){
    for(i=0; i<16; i++){
        V[i]^=M[i];
    }

    doubling(V);
    doubling(V);
    giftb128(V,K,V);
}
else if(Mlen>0){
    for(i=0; i<Mlen; i++){
        V[i]^=M[i];
    }

    /*10*-padding*/
    V[Mlen]^=0x80;

    doubling(V);
    giftb128(V,K,V);
}

/*output the Tag*/
for(i=0; i<16; i++){
    C[i]=V[i];
}

if(outputTag) return 0; /*for decryption, early termination*/

C+=16;

/*output C*/
M = startM;
while(Clen>=16){
    giftb128(V,K,V);

    for(i=0; i<16; i++){
        C[i]=M[i]^V[i];
    }

    Clen-=16;
    C+=16;
    M+=16;
}

if(Clen>0){
    giftb128(V,K,V);

    for(i=0; i<Clen; i++){
        C[i]=M[i]^V[i];
    }
}

return 0;}


int sundae_dec(const uint8_t* N, unsigned long long Nlen,
                const uint8_t* A, unsigned long long Alen,
                uint8_t* M,
                const uint8_t K[16],
                const uint8_t* C, unsigned long long Clen){
/*
-1 for authentication fail
-2 for invalid tag length
*/
uint8_t Tprime[16],V[16],T[16];
unsigned long long i;

if(Clen<16) return -2; /*invalid tag length*/

uint8_t* startM=M;

//uint8_t* V = (uint8_t*)malloc(16*sizeof(uint8_t));
//uint8_t* T = (uint8_t*)malloc(16*sizeof(uint8_t));

/*Extract the Tag*/
for(i=0; i<16; i++){
    V[i] = C[i];
    T[i] = C[i];
}

C+=16;
Clen-=16;

unsigned long long Mlen = Clen;

/*Decrypt C*/
while(Clen>=16){
    giftb128(V,K,V);

    for(i=0; i<16; i++){
        M[i]=C[i]^V[i];
    }

    Clen-=16;
    M+=16;
    C+=16;
}

if(Clen>0){
    giftb128(V,K,V);

    for(i=0; i<Clen; i++){
        M[i]=C[i]^V[i];
    }
}

/*Generate T*/
//uint8_t* Tprime = (uint8_t*)malloc(16*sizeof(uint8_t));
sundae_enc(N,Nlen,A,Alen,startM,Mlen,K,Tprime,1);

/*Match tags*/
for(i=0; i<16; i++){
    if(T[i] != Tprime[i]){
        return -1;
    }
}
return 0;}


//////////////////////////////mainencrypt/////////////////////////////////////////////
/*
 the code for the cipher implementation goes here,
 generating a ciphertext c[0],c[1],...,c[*clen-1]
 from a plaintext m[0],m[1],...,m[mlen-1]
 and associated data ad[0],ad[1],...,ad[adlen-1]
 and secret message number nsec[0],nsec[1],...
 and public message number npub[0],npub[1],...
 and secret key k[0],k[1],...
 */

int crypto_aead_encrypt(unsigned char *c, unsigned long long *clen,
                        const unsigned char *m, unsigned long long mlen,
                        const unsigned char *ad, unsigned long long adlen,
                        const unsigned char *nsec,
                        const unsigned char *npub,
                        const unsigned char *k
                        )
{
    sundae_enc(npub,CRYPTO_NPUBBYTES,ad,adlen,m,mlen,k,c,0);
    *clen = mlen+16;
    (void)nsec;
    return 0;
}

/*
 the code for the cipher implementation goes here,
 generating a plaintext m[0],m[1],...,m[*mlen-1]
 and secret message number nsec[0],nsec[1],...
 from a ciphertext c[0],c[1],...,c[clen-1]
 and associated data ad[0],ad[1],...,ad[adlen-1]
 and public message number npub[0],npub[1],...
 and secret key k[0],k[1],...
 */
int crypto_aead_decrypt(unsigned char *m, unsigned long long *mlen,
                        unsigned char *nsec,
                        const unsigned char *c, unsigned long long clen,
                        const unsigned char *ad, unsigned long long adlen,
                        const unsigned char *npub,
                        const unsigned char *k
                        )
{
    int result = sundae_dec(npub,CRYPTO_NPUBBYTES,ad,adlen,m,k,c,clen);
    *mlen = clen-16;
    (void)nsec;
    return result;
}




