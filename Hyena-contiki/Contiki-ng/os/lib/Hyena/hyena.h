#ifndef _HYENA_H_
#define _HYENA_H_

#include <string.h>
#include <stdlib.h>

typedef unsigned char u8; 
typedef unsigned int u32;
typedef unsigned long long int u64; 

/* 
 * No. of block cipher rounds to be used
 */ 
#define CRYPTO_BC_NUM_ROUNDS (40)

/*
 * Generate encryption round keys.
 */
#define _GIFT_ENC_ROUND_KEY_GEN(round_keys, key)		(generate_round_keys(round_keys, key))

/*
 * Generate decryption round keys.
 */
#define _GIFT_DEC_ROUND_KEY_GEN(round_keys, key)		(generate_round_keys(round_keys, key))

void generate_round_keys(u8 (*round_key_nibbles)[32], const u8 *key_bytes);

void gift_enc(u8 *ct, const u8 (*round_keys)[32], const u8 *pt);

#endif

/////////////////////////////////////////

#define CRYPTO_KEYBYTES 16

#define CRYPTO_NSECBYTES 0

#define CRYPTO_NPUBBYTES 12

#define CRYPTO_ABYTES 16

#define CRYPTO_NOOVERLAP 1

#define MAXTEST_BYTES_M 16 
#define MAXTEST_BYTES_AD 16


int crypto_aead_encrypt(
	unsigned char *c, unsigned long long *clen,
	const unsigned char *m, unsigned long long mlen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *nsec,
	const unsigned char *npub,
	const unsigned char *k
);

int crypto_aead_decrypt(
	unsigned char *m, unsigned long long *mlen,
	unsigned char *nsec,
	const unsigned char *c, unsigned long long clen,
	const unsigned char *ad, unsigned long long adlen,
	const unsigned char *npub,
	const unsigned char *k
);


